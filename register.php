<?php
/**
* Template Name: Register page
*
*/
get_header();
?>

<style>
.register-page:before {
    background-image: url("<?php echo get_template_directory_uri(); ?>/img/login-hero.png");
}
</style>

<section class="full-page register-page">
    <div class="container">
        <div class="full-page__content login-form form-dark">
            <h1>Register your account</h1>
            <!-- Form is in plugin -->
            <?php custom_registration_function(); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
