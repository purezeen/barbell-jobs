<?php get_header(); //Template Name: Lost password ?>

<style>
.full-page:before {
    background-image: url("<?php echo get_template_directory_uri(); ?>/img/login-hero.png");
}
</style>

<section class="full-page cover">
    <div class="container">
        <div class="full-page__content form-dark">
           <?php wc_get_template( 'myaccount/form-lost-password.php', array( 'form' => 'lost_password' ) ); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>