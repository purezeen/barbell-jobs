<?php get_header(); //Template Name: About us ?>


<style>
   .hero-about::before {background-image:url(<?php echo get_template_directory_uri(); ?>/img/hero-default.png);opacity: 0.6;}
</style>

<section class="hero-about hero-about--move-img">
   <div class="container">
      <div class="hero-about__content">
         <h1>ALL THE BEST BUSINESSES START BECAUSE SOMETHING IS MISSING</h1>
      </div>
      <div class="hero-about__img hero-about--border-top">
         <img src="<?php echo get_template_directory_uri(); ?>/img/about-hero.jpg" alt="barbelljobs" />
      </div>
   </div>
</section>

<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

<?php endwhile; 
// End of the loop. 
?>

<?php get_footer(); ?>