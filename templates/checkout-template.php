<?php get_header(); //Template Name: Checkout ?>
<style>
   .hero::before {background-image:url(<?php echo get_template_directory_uri(); ?>/img/hero-background.png);}
</style>

<section class="hero hero--pt-small hero--pb-small cover">
	<div class="hero__guide hero__guide--scroll">
		<div class="container hero__guide--space-between">
			<span class="hero__guide-list step-2 active">
				<span class="hero__guide-checkmark "></span>Company details
			</span>
			<span class="hero__guide-list step-3 active">
				<span class="hero__guide-checkmark"></span>job details
			</span>
			<span class="hero__guide-list step-4 active">
				<span class="hero__guide-checkmark active"></span>preview
			</span>
			<span class="hero__guide-list step-5 active">
				<span class="hero__guide-checkmark"></span>packages &amp; boosters
			</span>
			<span class="hero__guide-list step-6 active">
				<span class="hero__guide-checkmark"></span>payment
			</span>
		</div>
	</div>
	<div class="container">
		<div class="hero__content steps">
			<div class="step step-1">
				<span class="hero__subtitle checkout-show">Step 5</span>
				<h1 class="checkout-show">Payment</h1>
				<h1 class="order-received-show">Thank you. Your order has been received.</h1>
			</div>
		</div>
	</div>
</section>

<div class="container checkout-wrap" id="checkout-wrap">
   <?php the_content(); ?>
</div>


<?php get_footer(); ?>