<?php get_header(); //Template Name: Default page (container) ?>

<main class="section main-min-height">
   <div class="container">
      <?php the_content(); ?>
   </div>
</main>

<?php get_footer(); ?>