<?php get_header(); //Template Name: Register thank you ?>

<style>
.full-page:before {
    background-image: url("<?php echo get_template_directory_uri(); ?>/img/login-hero.png");
}
</style>

<?php 
$current_user = wp_get_current_user();
if ( !($current_user instanceof WP_User) )
   return;
$roles = $current_user->roles;
?>
<section class="full-page cover">
    <div class="container">
        <div class="full-page__content">
            <?php if($roles[0] == 'employer' || $roles[0] == 'administrator'): ?>
                <?php if ( have_rows( 'thank_you_for_employer' ) ) : ?>
                    <?php while ( have_rows( 'thank_you_for_employer' ) ) : the_row(); ?>

                        <div class="full-page__content--large">
                            <?php the_sub_field( 'intro_description_for_employer' ); ?>
                        </div>

                        <?php $button_employer = get_sub_field( 'button_employer' ); ?>
                        <?php if ( $button_employer ) { ?>
                            <a href="<?php echo $button_employer['url']; ?>" <?php echo ( get_sub_field( 'add_nofollow_empl' ) == 1 )? 'rel="nofollow" ':'';?> class="btn btn--primary" target="<?php echo $button_employer['target']; ?>"><?php echo $button_employer['title']; ?></a>
                        <?php } ?>
                        <?php $button_employer2 = get_sub_field( 'button_employer2' ); ?>
                        <?php if ( $button_employer2 ) { ?>
                            <a href="<?php echo $button_employer2['url']; ?>" <?php echo ( get_sub_field( 'add_nofollow_empl2' ) == 1 )? 'rel="nofollow" ':'';?> class="btn btn--ghost" target="<?php echo $button_employer2['target']; ?>"><?php echo $button_employer2['title']; ?></a>
                        <?php } ?>
                    <?php endwhile; ?>
                <?php endif; ?>              
            <?php elseif($roles[0] == 'candidate' || $roles[0] == 'administrator'): ?>

                <?php if ( have_rows( 'thank_you_for_candidate' ) ) : ?>
                    <?php while ( have_rows( 'thank_you_for_candidate' ) ) : the_row(); ?>

                        <div class="full-page__content--large">
                        <?php the_sub_field( 'intro_description_for_candidate' ); ?>
                        </div>                       
                        <?php $button = get_sub_field( 'button' ); ?>
                        <?php if ( $button ) { ?>
                            <a href="<?php echo $button['url']; ?>" <?php echo ( get_sub_field( 'add_nofollow_cand' ) == 1 )? 'rel="nofollow" ':'';?> class="btn btn--primary" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
                        <?php } ?>
                        <?php $button2 = get_sub_field( 'button2' ); ?>
                        <?php if ( $button2 ) { ?>
                            <a href="<?php echo $button2['url']; ?>" <?php echo ( get_sub_field( 'add_nofollow_cand2' ) == 1 )? 'rel="nofollow" ':'';?> class="btn btn--ghost" target="<?php echo $button2['target']; ?>"><?php echo $button2['title']; ?></a>
                        <?php } ?>
                    <?php endwhile; ?>
                <?php endif; ?>

            <?php else: ?>

            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>