<?php get_header(); //Template Name: Single - without hero ?>

<section class="single-inner container">
   <div class="single-main">
      <div class="editor editor--single-main counter--reset">
         <div class="single-header">
            <?php $label = get_field( 'label' ); ?>
            <?php if( $label ): ?>
               <span class="single-header__label"><?php echo $label; ?></span>
            <?php endif; ?>
            
            <h1 class="single-main-title"><?php the_title(); ?></h1>
         </div>
        
         <?php the_content(); ?>
      </div>

   </div>
</section>

<?php get_footer(); ?>