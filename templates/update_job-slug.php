<?php get_header(); //Template Name: Update job slug ?>

<h1>Update job slug</h1>
<?php 
// Get all job listing and update slug
// Create slug: job_title-job_company_name 
$args2 = array(
	'post_type' => 'job_listing',
	'post_status' => 'all',
	'numberposts' => -1
);

$job_listing2 = get_posts($args2);
foreach ($job_listing2  as $job) {
	$job_id = $job->ID;
	$company_name = get_field('_company_name', $job_id);
	wp_update_post([
		"post_name" => $job->post_title . '-' . $company_name,
		"ID" => $job_id,
	 ]);
} ?>

<?php get_footer(); ?>