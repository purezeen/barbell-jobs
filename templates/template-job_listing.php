<?php
/**
 * Template Name: Jobs listing
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package barbell-jobs
 */

use Gettext\Languages\Exporter\Php;

get_header();
?>

<?php 
$count_posts = wp_count_posts( $post_type = 'job_listing' );
//  var_dump($count_posts);
if ( $count_posts ) {
   $currently_open_post = $count_posts->publish;
   $expired_post = $count_posts->expired;
   $draft_post = $count_posts->draft;
   $posted_jobs_total = $expired_post + $draft_post + $currently_open_post ;
}
?>

<style>
   .hero::before { 
      background-image:url(<?php echo get_template_directory_uri(); ?>/img/hero-home.png);}
</style>

<?php if ( have_rows( 'hero' ) ) : ?>
	<?php while ( have_rows( 'hero' ) ) : the_row(); ?>

      <section class="hero hero--main">
         <div class="container">

            <div class="hero__content">
               <h1 class="hero--huge-title"><?php the_sub_field( 'hero_title' ); ?></h1>
               <div class="hero__content-inner">
                  <?php the_sub_field( 'hero_description' ); ?>
               </div>
            </div>
           
            <div class="hero__footer hero-number-counter">
               <div class="hero-number-counter__col">
                  <span class="hero__footer--title"><?php echo $posted_jobs_total; ?></span>
                  <span class="hero__footer--desc">Posted Jobs total</span>
               </div>
               <div class="hero-number-counter__col">
                  <span class="hero__footer--title"><?php echo $currently_open_post; ?></span>
                  <span class="hero__footer--desc">Currently Open Jobs</span>
               </div>
               <div class="hero-number-counter__col">
                  <span class="hero__footer--title"><a href="/post-a-job/" class="btn btn--border-white">Post a job</a></span>
                  <span class="hero__footer--desc">Get your job listed</span>
               </div>
            </div>

         </div>
      </section>  

	<?php endwhile; ?>
<?php endif; ?>

<!-- Banner  -->
<!-- <div class="container banner banner--position-top">
   <div class="banner__content">
      <div class="banner__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/profile-icon.svg" alt="profile"></div>
      <div class="banner__content-inner">
         <p>Answer a few quick questions and complete your profile to be listed on BarbellJobs.</p>
      </div>
   </div>
   <a href="#" class="btn btn--medium btn--light banner__btn">Complete your profile</a>
</div> -->

<main class="container job section-medium-pt">
	<div class="job__sidebar">
      <button class="btn btn--border-light btn--wide filter-btn"><img src="<?php echo get_template_directory_uri(); ?>/img/filter-icon.svg" alt="filter icon" width="16px" height="16px">Filter jobs</button>
      <div id="filter-custom">
         <div class="btn-close btn-close--dark filter-close">
            <span>+</span>
         </div>
         <!-- Don't change class 'filter-active' and style 'display: none;' -->
         <div class="filter-active" style="display: none;">
            <h6 class="filter-category-title">Active filters</h6>
            <ul class="unstyle-list"></ul>
         </div>
         <div class="filters">
            <!-- Filter search -->
            <?php echo do_shortcode('[job_search_filters section="137"] '); ?>
         </div>
      </div>
	</div>
   
	<div class="job__main section-large-pb">

		<!-- Filter search  -->
		<div id="wp-job-manager-custom">
         <?php 
            // Check if featured expired date is less than current date. If it is less, remove featured
            $jobs = get_job_listings( array(
               'posts_per_page'    => -1,
               'orderby'           => 'date',
               'order'             => 'DESC',
            ) );
          
            if (  $jobs->have_posts() ) { ?>
           
            <?php while ( $jobs->have_posts() ) : $jobs->the_post(); ?>
               <?php $featured_date = get_post_meta( $post->ID, '_job_featered_expired_date', true ); ?>
               <?php
               $date1 = date('Y-m-d');
               $datestring = strval($date1);
               $date2 = strval($featured_date);
               if((!empty($featured_date))) {
                  if($date2 > $datestring ){
                  }else {
                     update_post_meta( $post->ID, '_featured', 0 );
                  }
               } ?>
           <?php endwhile; ?>
           <?php wp_reset_postdata(); ?>
           <?php } ?>

			<?php echo do_shortcode('[job_search_filters section="138"] '); ?>
		</div>
		
		<?php
		while ( have_posts() ) :
			the_post();
			the_content();

		endwhile; // End of the loop.
		?>

	</div>

</main><!-- #main -->

<?php
get_footer();

