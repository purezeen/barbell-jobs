<?php get_header(); //Template Name: Companu profile ?>


<section class="hero hero--profile-layout cover">
   <div class="container">
      <a href="#" class="btn-link-back"><span class="arrow left"></span>To job listing</a>
      <div class="hero__profile-img profile-img profile-img--medium profile-img--border">
         <img src="<?php echo get_template_directory_uri(); ?>/img/company-profile.png" alt="profile">
      </div>
      <div class="hero__content">
         <h1>Centripetal CrossFit</h1>
         <p>Our space is unique: Guests/athletes visit daily from all over the world. Makena CF provides a very dynamic
            style of CrossFit with heavy emphasis on form, technique, mobility and community.</p>
         <div class="hero__tags">
            <span class="tag">CrossFit</span><span class="tag">ASC Powerlifting L1</span><span
               class="tag">Weightlifting</span><span class="tag">Powerlifting</span><span class="tag">Fitness</span>
         </div>

      </div>
      <div class="hero__social-icons">
         <div class="social-icons social-icons--white">
            <a href="#" class="social-icon">
               <div class="social-icon__img"><img
                     src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.png"
                     alt="linkedin"></div>
            </a>
            <a href="#" class="social-icon">
               <div class="social-icon__img"><img
                     src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png" alt="twitter">
               </div>
            </a>
            <a href="#" class="social-icon">
               <div class="social-icon__img"><img
                     src="<?php echo get_template_directory_uri(); ?>/img/email-icon.png" alt="email">
               </div>
            </a>
         </div>
      </div>
   </div>
</section>

<section class="company-profile">
   <div class="container">
      <div class="company-profile-row border-dashed-bottom">
         <div class="company-profile__col company-profile-row__title">
            <h2>About</h2>
         </div>
         <div class="company-profile__col company-profile-row__content">
            <h3>So, who are we?</h3>
            <p>We’re a Conversion Rate Optimization agency that helps online businesses, big AND small, grow their
               revenue through data-driven experimentation.</p>
            <h3>What’s it like working with us?</h3>
            <p>Every day at Conversion Advocates is fresh and unique. You’ll meet our team, who live all around the
               world, led by our founders Raphaëlle and Sina.</p>
            <p>You’ll have access to amazing resources and tools that enable you to produce best work.</p>
            <p>You’ll have a good time! We’re not all about business. Our atmosphere fosters a healthy attitude for work
               and play. We never want our team to be dreading Monday mornings.</p>
         </div>
      </div>
      <div class="company-profile-row border-dashed-bottom">
         <div class="company-profile__col company-profile-row__title">
            <h2>Openings</h2>
         </div>
         <div class="company-profile__col company-profile-row__content company-profile-row__content-wide">
            <div class="job-listing job-listing--featured ">
               <div class="job-listing__header">
                  <div class="job-listing__img">
                     <img class="company_logo"
                        src="http://barbel-jobswp.local/wp-content/uploads/2021/06/logo-intro.png" alt="company_logo" width="80px" height="80px">
                  </div>
                  <div class="job-listing__content">
                     <span class="label label--mb label--featured">New job</span>
                     <span class="label label--mb label--featured">xxxx</span>
                     <ul class="job-listing__content-left unstyle-list">
                        <li>
                           <p>Purezeen</p>
                        </li>
                        <li>
                           <h3>Full Stack Developer</h3>
                        </li>
                        <li>
                           <p><span>Novi Sad</span>
                              <span class="job-type full-time">Full Time</span>
                           </p>
                        </li>
                     </ul>
                     <div class="job-listing__content-right">
                        <span class="date"><time datetime="2021-06-16">1 month ago</time></span>
                        <span class="label label--mb label--featured">xxxx</span>
                        <div class="arrow down arrow--light"></div>
                     </div>
                  </div>
               </div>
               <div class="job-listing__short-description">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sed nam magnam eos possimus obcaecati
                     sunt perferendis porro ab asperiores.</p>
                  <a href="http://barbel-jobswp.local/job/full-stack-developer/"
                     class="btn btn--primary btn--medium">Find out more</a>
               </div>
            </div>
            <div class="job-listing job-listing--featured ">
               <div class="job-listing__header">
                  <div class="job-listing__img">
                     <img class="company_logo"
                        src="http://barbel-jobswp.local/wp-content/uploads/2021/06/logo-intro.png" alt="Purezeen">
                  </div>
                  <div class="job-listing__content">
                     <span class="label label--mb label--pick">New job</span>
                     <span class="label label--mb label--featured">xxxx</span>
                     <ul class="job-listing__content-left unstyle-list">
                        <li>
                           <p>Purezeen</p>
                        </li>label--featured
                        <li>
                           <h3>Full Stack Developer</h3>
                        </li>
                        <li>
                           <p><span>Novi Sad</span>
                              <span class="job-type full-time">Full Time</span>
                           </p>
                        </li>
                     </ul>
                     <div class="job-listing__content-right">
                        <span class="date"><time datetime="2021-06-16">1 month ago</time></span>
                        <span class="label label--mb label--featured">xxxx</span>
                        <div class="arrow down arrow--light"></div>
                     </div>
                  </div>
               </div>
               <div class="job-listing__short-description">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea sed nam magnam eos possimus obcaecati
                     sunt perferendis porro ab asperiores.</p>
                  <a href="http://barbel-jobswp.local/job/full-stack-developer/"
                     class="btn btn--primary btn--medium">Find out more</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<?php get_footer(); ?>