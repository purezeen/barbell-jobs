<?php get_header(); //Template Name: Profile list ?>

<section class="profile-list-wrap container section-medium-mt section-medium-mb">

   <div class="profile-list__header">
      <h1 class="section-title-large">Find the right Candidate</h1>

      <div class="" id="wp-job-manager-custom">
         <?php echo do_shortcode('[resume_search_filters section="495"]'); ?>
         <div class="wp-job-skill-filter">
            <?php echo do_shortcode('[resume_search_filters section="496"]'); ?>
         </div>
      </div>
   </div>

   <?php echo do_shortcode('[resumes]'); ?>

</section>

<?php get_footer(); ?>