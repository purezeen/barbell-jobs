<?php 

// Remove logout form woocommerce account menu 
add_filter ( 'woocommerce_account_menu_items', 'remove_my_account_links' );

function remove_my_account_links( $menu_links ){
	//unset( $menu_links['dashboard'] ); // Remove Dashboard
   //unset( $menu_links['edit-address'] ); // Addresses
	//unset( $menu_links['payment-methods'] ); // Remove Payment Methods
	//unset( $menu_links['orders'] ); // Remove Orders
	//unset( $menu_links['downloads'] ); // Disable Downloads
	//unset( $menu_links['edit-account'] ); // Remove Account details tab
	unset( $menu_links['customer-logout'] ); // Remove Logout link
	return $menu_links;
}


/**
 * @snippet       Removes shipping method labels @ WooCommerce Cart / Checkout
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.9
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
  
add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 9999, 2 );
   
function bbloomer_remove_shipping_label( $label, $method ) {
    $new_label = preg_replace( '/^.+:/', '', $label );
    return $new_label;
}

// Remove download from my account page
function custom_my_account_menu_items( $items ) {
	unset($items['downloads']);
	return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );


add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); 

function woo_custom_order_button_text() {
    return __( 'FINISH YOUR ORDER', 'woocommerce' ); 
}