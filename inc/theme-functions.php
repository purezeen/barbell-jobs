<?php
/**
 * Get user's first and last name, else just their first name, else their
 * display name. Defalts to the current user if $user_id is not provided.
 * and make Initials
 *
 * @param  mixed  $user_id The user ID or object. Default is current user.
 * @return string          The user's name.
 */
function km_get_users_name( $user_id = null ) {
	$user_info = $user_id ? new WP_User( $user_id ) : wp_get_current_user();
	if ( $user_info->first_name ) {
		if ( $user_info->last_name ) {
			return $user_info->first_name . ' ' . $user_info->last_name;
		}
		return $user_info->first_name;
	}
	return $user_info->display_name;
}

// Generate initials
function generate_user_initials( $name = null) {
        $name = km_get_users_name();
        $words = explode(' ', $name);
        if (count($words) >= 2) {
            return strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));
        }
        return make_initials_from_single_word($name);
}

// Make initials
function make_initials_from_single_word($name) {
        preg_match_all('#([A-Z]+)#', $name, $capitals);
        if (count($capitals[1]) >= 2) {
            return substr(implode('', $capitals[1]), 0, 2);
        }
        return strtoupper(substr($name, 0, 2));
}

