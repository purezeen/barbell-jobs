<?php
// Deregister WPJM styles
add_action( 'wp_enqueue_scripts', 'remove_default_stylesheet', 20 );
function remove_default_stylesheet() {
  wp_dequeue_style( 'wp-job-manager-frontend' );
  wp_deregister_style( 'wp-job-manager-frontend' );
  wp_dequeue_style( 'wp-job-manager-job-listings' );
  wp_deregister_style( 'wp-job-manager-job-listings' );
}

/**
* Displays the published date of the job listing.
* taken from plugins/wp-job-manager/wp-job-manager-template.php
* WHAT IS NEW: changing time prefix display and adding label for new job posts
* @since 1.25.3
* @param int|WP_Post $post (default: null).
*/
function bebell_the_job_publish_date( $post = null ) {
	$date_format = get_option( 'job_manager_date_format' );

	if ( 'default' === $date_format ) {
		$display_date = wp_date( get_option( 'date_format' ), get_post_timestamp( $post ) );
	} else {
		// translators: Placeholder %s is the relative, human readable time since the job listing was posted.
		$display_date = sprintf( esc_html__( '%s ago', 'wp-job-manager' ), human_time_diff( get_post_timestamp( $post ), time() ) );
	}

	echo '<time datetime="' . esc_attr( get_post_datetime( $post )->format( 'Y-m-d' ) ) . '">' . wp_kses_post( $display_date ) . '</time>';

  // Get 'new' label if Job post is not older then 7 days
  if ( esc_attr( get_post_datetime( $post )->getTimestamp() ) > strtotime('-7 days')) {
    echo 'new';
  }
}

/**
* Display Job time difference
*/
function b_display_time_diff($post = null) {
  $post = get_post( $post );
  $job_expires = $post->_job_expires;
  $display_diff = sprintf( esc_html__( 'Applications closing in %s', 'wp-job-manager' ), human_time_diff( strtotime($job_expires), time() ) );
  echo $display_diff;
}

/**
* ($)Salary Custom Field for WPJM
*/
add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );
function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Salary ($)', 'job_manager' ),
    'type'        => 'text',
    'required'    => true,
    'placeholder' => 'e.g. 20000',
    'priority'    => 7
  );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );
function admin_add_salary_field( $fields ) {
  $fields['_job_salary'] = array(
    'label'       => __( 'Salary ($)', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 20000',
    'description' => ''
  );
  return $fields;
}

/**
* Excerpt Custom Field for WPJM
*/
add_filter( 'submit_job_form_fields', 'frontend_add_excerpt_field' );
function frontend_add_excerpt_field( $fields ) {
  $fields['job']['job_excerpt'] = array(
    'label'       => __( 'Excerpt', 'job_manager' ),
    'type'        => 'textarea',
    'required'    => true,
    'placeholder' => 'Add excerpt',
    'priority'    => 50
  );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_excerpt_field' );
function admin_add_excerpt_field( $fields ) {
  $fields['_job_excerpt'] = array(
    'label'       => __( 'Excerpt', 'job_manager' ),
    'type'        => 'textarea',
    'placeholder' => 'Excerpt',
    'description' => ''
  );
  return $fields;
}

add_filter( 'submit_job_form_fields', 'frontend_add_application_url_field' );
function frontend_add_application_url_field( $fields ) {
  $fields['job']['application_url'] = array(
    'label'       => __( 'Please link to your application form', 'job_manager' ),
    'required'    => true,
    'placeholder' => 'Please link to your application form',
    'type'        => 'url',
    'priority'    => 6.9
  );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_application_url_field' );
function admin_add_application_url_field( $fields ) {
  $fields['_application_url'] = array(
    'label'       => __( 'Please link to your application form', 'job_manager' ),
    'type'        => 'url',
    'placeholder' => 'Please link to your application form',
    );
  return $fields;
}

add_filter( 'submit_job_form_fields', 'frontend_add_application_type_field' );
function frontend_add_application_type_field( $fields ) {
  $fields['job']['application_type'] = array(
    'label'       => __( 'Would you like to use our application form to gather and review candidates? ', 'job_manager' ),
    'required'    => true,
    'priority'    => 6.8
  );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_application_type_field' );
function admin_add_application_type_field( $fields ) {
  $fields['_application_type'] = array(
    'label'       => __( 'Would you like to use our application form to gather and review candidates? ', 'job_manager' ),
    );
  return $fields;
}

add_filter( 'job_manager_job_listing_data_fields', 'admin_add_staffpick_field' );
function admin_add_staffpick_field( $fields ) {
  $fields['_job_staffpick'] = array(
    'label'       => __( 'Staff pick', 'job_manager' ),
    'type'        => 'checkbox',
    'placeholder' => 'Staff pick',
    'description' => 'Add label: STAFF PICK'
  );
  return $fields;
}

add_filter( 'submit_job_form_fields', 'frontend_add_frontend_expires_field' );
function frontend_add_frontend_expires_field( $fields ) {
  $fields['job']['frontend_expires'] = array(
    'label'       => __( 'Application deadline', 'job_manager' ),
    'type'        => 'date',
    'required'    => true,
    'priority'    => 12
  );
  return $fields;
}

// function jom_job_manager_set_expiry_from_user( $job_id ) {
// 	$job_expires = get_post_meta( $job_id, '_frontend_expires', true );
// 	if ( ! empty( $job_expires ) ) {
// 		update_post_meta( $job_id, '_job_expires', $job_expires );
// 	}
// }
// add_action( 'job_manager_user_edit_job_listing', 'jom_job_manager_set_expiry_from_user' );

// add_action( 'job_manager_job_submitted', 'jom_job_manager_set_expiry_from_user' );

// Change text on load more Jobs button
function ijab_change_string( $translated_text, $text, $domain ) {
  $translated_text = str_replace('Load more listings', 'Load more', $translated_text);
  return $translated_text; 
} 
add_filter( 'gettext', 'ijab_change_string', 100, 3 );


// Remove all featured label
// $expired_jobs = get_posts([
//   'post_type' => 'job_listing',
//   'numberposts' => -1,
//   'post_status' => 'all',
//   // 'order'    => 'ASC'
// ]);

// foreach ($expired_jobs as $key => $job) {
//   $job_id = $job->ID;
//   $job_application = get_post_meta( $job_id, '_job_expires', true );
//   // update_post_meta( $jobId, '_job_expires', $job_application );
//   // _job_expires
//   update_post_meta( $job_id, '_frontend_expires',  $job_application );
  
// }

// $resumes = get_posts([
//   'post_type' => 'resume',
//   'numberposts' => -1,
//   'post_status' => 'all'
// ]);

// foreach ($resumes as $key => $resume) {

//   // $resume = get_field('_resume_content', $resume->ID);
//   // update_post_meta( $jobId, '_job_expires', $job_application );
//   // _job_expires
//   // update_field('_resume_content', $user->ID, $resumeId);
//   // $resume->post_content = "";
  
// }

$resumes = get_posts([
  'post_type' => 'resume',
  'numberposts' => -1,
  'post_status' => 'all'
]);

foreach ($resumes as $key => $resume) {
  // $arg = array(
  //   'ID'            => $resume->ID,
  //   'post_content'     => '',
  // );
  // wp_update_post( $arg );
  
  // $editor_id = $resume->ID;
  // $resume = get_field('_resume_content', $resume->ID);
  // update_post_meta( $jobId, '_job_expires', $job_application );
  // _job_expires
  // update_field('_resume_content', $user->ID, $resumeId);
  // $resume->post_content = "";
  // wp_update_post( $arg );
  // wp_editor( '', $editor_id );
  
}


