<?php 

// Register gutenberg block category
add_filter( 'block_categories_all', function( $categories, $post ) {
	return array_merge(
		 $categories,
		 array(
			  array(
					'slug'  => 'sections',
					'title' => 'Sections',
		  		)
		 )
	);
}, 10, 2 );

// Gutenberg editor 
// Register gutenberg blocks 
function register_acf_block_types() {
   acf_register_block_type(array(
      'name'              => 'Hero with full image',
      'title'             => __('Hero with full image'),
      'description'       => __('Hero with full image'),
      'render_template'   => 'template-parts/blocks/hero-fullimg/hero-fullimg.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
            'attributes' => array(
            'data' => array(
               '__is_preview' => true
            ),
            )
      )
   ));
    acf_register_block_type(array(
         'name'              => 'Landing hero',
         'title'             => __('Landing hero'),
         'description'       => __('Hero section with two columns(text - image : image - text) '),
         'render_template'   => 'template-parts/blocks/landing-hero/landing-hero.php',
         'category'          => 'sections',
         'mode' => 'edit',
         'icon'              => 'list-view',
         'keywords'          => array( 'column, icon', 'list' ),
         'example'  => array(
					'attributes' => array(
					'data' => array(
						'__is_preview' => true
					),
					)
		)
   ));
	acf_register_block_type(array(
      'name'              => 'Large text',
      'title'             => __('Large text'),
      'description'       => __('Large text'),
      'render_template'   => 'template-parts/blocks/large-text/large-text.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      'data' => array(
         '__is_preview' => true
      ),
      ))
   ));
	acf_register_block_type(array(
      'name'              => 'Card icon',
      'title'             => __('Card icon'),
      'description'       => __('Card with icon and description'),
      'render_template'   => 'template-parts/blocks/card-icon/card-icon.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      'data' => array(
         '__is_preview' => true
      ),
      ))
   ));
	acf_register_block_type(array(
      'name'              => 'Testimonial slider',
      'title'             => __('Testimonial slider'),
      'description'       => __('Testimonial slider'),
      'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      
      'data' => array(
         '__is_preview' => true
      ),
      )
		)
   ));
	acf_register_block_type(array(
      'name'              => 'Cta',
      'title'             => __('Cta'),
      'description'       => __('Cta red and cta blue.'),
      'render_template'   => 'template-parts/blocks/cta/cta.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      'data' => array(
         '__is_preview' => true
      ),
      )
		)
   ));
   acf_register_block_type(array(
      'name'              => 'Hero',
      'title'             => __('Hero'),
      'description'       => __('Hero section'),
      'render_template'   => 'template-parts/blocks/hero/hero.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      'data' => array(
         '__is_preview' => true
      ),
      )
		)
   ));
   acf_register_block_type(array(
      'name'              => 'Two columns',
      'title'             => __('Two columns'),
      'description'       => __('Two column'),
      'render_template'   => 'template-parts/blocks/two-column/two-column.php',
      'category'          => 'sections',
      'mode' => 'edit',
      'icon'              => 'list-view',
      'keywords'          => array( 'column, icon', 'list' ),
      'example'  => array(
      'attributes' => array(
      'data' => array(
         '__is_preview' => true
      ),
      )
		)
   ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
   add_action('acf/init', 'register_acf_block_types');
}

// Acf options 
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}