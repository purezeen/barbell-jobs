<?php
// Custom login styles 
function my_custom_login() {
    // echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-style.css" />';
}
add_action('login_head', 'my_custom_login');

// Change the Login Logo URL
function custom_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'custom_login_logo_url' );

function custom_login_logo_url_title() {
    return 'Default Site Title';
}
add_filter( 'login_headertitle', 'custom_login_logo_url_title' );


// Register form
function custom_registration_function() {

    global $password, $email, $first_name, $last_name, $role;
    if (isset($_POST['submit'])) {
        registration_validation(
        $_POST['password'],
        $_POST['email'],
        $_POST['fname'],
        $_POST['lname'],
        $_POST['role']
		);
		
		// sanitize user form input
       
        $password 	= 	esc_attr($_POST['password']);
        $email 		= 	sanitize_email($_POST['email']);
        $first_name = 	sanitize_text_field($_POST['fname']);
        $last_name 	= 	sanitize_text_field($_POST['lname']);
        $role 	    = 	$_POST['role'];

		// call @function complete_registration to create the user
		// only when no WP_error is found
        complete_registration(
        $password,
        $email,
        $first_name,
        $last_name,
        $role
		);
    }

    registration_form($password, $email, $first_name, $last_name);
}

function registration_form( $password, $email, $first_name, $last_name ) {
    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
        <div class="login-username">
            <input type="text" name="email" value="' . (isset($_POST['email']) ? $email : null) . '" placeholder="Email">
        </div>
        <div class="login-password">
            <input type="password" name="password" value="' . (isset($_POST['password']) ? $password : null) . '" placeholder="Password">
        </div>
        <div class="form-col2">
            <div class="login-input">
                <input type="text" name="fname" value="' . (isset($_POST['fname']) ? $first_name : null) . '" placeholder="First Name">
            </div>
            <div class="login-input">
                <input type="text" name="lname" value="' . (isset($_POST['lname']) ? $last_name : null) . '" placeholder="Last Name">
            </div>
        </div>
        <p class="login-label">Select Account Type</p>
        <div class="switch-2">
            <label for="candidate" class="switch-2__container">
                <input type="radio" value="candidate" name="role" id="candidate" required>
                <span class="switch-2__checkmark">Coach</span>
            </label>
            <label for="employer" class="switch-2__container"> 
                <input type="radio" value="employer" name="role" id="employer" required>
                <span class="switch-2__checkmark">Employer</span>
            </label>
        </div>
        <div class="login-submit">
            <input type="submit" name="submit" value="Create account"/>
            <input type="hidden" name="csrf" value="'.wp_create_nonce('bbj_user_register').'"/>
        </div>
        <p class="login-link"> Already have an account?<a href="/login/"><strong> Log in!</strong></a></p>
	</form>
	';
}

function registration_validation(  $password, $email,  $first_name, $last_name )  {
    global $reg_errors;
    $reg_errors = new WP_Error;

    if ( empty( $password ) || empty( $email ) ) {
        $reg_errors->add('field', 'Required form field is missing');
    }

    if ( strlen( $password ) < 5 ) {
        $reg_errors->add('password', 'Password length must be greater than 5');
    }

    if ( !is_email( $email ) ) {
        $reg_errors->add('email_invalid', 'Email is not valid');
    }

    if ( email_exists( $email ) ) {
        $reg_errors->add('email', 'Email Already in use');
    }

    if ( empty( $first_name ) ) {
        $reg_errors->add('fname', 'First name field is missing');
    }

    if ( empty( $last_name ) ) {
        $reg_errors->add('lname', 'Last name field is missing');
    }

    if (!array_key_exists('csrf', $_POST) || !wp_verify_nonce($_POST['csrf'], 'bbj_user_register')) {
        $reg_errors->add('csrf', 'The system detected you as a spammer. Are we wrong? Contact us.');
    }

    if ( is_wp_error( $reg_errors ) ) {
        echo '<div class="form-error">';
        foreach ( $reg_errors->get_error_messages() as $error ) {
            echo '<p><strong>Error:</strong> ';
            echo $error . '</p>';
        }
        echo '</div>';
    }
}

function complete_registration() {
    global $reg_errors, $password, $email, $first_name, $last_name, $role;
    $username = strtolower(str_replace(' ', '', $first_name));
    $name_parts = explode(' ',$first_name);
    if ( count($reg_errors->get_error_messages()) < 1 ) {

        // Find an unused username
        $username_tocheck = $username;
        $i = 1;
        while ( username_exists( $username_tocheck ) ) {
            $username_tocheck = $username . $i++;
        }
        $username = $username_tocheck;

        $userdata = array(
        'user_login'    =>  $username,
        'user_email' 	=> 	$email,
        'user_pass' 	=> 	$password,
        'nickname'      =>  reset($name_parts),
        'first_name' 	=> 	reset($name_parts),
        'last_name' 	=> 	$last_name,
        'role'          =>  $role
		);
        $user = wp_insert_user( $userdata );
        if ( !is_wp_error($user) ) {
            $redirect_url = home_url('register-thank-you');
            echo "<script>location.href = '$redirect_url';</script>";
            exit;
        }
	}
}

// Auto login after registration 
function automatically_log_me_in( $user_id ) {
    wp_set_current_user( $user_id );
    wp_set_auth_cookie( $user_id );
    wp_redirect( home_url( 'register-thank-you' ) );
    exit(); 
}
add_action( 'user_register', 'automatically_log_me_in' );




/* prevent redirect on login error in a custom login page */
/* prevent redirect on login error in a custom login page */
 
add_filter('login_redirect', 'choose_login_redirect', 10, 3);

function choose_login_redirect($redirect_to, $requested_redirect_to, $user) {

if (is_wp_error($user)) {
	//Login failed, find out why...
	$error_types = array_keys($user->errors);
	//Error type seems to be empty if none of the fields are filled out
	$error_type = 'both_empty';
	//Otherwise just get the first error (as far as I know there will only ever be one)
	if (is_array($error_types) & !empty($error_types)) {
	$error_type = $error_types[0];
	} 
	// Url of our custom login page
	wp_redirect( home_url() . "/login/?login=failed&reason=" . $error_type );
	exit;
	}
}

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect( $redirect_to, $user ) {
    //is there a user to check?
    global $user;
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
            return home_url();
        } else if ( in_array( 'employer', $user->roles ) ) {
            return home_url('browse-candidate');
        } else if ( in_array( 'candidate', $user->roles ) ) {
            return home_url('candidate-dashboard');
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

add_action('after_setup_theme' , function() {
    if( $_SERVER['REQUEST_URI'] == '/register' && is_user_logged_in() ) {
        wp_redirect('/');
    }
});