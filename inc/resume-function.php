<?php

// Debug function
// if (!function_exists('get_field')) {
// 	function get_field() {
// 		return false;
// 	}
// }
// if (!function_exists('update_field')) {
// 	function update_field() {
// 		return false;
// 	}
// }
// if (!function_exists('delete_field')) {
// 	function delete_field() {
// 		return false;
// 	}
// }

/**
 * Check the current user role, and redirect it to a specific pages
 * @param string|array $roles;
 * @param int $redirect;
 */
function checkRoleAndRedirect($roles, int $redirect = 0)
{
	// Redirect candidates to specific pages
	$user = wp_get_current_user();
	$userMeta = get_userdata($user->ID);
	$userRoles = $userMeta->roles;

	if (!is_array($roles)) {
		$roles = [$roles];
	}

	$access = false;
	foreach ($roles as $role) {
		if (in_array($role, $userRoles)) {
			$access = true;
		}
	}

	if (!$access) {
		if ($redirect > 0) {
			$location = get_permalink($redirect);
			if (wp_redirect($location)) {
				echo '<script>location.href = "'.$location.'";</script>';
				exit();
			}
		}

		return false;
	}

	return true;
}

/** Add body classes depending on user roles */
add_filter('body_class', function($classes)
{
	$user = wp_get_current_user();
	$userMeta = get_userdata($user->ID);

	if (isset($userMeta->roles) && $userRoles = $userMeta->roles) {
		if (is_array($userRoles)) {
			$classes = array_merge($classes, $userRoles);
		}
	}

    return $classes;
});


/**
 * Check if a user has a role
 * @param string $role
 * @param mixed $user
 */
function hasRole(string $role = '', $user = false)
{
	// If not passed any user, use the current one
	if (!$user) {
		$user = wp_get_current_user();
	}

	return (isset($user->caps[$role]) && $user->caps[$role]) ? true : false;
}

// Add resume style
function resumeStyles() {
    wp_register_style('resume_dashboard_style', get_stylesheet_directory_uri().'/css/resume/dashboard.css', '1.0.0');
}
add_action('wp_enqueue_scripts', 'resumeStyles');

// Add resume script
function resumeScripts() {
    wp_register_script('resume_dashboard_script', get_stylesheet_directory_uri().'/js/resume/dashboard.js', ['jquery'], '1.0.1', true);
}  
add_action('wp_enqueue_scripts', 'resumeScripts');

/**
 * Register endpoints for resume
 * @endpoint /wp-json/candidate/{uid}/resume
 */
function registerResumeEndpoints()
{
	register_rest_route( 
		'candidate',
		'/(?P<uid>\d+)/resume',
		[
			'methods' => 'POST, GET',
			'callback' => 'rfResume',
			'permission_callback' => '__return_true',
            'args' => [
                'uid' => [
                    'validate_callback' => function($param) {
                        return is_numeric($param);
                    }
                ]
            ]
		]
	);
	
	// Form
	register_rest_route( 
		'candidate',
		'/(?P<uid>\d+)/resume/(?P<type>[a-z0-9-_]+)/form',
		[
			'methods' => 'POST',
			'callback' => 'rfForm',
			'permission_callback' => '__return_true',
            'args' => [
                'uid' => [
                    'validate_callback' => function($param) {
                        return is_numeric($param);
                    }
                ],
                'type' => [
                    'validate_callback' => function($param) {
                        return preg_replace("/[^a-z0-9-_]+/", '', $param);
                    }
                ]
            ]
		]
	);
	
	// Update
	register_rest_route( 
		'candidate',
		'/(?P<uid>\d+)/resume/update/(?P<type>[a-z0-9-_]+)',
		[
			'methods' => 'POST, GET',
			'callback' => 'rfUpdate',
			'permission_callback' => '__return_true',
            'args' => [
                'uid' => [
                    'validate_callback' => function($param) {
                        return is_numeric($param);
                    }
                ],
				'type' => [
					'validate_callback' => function($param) {
						return preg_replace("/[^a-z0-9-_]+/", '', $param);
					}
				]
            ]
		]
    );
	
	// Remove
	register_rest_route(
		'candidate',
		'/(?P<uid>\d+)/resume/remove/(?P<type>[a-z0-9-_]+)/(?P<selector>[a-z0-9-_]+)',
		[
			'methods' => 'POST, GET',
			'callback' => 'rfRemove',
			'permission_callback' => '__return_true',
            'args' => [
                'uid' => [
                    'validate_callback' => function($param) {
                        return is_numeric($param);
                    }
                ],
				'type' => [
					'validate_callback' => function($param) {
						return preg_replace("/[^a-z0-9-_]+/", '', $param);
					}
				],
				'selector' => [
					'validate_callback' => function($param) {
						return preg_replace("/[^a-z0-9-_]+/", '', $param);
					}
				]
            ]
		]
    );
}
add_action('rest_api_init', 'registerResumeEndpoints');

/**
 * Returns the candidate resume - If not exists create one
 * @param WP_REST_Request|mixed $args
 */
function rfResume(WP_REST_Request $request)
{
    // Get user id from the request
    $uid = $request->get_param('uid');
    $user = get_user_by('id', $uid);

	// Get resume
	$resume = getResume($user);
	
	return [
		'data' => $resume
	];
}

/**
 * Returns the candidate resume - If not exists create one
 * @param WP_User $user
 */
function getResume($user)
{
	// Check for user resume
	$resumes = get_posts([
		'author' =>  $user->ID,
		'post_type' => 'resume',
		'post_status' => 'any',
		'posts_per_page' => -1
	]);

	if (count($resumes)) {
		$res = reset($resumes);
		$rid = $res->ID;
	} else {
		// Create resume
		$rid = createResume($user);
	}
	
	// Get resume
	$resume = $rid ? get_post($rid) : false;

	// Debug
	$resume->meta = get_post_meta($rid);

	return $resume;
}

/**
 * Returns the candidate resume - check if it is publish or draft
 * @param WP_User $user
 */
function getResumeStatus($user)
{
	// Check for user resume
	$resumes = get_posts([
		'author' =>  $user->ID,
		'post_type' => 'resume',
		'post_status' => 'publish',
		'posts_per_page' => -1
	]);

	if (count($resumes)) {
		$resume_status = 1;
	} else {
		// Create resume
		$resume_status = 0;
	}

	return $resume_status;
}

/**
 * Check user resume role
 * @param User $user
 * @param Post $resume
 */
function getResumeUserRole($user, $resume)
{
	// Check user resume role 
	$resume_role = $user && isset($user->ID) ? get_posts([
		'author' => $user->ID,
		'role' => 'candidate',
		'post_type' => 'resume',
		'post_status' => 'any',
		'post__in' => array($resume->ID),
		'posts_per_page' => -1
	]) : [];

	return is_array($resume_role) && count($resume_role) ? 1 : 0;
}

/**
 * Create a resume
 * @param WP_User $user
 * @param array $meta
 * 
 * @return int|false
 */
function createResume(WP_User $user, array $meta = [])
{
    // Create resume
    $rid = wp_insert_post([
        'post_author' => $user->ID,
        'post_title' => $user->data->display_name,
        'post_type' => 'resume',
        'post_status' => 'draft' // On create the status needs to be 'draft'
    ]);

	// Add custom meta
    if ($rid) {
        // update_post_meta(int $post_id, string $meta_key, mixed $meta_value, mixed $prev_value = '')
        update_post_meta($rid, '_candidate_email', $user->user_email);
        if (is_array($meta) && !empty($meta)) {
            foreach ($meta as $key => $value) {
                update_post_meta($rid, $key, $value);
            }
        }
    }
    
    return $rid;
}

/**
 * Create a resume
 * @param int $rid
 * @param array $data
 * @param array $meta
 * @param string $edit
 * 
 * @return int|false
 */
function updateResume(int $rid, array $data = [], array $meta = [], $edit = -1)
{
	// Update resume
	$resume = get_post($rid);

	// Check for required files (by then we change the post status)
	$status = $resume->post_status;
	foreach ($data as $key => $value) {
		if (in_array($key, ['post_content'])) {
			$status = 'pending';
		}
		$data[$key] = nl2br($data[$key]);
	}

	// Check for required files (by then we change the post status)
	foreach ($meta as $key => $value) {
		if (in_array($key, ['_candidate_photo', '_candidate_name', '_resume_content', '_job_qa_1', '_job_qa_2', '_job_qa_3', '_job_qa_4', '_job_qa_5', '_job_qa_6', '_job_qa_7', '_job_qa_8', '_job_qa_9', '_job_qa_10'])) {
			$status = 'pending';
		}
	}

    if (is_array($data) && !empty($data)) {
        $data['ID'] = $rid;
        $data['post_status'] = $status; // On update we change the resume status to 'pending'
        wp_update_post($data);
    }

    // Update custom meta
    // update_post_meta(int $post_id, string $meta_key, mixed $meta_value, mixed $prev_value = '')
	// update_field('key', 'value', 'post_id')
    if ($rid && is_array($meta) && !empty($meta)) {
        foreach ($meta as $key => $value) {

			// Check if the value is array
			if (is_array($value)) {

				// Update the old value
				$append = true;
				$newValue = [];
				$oldValue = get_field($key, $rid);

				if (is_array($oldValue)) {
					foreach ($oldValue as $k => $oldV) {
						if (is_array($oldV) && !empty($oldV)) {

							// Remove the old value elem if the edit flag is not false
							if ($edit >= 0 && $edit == $k) {
								$newValue[] = $value;
								$append = false;
							} else {
                                $newValue[] = $oldV;
                            }
						}
					}
				}

				// Append a new value
                if ($append) {
                    $newValue[] = $value;
                }
				$value = $newValue;
			}

			// Doa ctions by the key
			switch ($key) {
				case 'work_experience':
					updateSkills($rid, $value);
					break;
			}

            // update_post_meta($rid, $key, $value);
			update_field($key, ($value?: ''), $rid);
        }
    }

    return $rid;
}

/**
 * Returns the candidate resume - If not exists create one
 * @param WP_REST_Request|mixed $args
 */
function rfForm(WP_REST_Request $request)
{
    // Get user id from the request
    $uid = $request->get_param('uid');
    $user = get_user_by('id', $uid);

	// Get resume
	$resume = getResume($user);

   // Get the type from the request
   $type = $request->get_param('type');
	$action = "/wp-json/candidate/{$uid}/resume/update/{$type}";
	$form = [
		'header' => '<h3>Edit your '.str_replace('_', ' ', $type).'</h3>',
		'content' => '',
		'footer' => '
			<input type="hidden" name="csrf" form="resumeForm" value="'.wp_create_nonce('bbj_'.$uid.'_resume').'"/>
			<button type="submit" form="resumeForm" class="btn btn--primary js-submit">Save</button>
			<span class="btn btn--ghost js-close">Cancel</span>
		'
	];

    // Get the query/post paramethers
	$getData = $request->get_query_params();
	$edit = false;
	$key = -1;
	if (array_key_exists('key', $getData) && $getData['key'] > $key) {
		$key = $getData['key'];
		$editData = get_field($type, $resume->ID);
		if (is_array($editData) && array_key_exists($key, $editData)) {
			$edit = $editData[$key];
		}
	}

	switch ($type) {
		case 'profile':
			$img = ( get_field('_candidate_photo', $resume->ID) == null )? '<img src=" '.get_template_directory_uri() .'/img/profile-none.png" alt="avatar" class="avatar">' : '<img src="'. get_field('_candidate_photo', $resume->ID) .'" alt="'. get_field('_candidate_name', $resume->ID).'" class="avatar" width="80" height="80">';

			$form['content'] = '
				<div class="modal__avatar">
					<div class="modal__avatar-img">
						' . $img . '

					</div>
					<label class="btn btn--medium btn--border">
						Add avatar *
						<input form="resumeForm" type="file" name="avatar" class="candidate-avatar hidden" style="display: none;" data-image-selector=".avatar" data-form-selector=".candidate-photo" data-type="image" data-id="'.$resume->ID.'">
					</label>
					<p>The photo must be a headshot of your face. Supported formats are PNG, JPG up to 2MB with at least 256x256px.
</p>
				</div>
				<form action="'.$action.'" method="post" class="candidate-profile" id="resumeForm">
					<input type="hidden" name="meta[_candidate_photo]" class="candidate-photo" value="'.get_field('_candidate_photo', $resume->ID).'">
					<div class="form-col2">
						<label class="form-col">Full name *
							<input name="meta[_candidate_name]" type="text" value="'.get_field('_candidate_name', $resume->ID).'" required>
						</label>
						<label class="form-col">Your location *
							<input name="meta[_candidate_location]" type="text" value="'.get_field('_candidate_location', $resume->ID).'" required>
						</label>
					</div>
					<label class="form-col">About you *
						<textarea name="meta[_resume_content]" style="min-height: 180px; width: 100%;" required>'.get_field('_resume_content', $resume->ID).'</textarea>
					</label>
					<div class="form-col2">
						<div class="form-col">
							<label class="switch">
								<input name="meta[_location_remote]" type="checkbox" value="1" '.(get_field('_location_remote', $resume->ID) ? 'checked' : '').'>
								<span class="switch__slider round"></span>Open to Remote
							</label>
						</div>
						<div class="form-col">
							<label class="switch">
								<input name="meta[_location_relocate]" type="checkbox" value="1" '.(get_field('_location_relocate', $resume->ID) ? 'checked' : '').'>
								<span class="switch__slider round"></span>Open to Relocation
							</label>
						</div>
						<div class="form-col">
							<label class="switch">
								<input name="meta[_military_veteran]" type="checkbox" value="1" '.(get_field('_military_veteran', $resume->ID) ? 'checked' : '').'>
								<span class="switch__slider round"></span>Military Veteran
							</label>
						</div>
					</div>
					<div class="form-col3">
						<label class="form-col">LinkedIn profile URL
							<input name="meta[_candidate_linkedin]" type="text" value="'.get_field('_candidate_linkedin', $resume->ID).'">
						</label>
						<label class="form-col">Twitter profile URL
							<input name="meta[_candidate_twitter]" type="text" value="'.get_field('_candidate_twitter', $resume->ID).'">
						</label>
						<label class="form-col">Instagram profile URL
							<input name="meta[_candidate_instagram]" type="text" value="'.get_field('_candidate_instagram', $resume->ID).'">
						</label>
					</div>
				</form>
			';
			break;
		case 'work_experience':
			$form['content'] = '
				<form action="'.$action.'" method="post" id="resumeForm">
					<label class="form-col">Job title
						<input name="meta['.$type.'][title]" type="text" value="'.(is_array($edit) && array_key_exists('title', $edit) ? $edit['title'] : '').'" required>
					</label>
					<label class="form-col">Employer
						<input name="meta['.$type.'][employer]" type="text" value="'.(is_array($edit) && array_key_exists('employer', $edit) ? $edit['employer'] : '').'" required>
					</label>
					<div class="form-col2">
						<label class="form-col">Start date
							<input name="meta['.$type.'][start_date]" type="date" value="'.(is_array($edit) && array_key_exists('start_date', $edit) ? $edit['start_date'] : '').'" required>
						</label>
						<label class="form-col">End date
							<input name="meta['.$type.'][end_date]" type="date" value="'.(is_array($edit) && array_key_exists('end_date', $edit) ? $edit['end_date'] : '').'">
						</label>
					</div>
					<label class="form-col">Description
						<textarea name="meta['.$type.'][description]" style="min-height: 180px;" required>'.(is_array($edit) && array_key_exists('description', $edit) ? $edit['description'] : '').'</textarea>
					</label>
					<label class="form-col">Skills
						<input type="text" name="meta['.$type.'][skills]" value="'.(is_array($edit) && array_key_exists('skills', $edit) ? $edit['skills'] : '').'" pattern="^[a-zA-Z0-9 ,-_]*$" title="Comma separated skills - Only letter, numbers, space, and "_,-" allowed.">
					</label>
				</form>
			';
			break;

		case 'education':
			$form['content'] = '
				<div class="modal__pdf">
					<label class="custom-file-upload">
						Add certificate
						<input form="resumeForm" type="file" name="certificate" class="certificate" data-image-selector="" data-form-selector=".candidate-certificate" data-type="file" data-id="'.$resume->ID.'">
					</label>
					<p>Certificate description...</p>
				</div>
				<form action="'.$action.'" method="post" id="resumeForm">
					<input type="hidden" name="meta['.$type.'][certificate]" class="candidate-certificate" value="'.(is_array($edit) && array_key_exists('certificate', $edit) ? $edit['certificate'] : '').'">
					<label class="form-col">Qualification
						<input name="meta['.$type.'][qualification]" type="text" value="'.(is_array($edit) && array_key_exists('qualification', $edit) ? $edit['qualification'] : '').'" required>
					</label>
					<label class="form-col">School name
						<input name="meta['.$type.'][school]" type="text" value="'.(is_array($edit) && array_key_exists('school', $edit) ? $edit['school'] : '').'" required>
					</label>
					<div class="form-col2">
						<label class="form-col">Start date
							<input name="meta['.$type.'][start_date]" type="date" value="'.(is_array($edit) && array_key_exists('start_date', $edit) ? $edit['start_date'] : '').'" required>
						</label>
						<label class="form-col">End date
							<input name="meta['.$type.'][end_date]" type="date" value="'.(is_array($edit) && array_key_exists('end_date', $edit) ? $edit['end_date'] : '').'" required>
						</label>
					</div>
					<label class="form-col">Description
						<textarea name="meta['.$type.'][description]" style="min-height: 180px;" required>'.(is_array($edit) && array_key_exists('description', $edit) ? $edit['description'] : '').'</textarea>
					</label>
				</form>
			';
			break;

		case 'additional_skills':
			$form['content'] = '
				<form action="'.$action.'" method="post" id="resumeForm">
					<label class="form-col">Additional skill
						<input name="meta['.$type.'][title]" type="text" value="'.(is_array($edit) && array_key_exists('title', $edit) ? $edit['title'] : '').'" required>
					</label>
					<label class="form-col">Description
						<textarea name="meta['.$type.'][description]" style="min-height: 180px;" required>'.(is_array($edit) && array_key_exists('description', $edit) ? $edit['description'] : '').'</textarea>
					</label>
				</form>
			';
			break;

		case 'biography':
			$form['content'] = '
				<form action="'.$action.'" method="post" id="resumeForm">
					<label class="form-col">Short biography
						<textarea name="data[post_content]" style="min-height: 180px; width: 100%;">'.$resume->post_content.'</textarea>
					</label>
				</form>
			';
			break;

		case 'summary':
			$summaryQuestions = getSummaryQuestions();
			$form['content'] = '
				<form action="'.$action.'" method="post" id="resumeForm">';
					foreach ($summaryQuestions as $k => $value) {
						$form['content'] .= '<label class="form-col">'.$value.' *
							<textarea name="meta['.$k.']" style="min-height: 80px;" required>'.get_field($k, $resume->ID).'</textarea>
						</label>';
					}
				$form['content'] .= '</form>
			';
			break;

		case 'languages':
			$form['content'] = '
				<form action="'.$action.'" method="post" id="resumeForm">
					<label class="form-col">Language
						<input name="meta['.$type.'][language]" type="text" value="'.(is_array($edit) && array_key_exists('language', $edit) ? $edit['language'] : '').'" required>
					</label>
					<label class="form-col">Level (1-5)
						<input name="meta['.$type.'][level]" type="number" min="1" max="5" value="'.(is_array($edit) && array_key_exists('level', $edit) ? $edit['level'] : 1).'" required>
					</label>
				</form>
			';
			break;
		
		default:
			$form['content'] = '<p>'.__('Something went wrong, please try again later.', 'barbell-jobs').'</p>';
			break;
	}

	if ($edit > -1) {
		$form['content'] .= '<input name="edit" form="resumeForm" value="'.$key.'" type="hidden">';
	}
	$form['url'] = "/wp-json/candidate/{$uid}/resume/update/{$type}";

	return [
		'data' => $form
	];
}

/**
 * Returns the candidate resume - If not exists create one
 * @param WP_REST_Request|mixed $args
 */
function rfUpdate(WP_REST_Request $request)
{
	// Prepair response data
	$response = [];

    // Get the query/post paramethers
	// $getData = $request->get_query_params();
	$postData = $request->get_body_params();

    // Get data from the request
    $type = $request->get_param('type');
    $uid = $request->get_param('uid');
    $user = get_user_by('id', $uid);

	// Get values
	parse_str(urldecode($postData['form']), $queryData);

	// Validate the post request
    if (!array_key_exists('csrf', $queryData) || !wp_verify_nonce($queryData['csrf'], 'bbj_'.$uid.'_resume')) {
        return [
			'error' => 'The system detected you as a spammer. Are we wrong? Contact us.'
		];
    }

	if (is_array($queryData) && count($queryData)) {
		$resumeData = array_key_exists('data', $queryData) ? $queryData['data'] : [];
		$resumeMeta = array_key_exists('meta', $queryData) ? $queryData['meta'] : [];

		// Modify the metadata array
		$resumeMeta = modifyDataByType($resumeMeta, $type);

		// Debug
		$response['debug'] = [
			'queryData' => $queryData,
			'resumeData' => $resumeData,
			'resumeMeta' => $resumeMeta
		];

		// Update resume
		$resume = getResume($user);
		$edit = array_key_exists('edit', $queryData) ? $queryData['edit'] : -1;

		$result = updateResume($resume->ID, $resumeData, $resumeMeta, $edit);
		// return['data' => $result];

		// Update resume status
		wp_update_post([
			'ID' => $resume->ID,
			'post_status' => 'pending' // 'draft'
		]);

		// Get the new updated resume
		$resume = getResume($user);
		$response['data'] = getResponse($resume, [$type], $uid);
	}
	
	return $response;
}

/**
 * Modify post data by type
 * @param array $data
 * @param string $type
 * 
 * @return array $data
 */
function modifyDataByType(array $data, string $type)
{
	// Modify data by type
	switch ($type) {
		case 'profile':
				// Fix the checkbox issue
				$checkboxes = [
					'_location_relocate',
					'_location_remote',
					'_military_veteran'
				];
				foreach ($checkboxes as $name) {
					$data[$name] = isset($data[$name]) && $data[$name] ? 1 : 0;
				}
			break;
	}

	return $data;
}

/**
 * Load a template part but not print it
 * @param atring $url
 * @param atring $name
 * @param array $args
 */
function loadTemplatePart(string $url, string $name = null, array $args = []) {
	ob_start();
	get_template_part($url, $name, $args);
	$var = ob_get_contents();
	ob_end_clean();

	return $var;
}

/**
 * Return a response
 * @param WP_Post $resume
 * @param array $types
 * 
 * @return array $response
 */
function getResponse(WP_Post $resume, array $types, int $uid = 0) {
	// Set response array
	$types[] = 'guide';
	$types[] = 'profile';
	$response = [];

	// Add other response data
	$types = array_unique($types);
	foreach ($types as $type) {
		$response[] = [
			'selector' => '.js-'.$type,
			'content' => loadTemplatePart('template-parts/resume/'.$type, ucfirst($type), [
				'resume' => $resume,
				'uid' => $uid,
				'candidate' => true
			])
		];
	}

	return $response;
}

/**
 * Image upload handler
 */
function fileUploadHandler()
{
    $response = [
		'status' => 'error'
	];
  
    // Check if the file is available && the user is logged in
    if (!empty($_FILES[0])) {
		$response = [];
		$rid = array_key_exists('rid', $_POST) && $_POST['rid'] ? intval($_POST['rid']) : 0;
		$type = array_key_exists('type', $_POST) && $_POST['type'] ? intval($_POST['type']) : 'file';

        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');
       
        // Handle the media upload using WordPress helper functions
        $attachmentId = media_handle_upload(0, $rid);
        $response['aid'] = $attachmentId;
        
        // If error
        if (is_wp_error($attachmentId)) {
            $response['error'] = "Error.";
        } else {
            // Generate attachment in the media library
            $attachmentFilePath = get_attached_file($attachmentId);
            wp_generate_attachment_metadata($attachmentId, $attachmentFilePath);
            
            // Get the attachment entry in media library
            $response['src'] = wp_get_attachment_url($attachmentId);
            switch ($type) {
				case 'image':
					$imageThumbAttributes = wp_get_attachment_image_src($attachmentId, 'avatar');
					if (is_array($imageThumbAttributes) && count($imageThumbAttributes)) {
						$response['src'] = $imageThumbAttributes[0];
					}
					break;
			}

            $response['status'] = 'ok';
            $response['type'] = $type;
        }
	}

    // Output the json
    die(json_encode($response));
}
add_action('wp_ajax_file_upload', 'fileUploadHandler');

/**
 * Returns the candidate resume - If not exists create one
 * @param WP_REST_Request|mixed $args
 */
function rfRemove(WP_REST_Request $request)
{
	// Prepair response data
	$response = [];

    // Get the query/post paramethers
	$getData = $request->get_query_params();

    // Get data from the request
    $uid = $request->get_param('uid');
    $user = get_user_by('id', $uid);
    $type = $request->get_param('type');
	$selector = $request->get_param('selector');
	$resume = getResume($user);
	$rid = $resume->ID;

	// Debug
	$response['debug'] = [
		'getData' => $getData,
		'rid' => $rid,
		'type' => $type,
		'selector' => $selector,
		'rid' => $rid
	];

	$value = false;
	switch ($type) {
		case 'work_experience':
		case 'education':
		case 'additional_skills':
		case 'languages':
			$value = get_field($type, $rid);
            if (is_array($value)) {
                unset($value[$selector]);
			}

			// Update the resume skills
            if ($type == 'work_experience') {
                updateSkills($rid, $value);
            }

			break;
		case 'summary':
			update_field($selector, '', $rid);
			break;
	}

	// Update / delete resume
	delete_field($type, $rid);
	if ($value) {
		update_field($type, $value, $rid);
	}

	// Get the new updated resume
	$resume = getResume($user);
	$response['data'] = getResponse($resume, [$type], $uid);

	return $response;
}

/**
 * Update post tags
 * @param int $id
 * @param array $value
 */
function updateSkills(int $id, array $value)
{
	// Remove all tags
	$tags = [];
	$terms = wp_get_post_terms($id, 'resume_skill');
	foreach($terms as $tag) {
		$tags[] = $tag->term_id;
	}

	// wp_remove_object_terms( int $object_id, string|int|array $terms, array|string $taxonomy )
	wp_remove_object_terms($id, $tags, 'resume_skill');

	foreach ($value as $data) {
		if (array_key_exists('skills', $data)) {
			// wp_set_post_terms( int $post_id, string|array $tags = '', string $taxonomy = 'post_tag', bool $append = false )
			wp_set_post_terms($id, $data['skills'], 'resume_skill', true);
		}
	}
}

/**
 * Return the summary questions array
 * @return array
 */
function getSummaryQuestions()
{
	return [
        '_job_qa_1' => 'What are you most passionate about in your work? What motivates you?',
        '_job_qa_2' => 'Are you willing to relocate?',
        '_job_qa_3' => 'Which coaching skills do you possess that set you apart from other coaches?',
        '_job_qa_4' => 'Where do you see yourself in your career in 5 years?',
        '_job_qa_5' => "Tell us about a coach you've worked with who has mentored you or a coach you've learned from online.",
        '_job_qa_6' => 'What are your values as a coach that differentiate you from others?
		  ',
        '_job_qa_8' => 'Which certification or seminar has had the biggest impact on your coaching skills?',
        '_job_qa_9' => 'Have you completed the American Strength Club Powerlifting L1?',
        '_job_qa_9' => 'What Specialty Programs can you create for your new facility?',
        '_job_qa_10' => 'Are you a military veteran?'
    ];
}

/**
 * Return edit/delete actions
 */
function resumeActions(int $uid, string $type, string $key)
{
	?>
	<span class="js-open-modal" data-url="/wp-json/candidate/<?php echo $uid; ?>/resume/<?php echo $type; ?>/form?key=<?php echo $key; ?>"><?php _e('Edit', 'barbell-jobs'); ?></span>
	<span class="js-ajax more-dots__popup-text-red" data-url="/wp-json/candidate/<?php echo $uid; ?>/resume/remove/<?php echo $type; ?>/<?php echo $key; ?>"><?php _e('Delete', 'barbell-jobs'); ?></span>
	<?php
}

/**
 * Check for job_checkout POST request
 */
function jobCheckout() {
	// Set location
	$location = false;
	if (array_key_exists('job_checkout', $_POST)) {
		// Remove all items from the cart
		global $woocommerce;
		$woocommerce->cart->empty_cart();

		// Get data from the POST request
		$products = $_POST['product'];
		$jobId = $_POST['job_id'];

		// Add new items
        if (is_array($products)) {
            foreach ($products as $pid) {
                $woocommerce->cart->add_to_cart($pid, 1);
            }

            // Set location
            $location = wc_get_checkout_url().'?job_id='.$jobId;
        }
	}

	// If the current page is cart page, and there are products in the cart, redirect the user to the checkout page, else to the home page
	$params = explode('/', $_SERVER['REQUEST_URI']);
    if (in_array('cart', $params)) {

		// Set location - Post a job id
		$location = get_permalink(5);
	}

	// Check if there is job_id on checkout page 
    if (in_array('checkout', $params)) {
		if (!array_key_exists('job_id', $_GET)) {

			// Set location - Post a job id
			// $location = get_permalink(5);

		}
	}

	// Redirect
	if (wp_redirect($location)) {
		echo '<script>location.href = "'.$location.'";</script>';
		exit();
	}
}
add_action('init', 'jobCheckout');

/**
 * Insert a checkout button
 * @param array|bool $attr 
 * [checkout_button product_id=INTEGER* job_id=INTEGER label="STRING" css_class="STRING"]
 */
function checkoutButtonFunction($attr)
{
	$response = '';
	if (array_key_exists('product_id', $attr) && $productId = (int) $attr['product_id']) {
		$label = array_key_exists('label', $attr) && $attr['label'] ? $attr['label'] : __('Checkout', 'barbell_jobs');
		$cssClass = array_key_exists('css_class', $attr) && $attr['css_class'] ? $attr['css_class'] : 'btn btn--primary';
		$jobId = array_key_exists('job_id', $attr) && $attr['job_id'] ? (int) $attr['job_id'] : 0;
		$http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
		$url = "{$http}://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";
		$response .= '
			<form method="POST" action="'.htmlspecialchars($url).'">
				<input name="job_checkout" value="true" type="hidden">
				<input name="product[]" value="'.$productId.'" type="hidden">
				<input name="job_id" value="'.$jobId.'" type="hidden">
				<button class="'.$cssClass.'" type="submit">'.$label.'</button>
			</form>
		';
	}

	return $response;
}
add_shortcode('checkout_button', 'checkoutButtonFunction'); 


/**
 * Add job_id field to the checkout form
 */
function add_job_id_field_after_billing_address () {
	?>
	<input type="hidden" name="job_id" value="<?php echo $_GET['job_id']; ?>">
  	<?php 
}
add_action( 'woocommerce_after_checkout_billing_form', 'add_job_id_field_after_billing_address' , 10, 1 );


/**
 * Add job_id meta field to the order
 */
function add_job_id_to_order ($order_id)
{
	if (isset($_POST ['job_id']) &&  $_POST['job_id']) {
		add_post_meta($order_id, '_job_id',  sanitize_text_field($_POST['job_id']));
	}
}
add_action('woocommerce_checkout_update_order_meta', 'add_job_id_to_order' , 10, 1);

/**
 * On order processing 
 */
function statusProcessing($order_id)
{
	// Get order by id
	$order = new WC_Order($order_id);

	// Update job meta and status
	$jobId = get_post_meta($order_id, '_job_id')[0];
	$job = get_post($jobId);

	// Add this order id to the job
	$append = false;
	$allowedOrdersArray = [];
	$allowedOrders = get_field('allowed_orders', $jobId);

	// If the allowed orders is empty, add default value
	if (empty($allowedOrders) ) {
		$allowedOrders = json_encode([]);
	}

	// Add this order id to allowed orders array
	$allowedOrdersArray = json_decode($allowedOrders);
	if (!in_array($jobId, $allowedOrdersArray)) {
		$allowedOrdersArray[] = $order_id;
		$append = true;
	}

	// Update the allowed orders meta tag with the updated value
	update_field('allowed_orders', json_encode($allowedOrdersArray), $jobId);

	// Update the job status - Admins can see as 'pending' job at the admin panes
	if ($job->post_status != 'publish') {
		$data = [
			'ID' => $jobId,
			'post_status' => 'pending' //draft
		];
		wp_update_post($data);
	}

	// Check for append days
	$days = 0;
	if ($append && $exd = get_field('_featured_listing_expired_date', $jobId)) {
		$d = strtotime($exd);
		$n = time();
		$datediff = $d - $n;
		if ($datediff > 0) {
			$plusDays = round($datediff / (60 * 60 * 24));
			$days += $plusDays;
		}
	}

	// Check for products
	$items = $order->get_items();
	foreach ($items as $item) {
		$productId = $item->get_product_id();

		// Set dates
		$jobExpires = 30;
		switch ($productId) {
			// Basic Promotion Package
			case 548:
				//Appear in job listings for 90 days
				$jobExpires = 90;
				break;
			// Advanced Promotion Package
			case 547:
				// Appear in job listings for 90 days
				$jobExpires = 90;

				// Frontpage featured for 7 days
				$days += 7;
				update_field('_featured', true, $jobId);
				break;
			// Best Promotion Package
			case 511:
				// Appear in job listings for 90 days
				$jobExpires = 90;

				// Frontpage featured for 7 days
				$days += 7;
				update_field('_featured', true, $jobId);

				// Staff pick flag
				update_field('_job_staffpick', true, $jobId);
				break;
		}
	}

	// Update the job _job_expires and _featured_listing_expired_date meta tags
	update_field('_job_expires', date('Y-m-d', strtotime("+{$jobExpires} day")), $jobId);
	update_field('_featured_listing_expired_date', date('Y-m-d', strtotime("+{$days} day")), $jobId);
}
add_action('woocommerce_order_status_processing', 'statusProcessing', 10, 1);
add_action('woocommerce_order_status_completed', 'statusProcessing', 10, 1);

/** 
 * Remove the featured flag if expired
 */
function removeFeaturedIfExpired($jobId)
{
	if ($exd = get_field('_featured_listing_expired_date', $jobId)) {
		$currentTime = time();
		$expireDate = strtotime($exd);
		$datediff = $expireDate - $currentTime;
		$days = round($datediff / (60 * 60 * 24));

		if ($days <= 0) {
			update_field('_featured', false, $jobId);
		}
	}
}

/** 
 * Remove the staff pick flag if expired
 */
function removeStaffPickIfExpired($jobId)
{
	if ($exd = get_field('_featured_listing_expired_date', $jobId)) {
		$currentTime = time();
		$expireDate = strtotime($exd);
		$datediff = $expireDate - $currentTime;
		$days = round($datediff / (60 * 60 * 24));

		if ($days <= 0) {
			update_field('_featured', false, $jobId);
		}
	}
}

/** Detect post load */
function detectPostLoad()
{
	if (get_post_type() == 'job_listing') {
		removeFeaturedIfExpired(get_the_ID());
		removeStaffPickIfExpired(get_the_ID());
	}
}
add_action('wp', 'detectPostLoad');

/**
 * Return the booster packages
 * @param bool $onlyWithPrice
 */
function getBoosterPackages($onlyWithPrice = false)
{
	// Get booster products
	$boostProducts = wc_get_products([
		'category' => ['boost'],
	]);

	// Create the response
	$response = '<h2 class="post-job-packages__title">'.__('Booster types', 'barbell-jobs').'</h2>';
	if (is_array($boostProducts)) {
		foreach ($boostProducts as $key => $boostProduct) {
			if ($boostProduct->is_purchasable()) {
				$id = $boostProduct->get_id();
				$name = $boostProduct->get_name();
				$price = wc_price($boostProduct->get_price(), ['decimals' => 0]);
				$image = wp_get_attachment_image_src(get_post_thumbnail_id($boostProduct->get_id()));

				// If we need the packages only with price
				if ($onlyWithPrice) {
					$key--;
					if ($boostProduct->get_price() < 1) {
						continue;
					}
				}
				$response .= '
					<label class="product-item product-item__booster '.(!$key ? 'input-selected' : '').'" for="product-'.$id.'">
						<img src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" class="product-image product-image__booster">
						<div class="product-name product-name__booster">
							'.$name.'
							<div class="product-item__booster-short-description">
								<p>'.$boostProduct->short_description.'</p>
							</div>
						</div>
						<div class="product-price">
							'.$price.'
							<div class="radio-container">
								<input type="checkbox" name="product[]" value="'.$id.'" id="product-'.$id.'" data-name="'.$name.'" data-price="'.$boostProduct->get_price().'" '.(!$key ? 'checked' : '').'/>
								<span class="radio-checkmark"></span>
							</div>
						</div>
					</label>
				';
			}
		}
	} else {
		$response .= '<p>'.__('No packages found', 'wp-job-manager-wc-paid-listings').'</p>';
	}

	return $response;
}
