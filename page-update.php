<?php get_header(); ?>

<?php 
   if (!defined('ABSPATH')) {
      exit; // Exit if accessed directly.
   }

   require_once(ABSPATH . 'wp-admin/includes/media.php');
   require_once(ABSPATH . 'wp-admin/includes/file.php');
   require_once(ABSPATH . 'wp-admin/includes/image.php');
?>

<div style="padding-bottom: 45px;"></div>
<ol>
   <li>Fetch all jobs</li>
   <li>Get jobs meta tags</li>
   <li>Update the necessary tags</li>
   <li>Get the old id</li>
   <li>Fetch user by old id</li>
   <li>Update the author field</li>
   <li>Save the job</li>
</ol>
<hr />
<ol>
   <li>Ako _job_expires ima vrednost update-ujem vreme;</li>
   <li>Ako _frontend_expires ima vrednost, update-ujem vreme i takođe update-ujem _featured_listing_expired_date;</li>
   <li>Ako postoji vrednost za _featured_listing_expired_date update-ujem datum i ako je datum veći od današnjeg čekiram _featured; </li>
   <li>Za _job_company_email dodam default-ni barbell jobs email ako _application_url nema neki externi URL;</li>
   <li>Promenim _job_author na osnovu employer_id_canidate_id meta value-a;</li>
   <li>Ako postoji meta tag company_logo uploadujem sliku na server i dodam kao featured image.</li>
</ol>
<?php
   if (isset($_GET['from'], $_GET['to'])) {
       $jobs = get_posts([
         'post_type' => 'job_listing',
         'post_status' => 'all',
         'numberposts' => -1
         // 'order'    => 'ASC'
      ]);

       print 'Jobs count: '.count($jobs);

       /**
        * Validate date
       * @param bool|string $date
       */
       function validateDate($date)
       {
           if (!empty($date)) {
               if (strlen($date) > 10 || strpos($date, 'UTC') !== false) {
                   $date = date("Y-m-d", strtotime($date));
               }

               return $date;
           }

           return '';
       }

       $jobWithMetas = [];
       $i = $_GET['from'];
       foreach ($jobs as $key => $job) {
           if ($i > $_GET['to']) {
               break;
           }
           $i++;

           $jobId = $job->ID;
           $job->meta = get_post_meta($jobId);
           $jobWithMetas[] = $job;

           // Fetch all necessary data from the job object
           $_featured_listing_expired_date = get_field('_featured_listing_expired_date', $jobId);
           $_featured = get_field('_featured', $jobId);
           $_frontend_expires = get_field('_frontend_expires', $jobId);
           $_job_expires = get_field('_job_expires', $jobId);
           $_application_url = get_field('_application_url', $jobId);
           $employer_id_canidate_id = get_field('employer_id_canidate_id', $jobId);
           $company_logo = get_field('company_logo', $jobId);

           // Udate dates
           $_featured_listing_expired_date_updated = validateDate($_featured_listing_expired_date);
           $_frontend_expires_updated = validateDate($_frontend_expires);
           $_job_expires_updated = validateDate($_job_expires);

           update_field('_featured_listing_expired_date', $_featured_listing_expired_date_updated, $jobId);
           update_field('_frontend_expires', $_frontend_expires_updated, $jobId);
           update_field('_job_expires', $_job_expires_updated, $jobId);

           // Update the _job_expires if it is empty by the _featured_listing_expired_date_updated
           if (empty($_job_expires_updated) &&
            !empty($_featured_listing_expired_date_updated) &&
            strlen($_featured_listing_expired_date_updated) > 8) {
               update_field('_job_expires', validateDate($_featured_listing_expired_date_updated), $jobId);
           }

           // If it is a featured job, update the _frontend_expires by the _featured_listing_expired_date
           // Or the current date + 30 days
           if ($_featured) {
               $maxDate = date('Y-m-d', strtotime("+30 day"));
               $expires = (strtotime($_featured_listing_expired_date_updated) > strtotime($maxDate)) ? $maxDate : $_featured_listing_expired_date_updated;
               update_field('_frontend_expires', $expires, $jobId);
           } else {
               // Else delte them
               update_field('_frontend_expires', date('Y-m-d', strtotime("-1 day")), $jobId);
               update_field('_featured_listing_expired_date', '', $jobId);
           }

           // DEBUG
           $_featured_updated = ($datediff > 0) ? 'true' : 'false';

           // Validate the application URL
           if (empty($_application_url) || strpos($_application_url, 'barbelljobs.com') !== false) {
               // If the _application_url is empty or contains barbelljobs.com
               // Remove the _application_url meta tag
               update_field('_application_url', '', $jobId);

               // Set the job_application_tform value for _application_type
               update_field('_application_type', 'job_application_tform', $jobId);
           } else {
               // Set the job_application_turl for _application_type
               update_field('_application_type', 'job_application_turl', $jobId);
           }

           // DEBUG
           $_application_url_updated = $_application_url;
           if (empty($_application_url) || strpos($_application_url, 'barbelljobs.com') !== false) {
               $_application_url_updated = '';
               $_application_type_updated = 'job_application_tform';
           } else {
               $_application_type_updated = 'job_application_turl';
           }

           // Find the job author and update the autor id
           $args = [
            'meta_key' => 'employer_id_canidate_id',
            'meta_value' => $employer_id_canidate_id,
         ];
           $users = get_users($args);
           if (count($users)) {
               $user = $users[0];

               // Update the job
               update_field('_job_author', $user->ID, $jobId);
               wp_update_post([
               'ID' => $jobId,
               'post_author' => $user->ID
            ]);
           }

           // Update the company logo
           if ($company_logo) {

            // Change the src to the new src
               $company_logo_updated = get_template_directory_uri().'/logos/'.basename($company_logo);

               $desc = $job->post_title;
               $image = media_sideload_image($company_logo_updated, $jobId, $desc, 'id');
               set_post_thumbnail($jobId, $image);

               // // Unset the company_logo meta field
            // update_field('company_logo', $company_logo, $jobId);
           }

         // Update post status 
           $expireDate = strtotime(get_field('_job_expires', $jobId));
           $currentDate = strtotime(date('Y-m-d'));
           if ($expireDate > $currentDate) {
               wp_update_post([
                  'ID' => $jobId,
                  'post_status' => 'publish'
               ]);
           }

           

           print '<pre>';
           print_r([
            'title' => $job->post_title,
            'id' => $job->ID,
            '_featured_listing_expired_date' => $_featured_listing_expired_date,
            '_frontend_expires' => $_frontend_expires,
            '_job_expires' => $_job_expires,
            '_featured_listing_expired_date_updated' => $_featured_listing_expired_date_updated,
            '_frontend_expires_updated' => $_frontend_expires_updated,
            '_job_expires_updated' => $_job_expires_updated,
            '_featured_updated' => $_featured_updated,
            '_application_url' => $_application_url,
            'employer_id_canidate_id' => $employer_id_canidate_id,
            'company_logo' => $company_logo,
            '_application_url_updated' => $_application_url_updated,
            '_application_type_updated' => $_application_type_updated,
            'user_id' => $users[0]->ID,
            'company_logo_updated' => $company_logo_updated
            ]);
           print '</pre>';
      }
   } else {
      echo '<br /><br /><br /><br /><br />';
      echo '<h1>Please set the from and to values</h1>';
   }   
?>

<?php get_footer(); ?>
