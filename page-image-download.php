<?php get_header(); ?>
<script>
   function downloadBlob(blob, name = 'file.txt') {
      if (
      window.navigator && 
      window.navigator.msSaveOrOpenBlob
      ) return window.navigator.msSaveOrOpenBlob(blob);

      // For other browsers:
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(blob);

      const link = document.createElement('a');
      link.href = data;
      link.download = name;

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(
         new MouseEvent('click', { 
            bubbles: true, 
            cancelable: true, 
            view: window 
      })
      );

      setTimeout(() => {
         // For Firefox it is necessary to delay revoking the ObjectURL
         window.URL.revokeObjectURL(data);
         link.remove();
      }, 100);
   }
</script>
<?php
   if (isset($_GET['from'], $_GET['to'])) {
      $jobs = get_posts([
         'post_type' => 'job_listing',
         'post_status' => 'all',
         'numberposts' => -1
         // 'order'    => 'ASC'
      ]);

      print '<br /><br /><br /><br /><br /><br /><br />';
      print 'Jobs count: '.count($jobs);

      $i = $_GET['from'];
      foreach ($jobs as $key => $job) {
         if ($i > $_GET['to']) {
            break;
         }
         $i++;

         if (has_post_thumbnail($job)) {
            echo '<img src="'.get_the_post_thumbnail_url($job).'" style="width:80px;height:auto;">';
            echo '<hr />';
         } else {
            $jobId = $job->ID;
            echo $i.' - ';
            echo $company_logo = get_field('company_logo', $jobId);
            echo '<a href="http://jobs.local/wp-admin/post.php?post='.$job->ID.'&action=edit">Edit</a>';
            echo '<hr />';

            // Download the company logo
            $company_logo_updated = __DIR__.'/logos/'.basename($company_logo);
            echo file_exists($company_logo_updated) ? 'YES' : 'NO';
            if ($company_logo && !file_exists($company_logo_updated)) {
               ?>
                  <script>
                  setTimeout(() => {
                     (function() {
                        var src = '<?php echo $company_logo; ?>';
                        fetch(src)
                           .then(res => res.blob()) // Gets the response and returns it as a blob
                           .then(blob => {

                              // Download
                              downloadBlob(blob, src.substring(src.lastIndexOf('/')+1));
                           });
                     })();
                  }, 860);
                  </script>
               <?php
            }
         }
      }
   } else {
      echo '<br /><br /><br /><br /><br />';
      echo '<h1>Please set the from and to values</h1>';
      echo '<p>?from=0&to=21</p>';
   }
?>

<?php get_footer(); ?>
