<?php get_header(); ?>
<?php global $post; ?>
<?php
while (have_posts()) : the_post(); ?>
    <style>
        .hero::before {background-image: url(<?php echo get_template_directory_uri(); ?>/img/hero-background.png);}
    </style>

    <section class="hero hero--single-layout cover">
        <div class="container">
            <a href="<?php echo home_url() . '/'; ?>" class="btn-link-back"><span class="arrow left"></span>To job listing</a>
            <div class="hero__content">
                
                <!-- Labels  -->
                <?php if(is_position_featured() == 1)  { ?><span class="label label--mb label--featured2">Featured</span> <?php } ?>
                <?php $staffpick = get_post_meta( $post->ID, '_job_staffpick', true );
                if ( $staffpick ): ?>
                    <span class="label label--mb label--featured">Staff pick</span>
                <?php endif; ?>
                <?php 
                $datetime1 = date_create( $post->post_date );
                $datetime2 = date_create(); // current date
                $interval = date_diff( $datetime1, $datetime2 );
                $days_old =  $interval->format( '%a' );
                if($days_old < 5) { ?>
                    <span class="label label--mb label--pick">New job</span>
                <?php } ?>

                <h1><?php the_title() ?></h1>

                <?php
                $job_perks = get_post_meta( $post->ID, '_job_perks', true );
                $job_perks_explode= explode(", ", $job_perks); ?>
                <div class="hero__tags">
                    <?php if(!empty($job_perks)): ?>
                        <?php
                            // Company tag lines
                            foreach ($job_perks_explode as $value) {
                                echo '<span class="tag">';
                                echo $value;
                                echo '</span>';
                            }
                        ?>
                    <?php endif; ?>
                    <?php
                    // $terms = get_the_terms( $post->ID, 'job_listing_category' );
                    // if($terms) {
                    //     foreach($terms as $term) {
                    //         echo '<span class="tag">' . $term->name . '</span>';
                    //     }
                    // }
                    ?>
                    <?php
                    // $job_types = get_the_terms( $post->ID, 'job_listing_type' );
                    // if($job_types) {
                    //     foreach($job_types as $job_type) {
                    //         echo '<span class="tag">' . $job_type->name . '</span>';
                    //     }
                    // } 
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php 

the_content();

endwhile; // End of the loop.
?>

<?php get_footer(); ?>