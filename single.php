<?php get_header(); //Template Name: Single ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

   <?php
   // Define user ID
   // Replace NULL with ID of user to be queried
   $user_id = get_the_author_meta( 'ID' );

   // Example: Get ID of current user
   // $user_id = get_current_user_id();

   // Define prefixed user ID
   $user_acf_prefix = 'user_';
   $user_id_prefixed = $user_acf_prefix . $user_id;
   ?>

   <!-- Hero single  -->
   <section class="single-header container page-light">
      <div class="single-header__title text-center">
         <span class="single-header__data">Published <?php echo get_the_date(); ?></span>
         <h1><?php the_title(); ?></h1>
      </div>
   </section>

   <div class="single-img container-wide">
      <?php if( has_post_thumbnail() ): ?>
         <?php the_post_thumbnail('blog-hero'); ?>
      <?php endif; ?>
   </div>

   <section class="single-inner container">
      <div class="single-main">
         <div class="editor">
            <?php the_content(); ?>
         </div>
            <div class="author author--large single-author border-dashed-top">
               <div class="author__img">
                  <?php echo get_avatar( $user_id , $size = 150 ); ?>
               </div>
               <div class="author__name">
                  <h6><?php echo get_the_author_meta('user_firstname'); ?> <?php echo get_the_author_meta('user_lastname'); ?></h6>
                  <span><?php the_field( 'profession', $user_id_prefixed ); ?></span>
                  <div class="author__description">
                     <p><?php echo get_the_author_meta('user_description'); ?></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="single-sidebar">
            <div class="social-icons social-icons--vertical social-icons--sticky">
               <?php echo do_shortcode('[addtoany]'); ?>
            </div>
         </div>
      </div>
   </section>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<!-- Related posts  -->
<?php $do_not_duplicate = $post->ID; ?>

<?php $args2 = array(
   'post_type' => 'post',
   'post_status' => 'publish',
   'orderby' => 'rand',
   'ignore_sticky_posts' => true,
   'post__not_in' => array($do_not_duplicate),
   'posts_per_page' => 3
); ?>

<?php $related_post = new WP_Query($args2); ?>

<?php if( $related_post->have_posts()): ?>

   <section class="section section-background-light border-dashed-bottom">
      <div class="container">
         <h2 class="section-title">Related posts</h2>
         <div class="blog-related">

            <?php while( $related_post->have_posts()) : $related_post->the_post(); ?>
               <a href="<?php the_permalink(); ?>" class="blog-card">
                
                     <?php if( has_post_thumbnail()): ?>
                        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
                        <div class="blog-card__img cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>)">
                        
                        </div>
                     <?php endif; ?>
                 
                  <div class="blog-card__content">
                     <span class="blog-card__date"><?php echo get_the_date(); ?></span>
                     <h3><?php the_title(); ?></h3>
                  </div>
                  <span class="blog-card__author"><?php echo get_the_author_meta('user_firstname'); ?> <?php echo get_the_author_meta('user_lastname'); ?></span>
               </a>
            <?php endwhile; ?>
            
         </div>
      </div>
   </section>
   <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php 
   // If comments are open or we have at least one comment, load up the comment template.
   if ( comments_open() || get_comments_number() ) :
      comments_template();
   endif;
?>


<?php get_footer(); ?>



