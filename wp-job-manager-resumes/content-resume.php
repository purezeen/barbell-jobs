<?php
/**
 * Template for resume content inside a list of resumes.
 *
 * This template can be overridden by copying it to yourtheme/wp-job-manager-resumes/content-resume.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager-resumes
 * @category    Template
 * @version     1.18.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $post;

// var_dump($post);
$category = get_the_resume_category(); ?>
<div <?php resume_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" >
	<a href="<?php the_resume_permalink(); ?>" class="profile-list">
		<div class="profile-list__img">
			<?php the_candidate_photo('thumbnail'); ?>
      </div>
	
		<div class="profile-list__content">
         <h2><?php echo get_field('_candidate_name', $post->ID); ?></h2>

			<ul class="tag-icons tag-icons--small unstyle-list profile-list__tag-icons">
					<li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon-grey.png" alt="location icon"></span><?php echo get_field('_candidate_location', $post->ID) ?: __('Add location', 'barbell-jobs'); ?></li>
					<li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/job-type-icon-grey.png" alt="job"></span>
						<?php 
						if (get_field('_location_remote', $post->ID) || get_field('_location_relocate', $post->ID)) {
							$preferences = '';
							if (get_field('_location_remote', $post->ID)) {
								$preferences .= __('Remote', 'barbell-jobs');
							}
							if (get_field('_location_remote', $post->ID) && get_field('_location_relocate', $post->ID)) {
								$preferences .= ', '.__('relocation', 'barbell-jobs');
							} elseif (get_field('_location_relocate', $post->ID)) {
								$preferences .= __('Relocation', 'barbell-jobs');
							}
							$preferences .= ' '.__('for the right position', 'barbell-jobs');

							echo $preferences;
						} else {
							_e('Add job preference', 'barbell-jobs');
						} ?>
					</li>
					<?php if (get_field('_military_veteran', $post->ID)): ?>
						<li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon-grey.png" alt="location"></span><?php _e('Military veteran', 'barbell-jobs'); ?></li>
					<?php endif; ?>
			</ul>

			<?php $term_list = wp_get_post_terms( $post->ID, 'resume_skill', array( 'fields' => 'all' ) ); ?>

			<?php if ($term_list) { ?>
				<div class="profile-list__tags"> 
					<?php foreach($term_list as $term) { ?>
						<span class="tag tag--dark-border tag--small"><?php echo $term->name; ?></span>
					<?php } ?>
				</div>
			<?php } ?>  

         <div class="profile-list__short-description">
				<p><?php echo $post->post_content; ?></p>
         </div>
      </div>
	</a>
</div>
<hr class="dashed">
