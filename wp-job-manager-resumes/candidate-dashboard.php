<?php
/**
 * Template for the candidate dashboard (`[candidate_dashboard]`) shortcode.
 *
 * This template can be overridden by copying it to yourtheme/wp-job-manager-resumes/candidate-dashboard.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager-resumes
 * @category    Template
 * @version     1.13.0
 */
if (!defined('ABSPATH')) {
	exit;
}

// checkRoleAndRedirect($roles="Employer", $redirect = "7");

wp_enqueue_script('resume_dashboard_script');
wp_enqueue_style('resume_dashboard_style');



$user = wp_get_current_user();
$resume = getResume($user);

$roles="employer";
$redirect = 85;

$user = wp_get_current_user();
$userMeta = get_userdata($user->ID);
$userRoles = $userMeta->roles;

if (!is_array($roles)) {
   $roles = [$roles];
}

$access = false;
foreach ($roles as $role) {
   if (in_array($role, $userRoles)) {
      $access = true;
   }
}

if ($access) {
   if ($redirect > 0) {
      $location = get_permalink($redirect);
      if (wp_redirect($location)) {
         echo '<script>location.href = "'.$location.'";</script>';
         exit();
      }
   }

   return false;
}

?>
<style> 
   .hero::before {
	   background-image: url('<?php echo get_template_directory_uri(); ?>/img/hero-background.png');
	}
</style>

<section class="hero hero--profile-layout cover">
   <div class="hero__guide hero__guide-list--center">
      <div class="container js-guide">
         <?php get_template_part('template-parts/resume/guide', 'Guide', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>
   </div>
   <div class="container js-profile">
      <?php get_template_part('template-parts/resume/profile', 'Profile', [
         'resume' => $resume,
         'uid' => $user->ID,
         'candidate' => true
      ]); ?>
   </div>
   <div class="container" style="justify-content: flex-end;">
      <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/profile/form"><?php _e('Edit profile', 'barbell-jobs'); ?></span>
   </div>
</section>

<!-- Banner  -->
<!--Display banner if resume is draft -->
<?php if(getResumeStatus($user) == 0): ?>
   <div class="container banner banner--position-top">
      <div class="banner__content">
         <div class="banner__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/alert.png" alt="profile"></div>
         <div class="banner__content-inner">
            <p>Your profile isn’t visible to others since it needs admin approval.</p>
         </div>
      </div>
   </div>
<?php endif; ?>

<section class="profile-section section">
   <div class="container">
      <div class="profile-section__header">
         <h4 class="profile-section__title"><?php _e('Work experience', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data">
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/work_experience/form"><?php _e('Add experience', 'barbell-jobs'); ?></span>
         </div>
      </div>
      <div class="js-work_experience">
         <?php get_template_part('template-parts/resume/work_experience', 'Work experience', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>
   </div>
</section>
<section class="profile-section section-background-light section">
   <div class="container">
      <div class="profile-section__header">
         <h4 class="profile-section__title"><?php _e('Education and certification', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data">
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/education/form"><?php _e('Add education', 'barbell-jobs'); ?></span>
         </div>
      </div>

      <div class="js-education">
         <?php get_template_part('template-parts/resume/education', 'Education', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>

   </div>
</section>

<section class="profile-section section">
   <div class="container">
      <div class="profile-section__header">
         <h4 class="profile-section__title"><?php _e('Additional skills', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data tooltip">
            <span class="tooltip__text">Please add one additional skill at a time. After you add the text, click Save, and then repeat the process.</span>
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/additional_skills/form"><?php _e('Add skill', 'barbell-jobs'); ?></span>
         </div>
      </div>
      <div class="js-additional_skills">
         <?php get_template_part('template-parts/resume/additional_skills', 'Additional skills', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>
   </div>
</section>

<section class="section profile-short-bio section-background-blue">
   <div class="container">
      <div class="profile-section__header">
         <h4 class="profile-section__title profile-short-bio__label"><?php _e('Short biography', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data">
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/biography/form"><?php $resume->post_content ? _e('Edit biography', 'barbell-jobs') : _e('Add biography', 'barbell-jobs'); ?></span>
         </div>
      </div>
      <div class="js-biography">
         <?php get_template_part('template-parts/resume/biography', 'Biography', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]);  ?>
      </div>
   </div>
</section>

<section class="section profile-summary">
   <div class="container counter--reset">
      <div class="profile-section__header">
         <h4 class="profile-section__title"><?php _e('Summary', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data">
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/summary/form"><?php _e('Edit summary', 'barbell-jobs'); ?></span>
         </div>
      </div>
      <div class="js-summary">
         <?php get_template_part('template-parts/resume/summary', 'Summary', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>
   </div>
</section>

<section class="section-pb">
   <div class="container">
      <div class="profile-section__header">
         <h4 class="profile-section__title"><?php _e('Languages', 'barbell-jobs'); ?></h4>
         <div class="profile-btn-no-data">
            <span class="btn btn--small btn--border js-open-modal" data-url="/wp-json/candidate/<?php echo $user->ID; ?>/resume/languages/form"><?php _e('Add language', 'barbell-jobs'); ?></span>
         </div>
      </div>  
      <div class="js-languages">
         <?php get_template_part('template-parts/resume/languages', 'Languages', [
            'resume' => $resume,
            'uid' => $user->ID,
            'candidate' => true
         ]); ?>
      </div>

   </div>
</section>

<?php get_template_part('template-parts/modal', 'Modal'); ?>

<?php get_footer(); ?>
