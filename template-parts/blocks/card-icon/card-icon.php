<?php 
if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/box-icon.png" alt="box icon">
   <?php return;
}
?>

<?php if ( have_rows( 'card_icon' ) ) : ?>

   <section class="container section-large-pb">
      <div class="box-icon-wrap">

      <?php while ( have_rows( 'card_icon' ) ) : the_row(); ?>

         <div class="box-icon">
            <?php $icon = get_sub_field( 'icon' ); ?>
            <?php if ( $icon ) { ?>
               <div class="box-icon__img">
                  <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
               </div>
            <?php } ?>
            <div class="box-icon__content">
               <?php the_sub_field( 'card_description' ); ?>
            </div>
         </div>
         
      <?php endwhile; ?>
      
      </div>
   </section>

<?php endif; ?>