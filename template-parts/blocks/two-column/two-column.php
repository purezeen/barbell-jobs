<?php 
if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/two-columns.png" alt="two-columns">
   <?php return;
}
?>

<?php if ( have_rows( 'two_columns' ) ) : ?>
   <section class="two-columns container section-large-pt section-large-pb">

      <?php while ( have_rows( 'two_columns' ) ) : the_row(); ?>

         <div class="two-column <?php echo ( get_sub_field( 'column_reverse' ) == 1 )? 'two-column--reverse' : ''; ?>">
            <div class="two-column__text">
               <?php the_sub_field( 'column_text' ); ?>
            </div>
            <div class="two-column__image">
               <?php $column_image = get_sub_field( 'column_image' ); ?>
               <?php if ( $column_image ) { ?>
                  <img src="<?php echo $column_image['url']; ?>" alt="<?php echo $column_image['alt']; ?>" />
               <?php } ?>
              
            </div>
         </div>
      <?php endwhile; ?>

   </section>
<?php endif; ?>
