<?php 

if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/landing-hero.png" alt="landing page">
   <?php return;
}
?>

<style> .home-hero::before {background-image: url(<?php echo get_template_directory_uri();  ?>/img/hero-background.png);} </style>

<section class="home-hero section-large-mb cover">
   <div class="container">

      <?php if ( have_rows( 'hero_with_two_columns_text_and_image' ) ) : ?>
         <?php while ( have_rows( 'hero_with_two_columns_text_and_image' ) ) : the_row(); ?>

            <div class="home-hero__content">
               <?php the_sub_field( 'description' ); ?>

               <?php $link1 = get_sub_field( 'link1' ); ?>
               <?php if ( $link1 ) { ?>
                  <a href="<?php echo $link1['url']; ?>" target="<?php echo $link1['target']; ?>" class="btn btn--primary" <?php echo ( get_sub_field( 'btn-nofollow' ) ) ? 'rel="nofollow"' : ''; ?>><?php echo $link1['title']; ?></a>
               <?php } ?>
            </div>

            <div class="home-hero__img">
               <?php $image1 = get_sub_field( 'image1' ); ?>

               <?php if ( $image1 ) { ?>
                  <div class="home-hero__img-wrap">
                  <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
                  </div>
               <?php } ?>
            </div>

         <?php endwhile; ?>
      <?php endif; ?>

      <?php if ( have_rows( 'hero_with_two_columns_text_and_image_2' ) ) : ?>
         <?php while ( have_rows( 'hero_with_two_columns_text_and_image_2' ) ) : the_row(); ?>
            <div class="home-hero__img">
               <?php $portrait_image = get_sub_field( 'portrait_image' ); ?>
               <?php if ( $portrait_image ) { ?>
                  <div class="home-hero__img-wrap">
                     <img src="<?php echo $portrait_image['url']; ?>" alt="<?php echo $portrait_image['alt']; ?>" />
                  </div>
               <?php } ?>
            </div>
            <div class="home-hero__content home-hero__content--large-p">
               <?php the_sub_field( 'description2' ); ?>
               <?php $link2 = get_sub_field( 'link2' ); ?>
               <?php if ( $link2 ) { ?>
                  <a href="<?php echo $link2['url']; ?>" target="<?php echo $link2['target']; ?>" class="btn btn--primary" <?php echo ( get_sub_field( 'btn-nofollow2' ) ) ? 'rel="nofollow"' : ''; ?>><?php echo $link2['title']; ?></a>
               <?php } ?>
            </div>
         <?php endwhile; ?>
      <?php endif; ?>
      
   </div>
</section>