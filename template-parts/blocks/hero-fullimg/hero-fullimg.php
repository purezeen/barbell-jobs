<?php 

if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/barbell-heroabout.png" alt="hero">
   <?php return;
}
?>

<?php $hero_background = get_field( 'hero_background' ); ?>
<?php if ( $hero_background ) { ?>
	<?php $background = $hero_background['url']; ?>
   <?php } ?>
<style>
   .hero-about::before {background-image:url(<?php echo $hero_background['url']; ?>);opacity: 0.6;}
</style>

<section class="hero-about hero-about--move-img">
   <div class="container">
      <div class="hero-about__content">
         <?php the_field( 'hero_title' ); ?>
      </div>
      <?php $hero_image = get_field( 'hero_image' ); ?>
      <?php if ( $hero_image ) { ?>
         <div class="hero-about__img hero-about--border-top">
            <img src="<?php echo $hero_image['url']; ?>" alt="<?php echo $hero_image['alt']; ?>" />
         </div>
      <?php } ?>
   </div>
</section>