
<?php if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/large-text.png" alt="large text">
   <?php return;
} ?>

<section class="container large-text section-medium-pb">
   <?php the_field( 'large_text' ); ?>
</section>