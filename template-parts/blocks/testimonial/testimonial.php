<?php if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/testimonial.png" alt="quote">
   <?php return;
}?>

<?php if ( have_rows( 'testimonial' ) ) : ?>
   <section class="container section-large-pb">
      <h3 class="section-title-small"><?php the_field( 'section_title' ); ?></h3>
         <div class="slick-container">
            <div class="slick-slider slider">
               <?php $count = 0; ?>
               <?php while ( have_rows( 'testimonial' ) ) : the_row(); ?>
                  <div>
                     <div class="slider__item quote-wrap quote-wrap--large">
                        <div class="quote quote--large">
                           <blockquote><?php the_sub_field( 'quote' ); ?></blockquote>
                           <div class="quote-author">

                              <?php $author_image = get_sub_field( 'author_image' ); ?>
                              <?php if ( $author_image ) { ?>
                                 <div class="quote-author__img">
                                    <img src="<?php echo $author_image['sizes']['thumbnail']; ?>" alt="<?php echo $author_image['alt']; ?>" />
                                 </div>
                              <?php } ?>

                              <div class="quote-author__content">
                                 <h6><?php the_sub_field( 'author_name' ); ?></h6>
                                 <span><?php the_sub_field( 'short_description' ); ?></span>
                              </div>
                              <?php $count++; ?>
                             
                           </div>
                        </div>
                     </div>
                  </div>
               <?php endwhile; ?>
            </div>
            <?php if($count > 1): ?>
               <div class="slider-controls">
                  <div class="slider-controls__btn slider-controls__btn--prev"><span class="arrow left"></span></div>
                  <div class="slider-controls__btn slider-controls__btn--next"><span class="arrow right"></span></div>
               </div>
            <?php endif; ?>
      </div>
   </section>
<?php endif; ?>
