<?php 

if( !empty( $block['data']['__is_preview'] )) { ?>
   <img src="<?php echo get_template_directory_uri() ?>/img/hero.png" alt="landing page">
   <?php return;
}
?>


<?php $hero_background = get_field( 'hero_background' ); ?>
<?php if ( $hero_background ) { ?>
   <?php $hero_background_image = $hero_background['url']; ?>
<?php } ?>

<style>
.hero::before {
   background-image: url(<?php echo $hero_background_image; ?>);
}
</style>

<section class="hero hero--main">
   <div class="container">
      <div class="hero__content">
         <div class="hero__content-inner">
            <?php the_field( 'hero_content' ); ?>
               <?php $button = get_field( 'button' ); ?>
               <?php if ( $button ) { ?>
                  <a href="<?php echo $button['url']; ?>" <?php echo ( get_field( 'add_no-follow' ) == 1 ) ? 'no-follow' : ''; ?> class="btn btn--primary" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
               <?php } ?>
         </div>
      </div>
   </div>
</section>