<?php 
   if( !empty( $block['data']['__is_preview'] )) { ?>
      <img src="<?php echo get_template_directory_uri() ?>/img/cta-examples.png" alt="cta-examples">
      <?php return;
   }
?>

<?php if ( get_field( 'add_cta_inside_blog' ) == 1 ) { 
   $cta_inner = 'cta--inner';
   $cta_button_light_inner = "btn--light";
} else { 
   $cta_inner = 'container section-large-mb';
   $cta_button_light_inner = "";
} ?>

<?php if ( have_rows( 'cta_style' ) ) : ?>
	<?php while ( have_rows( 'cta_style' ) ) : the_row(); ?>

		<?php if (get_sub_field( 'cta_background' ) == "blue") { ?>
         <?php $cta_blue = 'cta--blue'; ?>
         <?php $cta_button_primary = "btn--primary"; ?>
         <?php $cta_button_light = ""; ?>
      <?php } else { ?>
         <?php $cta_blue = ''; ?>
         <?php $cta_button_light = "btn--light"; ?>
         <?php $cta_button_primary = ""; ?>
      <?php }?>

      <?php if (get_sub_field( 'cta_size' ) == "large") { ?>
         <?php $cta_large = 'cta--large'; ?>
      <?php } else { ?>
         <?php $cta_large = ''; ?>
      <?php }?>

	<?php endwhile; ?>
<?php endif; ?>

<?php if ( get_field( 'cta_button_-_large_width' ) == 1 ) { 
   $cta_button_width = "btn--wide";
} else { 
   $cta_button_width = "";
} ?>

<?php 
if ( get_field( 'hide_button1_from_canidate' ) == 1 ) { 
   $hide_button1_candidate = 'hide-for-candidate';
} else { 
   $hide_button1_candidate = '';
} 

if ( get_field( 'hide_button2_from_candidate2' ) == 1 ) { 
   $hide_button2_candidate = 'hide-for-candidate';
  } else { 
   $hide_button2_candidate = '';
} 
?>

<article class="cta <?php echo $cta_inner; ?> <?php echo $cta_large; ?> <?php echo $cta_blue; ?> ">
   <div class="cta__content">
      <?php the_field( 'cta_content' ); ?>
      <?php  $cta_button = get_field( 'cta_button' ); ?>

      <?php if ( get_field( 'add_second_button' ) == 1 ) {  ?>
        
      <div class="cta__button-group">
         <?php if ( $cta_button ) { ?>
         
            <a href="<?php echo $cta_button['url']; ?>" <?php echo ( get_field( 'cta_button_-_add_nofollow' ) == 1 ) ? "rel='nofollow'" : ""; ?> class="btn  <?php echo $hide_button1_candidate; ?> <?php echo $cta_button_light_inner; ?> <?php echo $cta_button_light; ?> <?php echo $cta_button_primary; ?> <?php echo $cta_button_width; ?>"  target="<?php echo $cta_button['target']; ?>"><?php echo $cta_button['title']; ?></a>
         <?php } ?>
         
         <?php $cta_button2 = get_field( 'cta_button2' ); ?>
         <?php if ( $cta_button2 ) { ?>
            <a <?php echo ( get_field( 'cta_button_-_add_nofollow' ) == 1 ) ? "rel='nofollow'" : ""; ?> href="<?php echo $cta_button2['url']; ?>" class="btn <?php echo $hide_button2_candidate; ?> <?php echo $cta_button_light_inner; ?>" target="<?php echo $cta_button2['target']; ?>"><?php echo $cta_button2['title']; ?></a>
         <?php } ?>

      </div>
      <?php } else { ?>
         <?php if ( $cta_button ) { ?>
            <a href="<?php echo $cta_button['url']; ?>" <?php echo ( get_field( 'cta_button_-_add_nofollow' ) == 1 ) ? "rel='nofollow'" : ""; ?> class="btn <?php echo $hide_button1_candidate; ?> <?php echo $cta_button_light_inner; ?> <?php echo $cta_button_light; ?> <?php echo $cta_button_primary; ?> <?php echo $cta_button_width; ?>"  target="<?php echo $cta_button['target']; ?>"><?php echo $cta_button['title']; ?></a>
         <?php } ?>
      <?php } ?>

   </div>
   <div class="cta__logo">
      <img src="<?php echo get_template_directory_uri(); ?>/img/barbell-jobs-white.svg" alt="barbell jobs logo">
   </div>
</article>
