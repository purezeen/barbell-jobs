<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php if ($additional_skills = get_field('additional_skills', $resume->ID)): ?>
        <div class="profile-skill-box-wrap">
            <?php foreach ($additional_skills as $key => $additional_skill): ?>
                <div class="profile-skill-box more-dots-wrap">
                    <?php if (array_key_exists('candidate', $args) && $args['candidate']): ?>
                        <div class="more-dots">
                            <div class="dots"><span></span><span></span><span></span></div>
                            <div class="more-dots__popup" style="display: none;">
                                <?php resumeActions($args['uid'], 'additional_skills', $key); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (array_key_exists('title', $additional_skill) && $title = $additional_skill['title']): ?>
                        <h2><?php echo $title; ?></h2>
                    <?php endif;?>
                    <?php if (array_key_exists('description', $additional_skill) && $description = $additional_skill['description']): ?>
                        <p><?php echo nl2br($description); ?></p>
                    <?php endif;?>
                </div>
            <?php endforeach;?>
        </div>
    <?php else: ?>
        <div class="profile-no-data-icon" style="text-align: center;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/no-data-icon.png" alt="no-data">
        </div>
    <?php endif;?>
<?php endif;?>
