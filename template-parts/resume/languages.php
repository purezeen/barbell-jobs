<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php if ($languages = get_field('languages', $resume->ID)): ?>
        <div class="profile-lang">
            <?php foreach ($languages as $key => $language): ?>
                <div class="profile-lang__box more-dots-wrap">
                    <?php if (array_key_exists('candidate', $args) && $args['candidate']): ?>
                        <div class="more-dots">
                            <div class="dots"><span></span><span></span><span></span></div>
                            <div class="more-dots__popup" style="display: none;">
                                <?php resumeActions($args['uid'], 'languages', $key); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="profile-lang__title">
                        <?php if (array_key_exists('language', $language) && $title = $language['language']): ?>
                            <h4><?php echo $title; ?></h4>
                        <?php endif;?>
                    </div>
                    <?php $level = (array_key_exists('level', $language) && intval($language['level'])) ? intval($language['level']) : 1; ?>

                    <div class="profile-lang__stars">
                        <?php for ($i=1; $i < 6; $i++): ?>
                            <span class="star <?php echo ($level >= $i) ? 'active' : ''; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/star-icon.<?php echo ($level >= $i) ? 'svg' : 'png'; ?>" alt="star"></span>
                        <?php endfor;?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    <?php else: ?>
        <div class="profile-no-data-icon" style="text-align: center;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/no-data-icon.png" alt="no-data">
        </div>
    <?php endif;?>
<?php endif;?>
