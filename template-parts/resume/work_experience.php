<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php if ($workExperience = get_field('work_experience', $resume->ID)): ?>
        <?php foreach ($workExperience as $key => $experience): ?>
            <div class="profile-row border-dashed-top" data-selector="<?php echo $key; ?>">
                <div class="profile__col profile-row__title">
                    <?php if (array_key_exists('title', $experience) && $title = $experience['title']): ?>
                        <h2><?php echo $title; ?></h2>
                    <?php endif;?>
                    <?php if (array_key_exists('employer', $experience) && $employer = $experience['employer']): ?>
                        <h3><?php echo $employer; ?></h3>
                    <?php endif;?>
                </div>
                <div class="profile__col profile-row__content more-dots-wrap">
                    <?php if (array_key_exists('candidate', $args) && $args['candidate']): ?>
                        <div class="more-dots">
                            <div class="dots"><span></span><span></span><span></span></div>
                            <div class="more-dots__popup" style="display: none;">
                                <?php resumeActions($args['uid'], 'work_experience', $key); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (array_key_exists('start_date', $experience) && $start = $experience['start_date']): ?>
                        <p><strong><?php echo date('Y M. d', strtotime($start)); ?> - <?php echo array_key_exists('end_date', $experience) ? date('Y M. d', strtotime($experience['end_date'])) : __('Now', 'barbell-jobs'); ?></strong></p>
                    <?php endif;?>
                    <?php if (array_key_exists('description', $experience) && $description = $experience['description']): ?>
                        <p><?php echo nl2br($description); ?></p>
                    <?php endif;?>
                    <div>
                    <?php if (array_key_exists('skills', $experience) && $skills = $experience['skills']): ?>
                        <?php foreach (explode(',', $skills) as $skill): ?>
                            <span class="tag tag--dark-border"><?php echo $skill; ?></span>
                        <?php endforeach; ?>
                    <?php endif;?>
                    </div>
                </div>

            </div>
        <?php endforeach;?>
    <?php else: ?>
        <div class="profile-no-data-icon" style="text-align: center;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/no-data-icon.png" alt="no-data">
        </div>
    <?php endif;?>
<?php endif;?>
