
<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php  $resume_content = $resume->post_content; ?>
    <?php if(empty($resume_content)): ?>
        <p>Add a short biography to describe your skills and competencies. This is what employers will look at. </p>
    <?php else: ?>
        <p><?php echo nl2br($resume_content); ?></p>
    <?php endif; ?>
    <?php if (!array_key_exists('candidate', $args)): ?>
        <!-- <hr class="dashed-light">
        <span><?php //sprintf(__("View %s’s resume in PDF format", 'barbell-jobs'), get_field('_candidate_name', $resume->ID)); ?> </span>
         <a href="#" class="btn btn--primary btn--download"><?php //_e('Download resume', 'barbell-jobs'); ?></a> -->
    <?php endif; ?>
<?php endif;?>
