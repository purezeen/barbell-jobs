<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php if ($educations = get_field('education', $resume->ID)): ?>
        <?php foreach ($educations as $key => $education): ?>
            <div class="profile-row border-dashed-top" data-selector="<?php echo $key; ?>">
                <div class="profile__col profile-row__title">
                    <?php if (array_key_exists('qualification', $education) && $qualification = $education['qualification']): ?>
                        <h2><?php echo $qualification; ?></h2>
                    <?php endif;?>
                    <?php if (array_key_exists('school', $education) && $school = $education['school']): ?>
                        <h3><?php echo $school; ?></h3>
                    <?php endif;?>
                </div>
                <div class="profile__col profile-row__content more-dots-wrap">
                    <?php if (array_key_exists('candidate', $args) && $args['candidate']): ?>
                        <div class="more-dots">
                            <div class="dots"><span></span><span></span><span></span></div>
                            <div class="more-dots__popup" style="display: none;">
                                <?php resumeActions($args['uid'], 'education', $key); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (array_key_exists('start_date', $education) && $start = $education['start_date']): ?>
                        <p><strong><?php echo date('Y M. d', strtotime($start)); ?> - <?php echo array_key_exists('end_date', $education) ? date('Y M. d', strtotime($education['end_date'])) : __('Now', 'barbell-jobs'); ?></strong></p>
                    <?php endif;?>
                    <?php if (array_key_exists('description', $education) && $description = $education['description']): ?>
                        <p><?php echo nl2br($description); ?></p>
                    <?php endif;?>
                    <?php if (array_key_exists('certificate', $education) && $certificate = $education['certificate']): ?>
                        <a href="<?php echo wp_get_attachment_url($certificate); ?>" target="_blank" class="btn btn--medium btn--primary btn--certificate"><?php _e('View certificate', 'barbell-jobs'); ?></a>
                    <?php endif;?>
                </div> 
            </div>
        <?php endforeach;?>
    <?php else: ?>
        <div class="profile-no-data-icon" style="text-align: center;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/no-data-icon.png" alt="no-data">
        </div>
    <?php endif;?>
<?php endif;?>
