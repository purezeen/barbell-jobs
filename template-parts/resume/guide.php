<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <span class="hero__guide-list <?php echo !get_field('_candidate_name', $resume->ID) ?: 'active'; ?>">
        <span class="hero__guide-checkmark"></span>General
    </span>

    <span class="hero__guide-list <?php echo !$resume->post_content ?: 'active'; ?>">
        <span class="hero__guide-checkmark"></span>Biografy
    </span>

    <?php $summary = false; ?>
    <?php if ($summaryQuestions = getSummaryQuestions()): ?>
        <?php foreach ($summaryQuestions as $key => $question): ?>
            <?php if ($answer = get_field($key, $resume->ID)) { $summary = true; break; } ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <span class="hero__guide-list <?php echo !$summary ?: 'active'; ?>">
        <span class="hero__guide-checkmark"></span>Summary
    </span>
<?php endif; ?>