
<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php //var_dump(get_field('_candidate_photo', $resume->ID)); ?>
    <div class="hero__profile-img profile-img profile-img--medium">

    <?php if( get_field('_candidate_photo', $resume->ID) == null ): ?>
        <img src="<?php echo get_template_directory_uri(); ?>/img/profile-none.png" alt="profile">
    <?php else: ?>
       <img src="<?php echo get_field('_candidate_photo', $resume->ID); ?>" alt="<?php echo get_field('_candidate_name', $resume->ID); ?>" width="80" height="80">
    <?php endif; ?>
       
    </div>
    <div class="hero__content">
        <h1><?php echo nl2br(get_field('_candidate_name', $resume->ID)?: (isset($args['candidate']) && $args['candidate'] > 0 ? __('Your name', 'barbell-jobs'): '')); ?></h1>
        <p><?php echo nl2br(get_field('_resume_content', $resume->ID)?: (isset($args['candidate']) && $args['candidate'] > 0 ? __('Introduce yourself and summarize your skills to help employers see what you’re all about.', 'barbell-jobs'): '')); ?></p>
        <ul class="hero__profile-list unstyle-list">
        <li><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon.png" alt="location"><?php echo get_field('_candidate_location', $resume->ID) ?: __('Add location', 'barbell-jobs'); ?></li>
        <?php if (get_field('_location_remote', $resume->ID) || get_field('_location_relocate', $resume->ID)) {
            $jobType = '';
            if (get_field('_location_remote', $resume->ID)) {
                $jobType .= __('Remote', 'barbell-jobs');
            }
            if (get_field('_location_remote', $resume->ID) && get_field('_location_relocate', $resume->ID)) {
                $jobType .= ', '.__('relocation', 'barbell-jobs');
            } elseif (get_field('_location_relocate', $resume->ID)) {
                $jobType .= __('Relocation', 'barbell-jobs');
            }
            $jobType .= ' '.__('for the right position', 'barbell-jobs');
        } else {
            $jobType = (isset($args['candidate']) && $args['candidate'] > 0 && !get_field('_resume_content', $resume->ID) ? 
                __('Add job preference', 'barbell-jobs') : '');
        } ?>
        <?php if (isset($jobType) && $jobType): ?>
            <li><img src="<?php echo get_template_directory_uri(); ?>/img/job-type-icon.png" alt="job"><?php echo $jobType; ?></li>
        <?php endif; ?>
        <?php if (get_field('_military_veteran', $resume->ID)): ?>
            <li><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon.png" alt="location"><?php _e('Military veteran', 'barbell-jobs'); ?></li>
        <?php endif; ?>
        </ul>

        <?php
            // wp_get_post_terms( int $post_id, string|string[] $taxonomy = 'post_tag', array $args = array() )
            $tags = wp_get_post_terms($resume->ID, 'resume_skill');
        ?>
        <?php if ($tags): ?>
            <div class="hero__tags">
                <?php foreach($tags as $tag): ?>
                    <?php if ($tag): ?>
                        <span class="tag"><?php echo $tag->name; ?></span>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php 
            $user_id = array_key_exists('uid', $args) && $args['uid'] ? $args['uid'] : 0;
            $vip = false;
            if ($user_id > 0) {
                $user = get_user_by('id', $user_id);
                $vipProductId = 119;
                $vip = wcs_user_has_subscription($user->ID, $vipProductId, "active");
            }
        ?>

        <?php if (isset($user) && $user) { $user_resume_role = getResumeUserRole($user, $resume); } ?>
        <?php 
            $user_current = wp_get_current_user();
            $vipProductId2 = 119;
            $vip_role = wcs_user_has_subscription($user_current->ID, $vipProductId2, "active");
        ?>
        <!-- Show contact button only for vip user  -->
        <?php if($vip_role == true || (!empty($user) && $user->ID && $resume->post_author == $user->ID))  { ?>
            <?php if (!array_key_exists('candidate', $args)): ?>
                <a href="mailto:<?php echo get_field('_candidate_email', $resume->ID); ?>?subject=Barbell%20Jobs" class="btn btn--primary" target="_top"><?php _e('Contact now', 'barbell-jobs'); ?></a>
                <button class="btn btn--border" onclick="window.print()">Download resume</button>
            <?php endif; ?>
      
        <?php } elseif(hasRole($role="candidate") == "true") { ?>

        <?php } else { ?>
            <a href="/upgrade-to-vip-today/" class="btn btn--primary">Contact now</a>
            <a href="/upgrade-to-vip-today/" class="btn btn--border">Download resume</a>
        <?php } ?>

    </div>

    <?php if($vip_role == true || (!empty($user) && $user->ID && $resume->post_author == $user->ID)): ?>
        <div class="hero__social-icons">
            <div class="social-icons social-icons--white">
            <?php if (get_field('_candidate_email', $resume->ID)): ?>
                <a href="mailto:<?php echo get_field('_candidate_email', $resume->ID); ?>" target="_blank" class="social-icon"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/email-white.png" alt="email"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_linkedin', $resume->ID)): ?>
                <a href="<?php echo get_field('_candidate_linkedin', $resume->ID); ?>" target="_blank" class="social-icon"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-white.png" alt="linkedin"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_twitter', $resume->ID)): ?>
                <a href="<?php echo get_field('_candidate_twitter', $resume->ID); ?>" target="_blank" class="social-icon"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-white.png" alt="twitter"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_instagram', $resume->ID)): ?>
                <a href="<?php echo get_field('_candidate_instagram', $resume->ID); ?>" target="_blank" class="social-icon"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-white.png" alt="instagram"></div></a>
            <?php endif; ?>
            </div>
        </div>
    <?php elseif(hasRole($role="candidate") == "true"):?>
        
    <?php else:  ?>
        <div class="hero__social-icons">
            <div class="social-icons social-icons--white">
            <?php if (get_field('_candidate_email', $resume->ID)): ?>
                <a href="/upgrade-to-vip-today/" class="social-icon" target="_blank"> <div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/email-white.png" alt="email"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_linkedin', $resume->ID)): ?>
                <a href="/upgrade-to-vip-today/" class="social-icon" target="_blank"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-white.png" alt="linkedin"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_twitter', $resume->ID)): ?>
                <a href="/upgrade-to-vip-today/" class="social-icon" target="_blank"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-white.png" alt="twitter"></div></a>
            <?php endif; ?>
            <?php if (get_field('_candidate_instagram', $resume->ID)): ?>
                <a href="/upgrade-to-vip-today/" class="social-icon" target="_blank"><div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-white.png" alt="instagram"></div></a>
            <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
  
<?php endif; ?>