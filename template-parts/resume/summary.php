<?php if (array_key_exists('resume', $args) && $resume = $args['resume']): ?>
    <?php $summary = false; ?>
    <?php if ($summaryQuestions = getSummaryQuestions()): ?>
        <?php foreach ($summaryQuestions as $key => $question): ?>
            <?php if ($answer = get_field($key, $resume->ID)): $summary = true; ?>
                <div class="profile-summary__row border-dashed-top more-dots-wrap">
                    <?php if (array_key_exists('candidate', $args) && $args['candidate']): ?>
                        <div class="more-dots">
                            <div class="dots"><span></span><span></span><span></span></div>
                            <div class="more-dots__popup" style="display: none;">
                                <?php resumeActions($args['uid'], 'summary', $key); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="profile-summary__question counter">
                        <h4><?php echo $question; ?></h4>
                    </div>
                    <div class="profile-summary__content">
                        <p><?php echo nl2br($answer); ?></p>
                    </div>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif; ?>
    <?php if (!$summary): ?>
        <div class="profile-no-data-icon" style="text-align: center;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/no-data-icon.png" alt="no-data">
        </div>
    <?php endif;?>
<?php endif;?>
