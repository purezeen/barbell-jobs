<div class="loader"></div>
<div class="modal-wrap">
    <div class="modal">
        <div class="modal__header js-header"></div>
        <div class="modal__content js-content"></div>
        <div class="modal__footer js-footer"></div>
    </div>
</div>