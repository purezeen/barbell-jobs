<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package barbell-jobs
 */

get_header(); ?>

<main class="page-404">

	<div class="container">
		<div class="page-404__content">
			<span class="page-404__label">Error 404</span>
			<h1>OOps. Looks like something got broken!</h1>
			<div class="page-404__inner-content">
				<p>Something went wrong while opening this page. Try hitting the back button in your browser or go to the BarbellJobs home page.</p>
				<a href="/" class="btn btn--primary">Go to the home page</a>
			</div>
		</div>
	</div>

</main>

<?php get_footer();
