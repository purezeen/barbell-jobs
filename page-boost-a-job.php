<?php get_header(); ?>

<?php 
   if (!defined('ABSPATH')) {
      exit; // Exit if accessed directly.
   }

   // Check if there is a job ID
   if (array_key_exists('job_id', $_GET) && $jobId = $_GET['job_id']):

      wp_enqueue_script('resume_dashboard_script');
      wp_enqueue_style('resume_dashboard_style');

      // Redirect candidates to specific pages
      checkRoleAndRedirect(['administrator', 'employer'], 81);
      ?>

      <section class="hero hero--pb-small hero--pt-small cover">
         <div class="hero__guide hero__guide--scroll">
            <div class="container hero__guide--space-between">
               <span class="hero__guide-list active">
                  <span class="hero__guide-checkmark"></span>Company details
               </span>
               <span class="hero__guide-list active">
                  <span class="hero__guide-checkmark"></span>job details
               </span>
               <span class="hero__guide-list active">
                  <span class="hero__guide-checkmark"></span>preview
               </span>
               <span class="hero__guide-list">
                  <span class="hero__guide-checkmark"></span>packages & boosters
               </span>
               <span class="hero__guide-list">
                  <span class="hero__guide-checkmark"></span>payment
               </span>
            </div>
         </div>
         <div class="container">
            <div class="hero__content">
               <span class="hero__subtitle"><?php _e('Step 4', 'barbell-jobs'); ?></span>
               <h1><?php _e('Packages & boosters', 'barbell-jobs'); ?></h1>
            </div>
         </div>
      </section>
      <div class="container post-job-packages">
         <div class="post-job-packages__main">
            <form action="" method="post">
               <?php echo getBoosterPackages(true); ?>
               <input type="hidden" name="job_checkout" value="1">
               <input type="hidden" name="step" value="1" />
               <input type="hidden" name="job_id" value="<?php echo $jobId; ?>" />
               <input type="submit" class="button btn btn--primary btn--desktop" value="<?php _e('Go to payment', 'barbell-jobs'); ?>">
            </form>
         </div>
         <div class="post-job-packages__sidebar">
            <div class="package-summary">
               <h3 class="post-job-packages__title"><?php _e('Package summary', 'barbell-jobs'); ?></h3>
               <div class="content package-summary__content">
                  <p><?php _e('Your selection is empty', 'barbell-jobs'); ?></p>
               </div>
            </div>
            <input type="submit" class="button btn btn--primary btn--mobile"  value="<?php _e('Go to payment', 'barbell-jobs'); ?>">
         </div>
      </div>
   <?php endif; ?>
<?php get_footer(); ?>
