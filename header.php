<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package barbell-jobs
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="DNS-prefetch" href="//fonts.googleapis.com"/>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<!-- optionally increase loading priority -->
	<link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Archivo:wght@100;300;400;500;600&display=swap">
	<!-- async CSS -->
	<link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" href="https://fonts.googleapis.com/css2?family=Archivo:wght@100;300;400;500;600&display=swap">
	<!-- no-JS fallback -->
	<noscript>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Archivo:wght@100;300;400;500;600&display=swap">
	</noscript>	
	<script type="text/javascript">
	var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	</script>
	
	<?php wp_head(); ?>
	
<meta name="facebook-domain-verification" content="o59zezcpcia3292vkd3ivfdaxha1la" />	
</head>
<body <?php body_class(); ?>>

<?php 
$page_id = get_the_ID(); ?>
<?php $header__dark = ( get_field( 'navigation_style', $page_id ) == 1 ) ? 'header--dark' : ''; ?>
<?php wp_body_open(); ?>
<div id="page" class="site <?php echo $header__dark; ?>">
	<header class="site-header header">

		<div class="container-wide">
			<!-- Header logoes -->
			<a href="/" class="header__logo">
				<div class="header__logo-mobile">
					<img src="<?php echo get_template_directory_uri(); ?>/img/barbelljobs-small.svg" alt="Barbelljobs logo" class="header__logo--dark">
					<img src="<?php echo get_template_directory_uri(); ?>/img/barbelljobs-small-white.svg" alt="Barbelljobs logo" class="header__logo--white" width="30px" height="30px">
				</div>
				<div class="header__logo-desktop">
					<img src="<?php echo get_template_directory_uri(); ?>/img/barbelljobs.svg" alt="Barbelljobs logo" class="header__logo--dark">
					<img src="<?php echo get_template_directory_uri(); ?>/img/barbelljobs-white.svg" alt="Barbelljobs logo" class="header__logo--white">
				</div>
			</a>

			<!-- Hader nav list - mobile  -->
			<nav class="header__nav">

				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_class'        => 'header__nav-primary unstyle-list',
						'depth' => 1
					)
				);
				?>

				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer_col2',
						'menu_class'        => 'header__nav-secondary unstyle-list',
						'depth' => 1
					)
				);
				?>
				<?php if ( !is_user_logged_in() ) : ?>
					<a href="<?php echo home_url('login'); ?>" class="btn btn-small btn--white btn-small--white">Login</a>
				<?php endif; ?>
				
				<div class="header__nav-background">
				</div>

				<div class="social-links social-links--horizontal">
					<a href="https://www.instagram.com/barbelljobs/" target="_blank" rel="nofollow" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-white.png" alt="instagram"></a>
					<a href="https://www.facebook.com/BarbellJobs/" target="_blank" rel="nofollow" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-white.png" alt="facebook"></a>
					<a href="mailto:admin@barbelljobs.com" target="_blank" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/email-white.png" alt="email"></a>
				</div>

				<p class="header__nav-copyright">Copyright Barbell Jobs 2021. All rights reserved</p>

			</nav>
			
			<div class="header__right">
				<?php if ( is_user_logged_in() ) : ?>
					<div class="btn-profile">
						<div class="btn-profile__button btn-profile--clickable">
							<?php echo generate_user_initials(); ?>
							<span class="arrow"></span>
						</div>
						<div class="btn-profile__dropdown">
							<div class="btn-profile__header">
								<div class="btn-profile__button btn-profile__button--large">
									<?php echo generate_user_initials(); ?>
								</div>
								<p><?php echo km_get_users_name(); ?>
								</p>
							</div>
							<ul class="btn-profile__links unstyle-list">
								
								<?php 
								if(hasRole($role="employer") == "true") { ?>
									<li><a href="/my-account">My account</a></li>
								<?php } ?>

								<?php if(hasRole($role="employer") == "true") { ?>
									<li><a href="/job-dashboard/">Job Dashboard</a></li>
								<?php } ?>

								<?php if(hasRole($role="candidate") == "true") { ?>
									<li><a href="/candidate-dashboard/">Coach's Corner</a></li>
								<?php } ?>
								
								<li><a href="<?php echo wp_logout_url( home_url('login') ); ?>">Log out</a></li>
							</ul>
						</div>
					</div>
				<?php else : ?>
					<a href="<?php echo home_url('login'); ?>" class="btn btn--medium header__btn">Log in</a>
				<?php endif; ?>

				<div class="header__burger">
					<span>Menu</span>
					<svg width="14" height="11" viewBox="0 0 14 11" fill="none" xmlns="http://www.w3.org/2000/svg">
						<line x1="4" y1="5.5" y2="5.5" stroke="#060B41" stroke-width="3"/>
						<line x1="4" y1="9.5" y2="9.5" stroke="#060B41" stroke-width="3"/>
						<line x1="9" y1="5.5" x2="5" y2="5.5" stroke="#060B41" stroke-width="3"/>
						<line x1="9" y1="9.5" x2="5" y2="9.5" stroke="#060B41" stroke-width="3"/>
						<line x1="14" y1="5.5" x2="10" y2="5.5" stroke="#060B41" stroke-width="3"/>
						<line x1="14" y1="9.5" x2="10" y2="9.5" stroke="#060B41" stroke-width="3"/>
						<line x1="4" y1="1.5" y2="1.5" stroke="#060B41" stroke-width="3"/>
						<line x1="9" y1="1.5" x2="5" y2="1.5" stroke="#060B41" stroke-width="3"/>
						<line x1="14" y1="1.5" x2="10" y2="1.5" stroke="#060B41" stroke-width="3"/>
					</svg>
				</div>
				<div class="header__close">
					<span>+</span>
				</div>
			</div>
		</div>
		
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '567826721127505');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=567826721127505&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Fathom - beautiful, simple website analytics -->
<script src="https://cdn.usefathom.com/script.js" data-site="CWZWYVAX" defer></script>
<!-- / Fathom -->

	</header>

		
