<?php get_header(); ?>

<?php  
  require_once(ABSPATH . 'wp-admin/includes/media.php');
  require_once(ABSPATH . 'wp-admin/includes/file.php');
  require_once(ABSPATH . 'wp-admin/includes/image.php');
?>
<?php
   if (isset($_GET['from'], $_GET['to'])) {

      // Check if an image exists
      function imageExists($url)
      {
         $ch = curl_init($url);
         curl_setopt($ch, CURLOPT_NOBODY, true);
         curl_exec($ch);
         $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);
         return $httpCode == 200 ? true : false;
      }

      $jobs = get_posts([
         'post_type' => 'job_listing',
         'post_status' => 'all',
         'numberposts' => -1
         // 'order'    => 'ASC'
      ]);

      print '<br /><br /><br /><br /><br /><br /><br />';
      print '<h2>Jobs count: '.count($jobs).'</h2>';
      print '<hr /><hr />';

      $i = $_GET['from'];
      foreach ($jobs as $key => $job) {
         if ($i > $_GET['to']) {
            break;
         }
         $i++;

         if (has_post_thumbnail($job)) {
            $thumbnail = get_the_post_thumbnail_url($job);
            if (!imageExists($thumbnail)) {
               $jobId = $job->ID;

               // Download the company logo
               $company_logo = get_field('company_logo', $jobId, false);
               if ($company_logo) {
                  $company_logo_updated = __DIR__.'/logos/'.basename($company_logo);

                  $desc = $job->post_title;
                  $image = media_sideload_image($company_logo_updated, $jobId, $desc, 'id');
                  set_post_thumbnail($jobId, $image);

                  echo '<img src="'.get_the_post_thumbnail_url($job).'" style="width:80px;height:auto;">';
                  echo '<hr />';
               }
            }
         }
      }
   } else {
      echo '<br /><br /><br /><br /><br />';
      echo '<h1>Please set the from and to values</h1>';
      echo '<p>?from=0&to=21</p>';
   }
?>

<?php get_footer(); ?>