<?php

use Gettext\Languages\Exporter\Php;

get_header(); //Template Name: Blog ?>

<div id="main">

<?php // WP_Query arguments
$args = array(
   'posts_per_page'      => 1,
   'post__in'            => get_option( 'sticky_posts' ),
   'ignore_sticky_posts' => 1,
);
$query = new WP_Query( $args );

// The Query
$the_query_post = new WP_Query($args);

if ($the_query_post->have_posts()) : ?>

   <?php while ($the_query_post->have_posts()): $the_query_post->the_post();?>

   <?php $author_id = $post->post_author; ?>
   <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

   <div class="container page-light">
      <div class="blog-hero cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>);">
         <a href="<?php the_permalink(); ?>" class="blog-hero__content">
            <h1 class="blog-hero__title"><?php the_title(); ?></h1>
            <div class="blog-hero__content-inner">
               <p><?php the_excerpt(); ?></p>
               <span class="btn-arrow"><div class="arrow right"></div></span>
            </div>
            <div class="author author--small">
               <div class="author__img">
                  <?php echo get_avatar( $author_id, $size = 150 ); ?>
               </div>
               <div class="author__name">
                  <h6><?php echo get_the_author_meta( 'user_firstname', $author_id ); ?>&nbsp;<?php echo get_the_author_meta( 'user_lastname', $author_id ); ?></h6>
               </div>
            </div>
         </a>
      </div>
   </div>

   <?php endwhile; ?>
   <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php
$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
$per_page = 3;
$post_list = new WP_Query(array(
   'post_type' => 'post',
   'paged'          => $current_page,
   'post__not_in' => get_option( 'sticky_posts' )
));

if ($post_list->have_posts()): ?>
   <div class="blog-list container">
      <h2 class="blog-list__title">Latest posts</h2>
     
      <div class="blog-cards">
         <?php while ($post_list->have_posts()): $post_list->the_post(); ?>
            <a href="<?php the_permalink(); ?>" class="blog-card">

               <?php if( has_post_thumbnail()): ?>
                  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                  <div class="blog-card__img cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>)">
                  
                  </div>
               <?php endif; ?>

               <div class="blog-card__content">
                  <span class="blog-card__date"><?php echo get_the_date(); ?></span>
                  <h3><?php the_title(); ?></h3>
                 
               </div>
               <span class="blog-card__author"><?php echo get_the_author_meta( 'user_firstname' ); ?> <?php echo get_the_author_meta( 'user_lastname' ); ?></span>
            </a>
         <?php endwhile; ?>
      </div>

      <div class="pagination pagination--center">
         <?php
         echo paginate_links(array(
            'prev_text' => __('Previous'),
            'next_text' => __(' Next'),
         )); ?>
      </div>
     
   </div>
   <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php get_footer(); ?>