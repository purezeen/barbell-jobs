<?php
/**
 * Singe resume page template
 */
if (!defined('ABSPATH')) {
	exit;
}
get_header();

wp_enqueue_script('resume_dashboard_script');
wp_enqueue_style('resume_dashboard_style');

$resume = get_post(get_the_id());

?>

<style>
.hero::before {
    background-image: url('<?php echo get_template_directory_uri(); ?>/img/hero-background.png');
    opacity: 0.3;
    }
</style>
<section class="hero hero--profile-layout cover">
    <div class="container">
        <a href="/resumes/" class="btn-link-back"><span class="arrow left"></span><?php _e('To all profiles', 'barbell-jobs'); ?></a>
        <?php get_template_part('template-parts/resume/profile', 'Profile', [
            'resume' => $resume
        ]); ?>
    </div>
</section>

<main>
    <!-- Work experience  -->
    <?php if (get_field('work_experience', $resume->ID)): ?>
    <section class="profile-section section">
        <div class="container">
            <div class="profile-section__header">
                <h4 class="profile-section__title"><?php _e('Work experience', 'barbell-jobs'); ?></h4>
                <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-print.png" alt="print"></div>
            </div>
            <?php get_template_part('template-parts/resume/work_experience', 'Work experience', [
                'resume' => $resume
            ]); ?>
        </div>
    </section>
    <?php endif; ?>

    <!-- Education and certification -->
    <?php if (get_field('education', $resume->ID)): ?>
    <section class="profile-section section-background-light section">
        <div class="container">
            <div class="profile-section__header">
                <h4 class="profile-section__title"><?php _e('Education and certification', 'barbell-jobs'); ?></h4>
                <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/certificate-icon.png" alt="print"></div>
            </div>
            <?php get_template_part('template-parts/resume/education', 'Education', [
                'resume' => $resume,
            ]); ?>
        </div>
    </section>
    <?php endif; ?>

    <!-- Additional skills  -->
    <?php if (get_field('additional_skills', $resume->ID)): ?>
    <section class="profile-section section">
        <div class="container">
            <div class="profile-section__header">
                <h4 class="profile-section__title"><?php _e('Additional skills', 'barbell-jobs'); ?></h4>
                <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/star-icon.png" alt="print"></div>
            </div>
            <?php get_template_part('template-parts/resume/additional_skills', 'Additional skills', [
                'resume' => $resume,
            ]); ?>
        </div>
    </section>
    <?php endif; ?>

    <!-- Short description  -->
    <?php if ($resume->post_content): ?>
    <section class="section profile-short-bio section-background-blue">
        <div class="container">
            <h4 class="profile-section__title profile-short-bio__label"><?php _e('Short biography', 'barbell-jobs'); ?></h4>
            <?php get_template_part('template-parts/resume/biography', 'Biography', [
                'resume' => $resume,
            ]); ?>
        </div>
    </section>
    <?php endif; ?>

    <!-- Summary  -->
    <?php $summary = false; ?>
    <?php if ($summaryQuestions = getSummaryQuestions()): ?>
        <?php foreach ($summaryQuestions as $key => $question): ?>
            <?php if ($answer = get_field($key, $resume->ID)) { $summary = true; break; } ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($summary): ?>
    <section class="section profile-summary">
        <div class="container counter--reset">
            <div class="profile-section__header">
                <h4 class="profile-section__title"><?php _e('Summary', 'barbell-jobs'); ?></h4>
                <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/summary-icon.png" alt="print"></div>
            </div>
            <?php get_template_part('template-parts/resume/summary', 'Summary', [
                'resume' => $resume,
            ]); ?>
        </div>
    </section>
    <?php endif; ?>

    <!-- Language  -->
    <?php if (get_field('languages', $resume->ID)): ?>
    <section class="section">
        <div class="container">
            <div class="profile-section__header">
                <h4 class="profile-section__title"><?php _e('Languages', 'barbell-jobs'); ?></h4>
                <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/language-icon.png" alt="print"></div>
            </div>
            <?php get_template_part('template-parts/resume/languages', 'Languages', [
                'resume' => $resume,
            ]); ?>     
        </div>
    </section>
    <?php endif; ?>

    <!-- Cta  -->

    <?php 
        $user = wp_get_current_user();
        $vipProductId = 119;
        // $orders = wc_get_orders([
        //     'customer_id' => $user->ID
        // ]);

        // // Compare VIP product with orders
        // $vip = false;
        // foreach ($orders as $order) {
        //    $items = $order->get_items();
        //     foreach ($items as $item) {
        //         $productId = $item->get_product_id();
        //         $productVariationId = $item->get_variation_id();
        //         if ($vipProductId === $productId || $vipProductId === $productVariationId) {
        //             $vip = true;
        //         }
        //     }
        // }

        $vip = wcs_user_has_subscription($user->ID, $vipProductId, "active");
       
    ?>

    <article class="cta cta--wide container section-mb">
        <div class="cta__content">
            <h3><?php _e('Want to hire', 'barbell-jobs'); ?> <?php echo get_field('_candidate_name', $resume->ID); ?>?</h3>
            <p><?php echo sprintf(__("If you like what you see in %s’s CV and resume you can contact him directly and start talking about employement.", 'barbell-jobs'), get_field('_candidate_name', $resume->ID)); ?></p>

            <?php if($vip == true): ?>
                <a href="mailto:<?php echo get_field('_candidate_email', $resume->ID); ?>" class="btn btn--light btn--wide" target="_blank"><?php _e('Contact now', 'barbell-jobs'); ?></a>
            <?php elseif(hasRole($role="candidate") == "true"): ?>

            <?php else: ?>
                <a href="/upgrade-to-vip-today/" class="btn btn--light btn--wide"><?php _e('Contact now', 'barbell-jobs'); ?></a>
            <?php endif; ?>
        </div>
        <div class="cta__logo">
            <img src="<?php echo get_template_directory_uri(); ?>/img/barbell-jobs-white.svg" alt="barbell jobs logo">
        </div>
    </article>

</main>

<?php get_footer(); ?>
