<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package barbell_jobs_theme
 */

?>

<!-- Social links  -->
<?php if ( have_rows( 'social_links', 'option'  ) ) : ?>
	<?php while ( have_rows( 'social_links', 'option'  ) ) : the_row(); ?>
		<?php $phone = get_sub_field( 'phone', 'option' ); ?>
		<?php $messanger = get_sub_field( 'messanger', 'option' ); ?>
		<?php $linkedin = get_sub_field( 'linkedin', 'option' ); ?>
		<?php $twitter = get_sub_field( 'twitter', 'option' ); ?>
		<?php $email = get_sub_field( 'email', 'option' ); ?>
		<?php $facebook = get_sub_field( 'facebook', 'option' ); ?>
		<?php $instagram = get_sub_field( 'instagram', 'option' ); ?>
	<?php endwhile; ?>
<?php endif; ?>

	<?php $page_template =  basename( get_page_template() ); ?>
	<?php 
	if($page_template == "login.php" || $page_template == "register.php" || $page_template == "register-thank-you-template.php" || $page_template == "lost-password-template.php" ): ?>
		<!-- Footer dark  -->
		<footer class="footer footer--dark border-dashed-top border-dashed-top--light">
			<div class="container">
				<div class="footer__bottom">
					<div class="footer-bottom__col footer-copyright">
						<a href="/" class="footer__logo-small">
							<img src="<?php echo get_template_directory_uri(); ?>/img/barbelljobs-small-white.svg" alt="Barbelljobs logo" width="23px" height="28px">
						</a>
						<span>© Copyright 2021 Barbell Jobs. All rights reserved. Contact us at: <a href="mailto:admin@barbelljobs.com"><?php echo $email; ?></a></span>
					</div>
					<div class="footer-bottom__col social-links">

						<?php if($phone): ?>
							<a href="tel:<?php echo $phone; ?>" class="social-links__icon" rel="noopener" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-white.png" alt="phone" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($messanger): ?>
							<a href="<?php echo $messanger; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/messenger-white.png" alt="messenger" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($linkedin): ?>
							<a href="<?php echo $linkedin; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-white.png" alt="linkedin" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($instagram): ?>
							<a href="<?php echo $instagram; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-white.png" alt="instagram" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($facebook): ?>
							<a href="<?php echo $facebook; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-white.png" alt="facebook" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($twitter): ?>
							<a href="<?php echo $twitter; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-white.png" alt="twitter" width="18px" height="18px"></a>
						<?php endif; ?>
						<?php if($email): ?>
							<a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/email-white.png" alt="email" width="18px" height="18px"></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</footer>
	<?php else: ?>
		<!-- Footer light -->
		<footer class="footer">
			<div class="container">
				<div class="footer__top">
					<div class="footer-col">
						<a href="/" class="footer__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/barbell-job-logo.png" alt="Barbell Jobs" width="60px" height="27px">
						</a>
					</div>
					<div class="footer-col">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer_col1',
								'menu_class'        => 'unstyle-list',
								'depth' => 1
							)
						);
						?>
					</div>
					<div class="footer-col">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer_col2',
								'menu_class'        => 'unstyle-list',
								'depth' => 1
							)
						);
						?>
					</div>
					<div class="footer-col">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer_col3',
								'menu_class'        => 'unstyle-list',
								'depth' => 1
							)
						);
						?>
					</div>
				</div>
				<div class="footer__bottom">
					<div class="footer-bottom__col">
						<span>© Copyright 2021 Barbell Jobs. All rights reserved. Contact us at: <a href="mailto:admin@barbelljobs.com"><?php echo $email; ?></a></span>
					</div>
					<div class="footer-bottom__col footer-social-links social-links">
						<?php if($phone): ?>
							<a href="tel:<?php echo $phone; ?>" class="social-links__icon" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png" alt="phone" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($messanger): ?>
							<a href="<?php echo $messanger; ?>" target="_blank" rel="noopener"class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/messenger-icon.png" alt="messenger" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($linkedin): ?>
							<a href="<?php echo $linkedin; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.png" alt="linkedin" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($instagram): ?>
							<a href="<?php echo $instagram; ?>" target="_blank" rel="noopener nofollow"class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="instagram" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($facebook): ?>
							<a href="<?php echo $facebook; ?>" target="_blank" rel="noopener nofollow" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($twitter): ?>
							<a href="<?php echo $twitter; ?>" target="_blank" rel="noopener" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png" alt="twitter" width="20px" height="18px"></a>
						<?php endif; ?>
						<?php if($email): ?>
							<a href="mailto:<?php echo $email; ?>" rel="noopener" target="_blank" class="social-links__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/email-icon.png" alt="email" width="20px" height="18px"></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</footer>
	<?php endif; ?>

</div>

<?php wp_footer(); ?>

</body>
</html>
