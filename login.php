<?php
/**
* Template Name: Login page
*
*/
get_header();
?>

<section class="full-page login-page cover">
    <div class="container">
        <div class="full-page__content login-form form-dark">
            <h1>Log back into your Barbell jobs account</h1>

            <div class="form-error">    
                <?php 
                    if(isset($_GET['reason'])) {
                        if($_GET['reason'] == 'empty_username'){
                        echo __("<p><strong>Error: </strong>Required form field is missing</p>", "zotup"); // Message if e-mail is empty
                        }else if( $_GET['reason'] == 'empty_password'){
                        echo __("<p><strong>Error: </strong>Password field is</p>", "zotup"); // Message if password is empty
                        }else if( $_GET['reason'] == 'invalid_email'){
                        echo __("<p><strong>Error: </strong>Email is not valid</p>", "zotup");// Message if e-mail isn't valid or isn't in the registered email db
                        }else if( $_GET['reason'] == 'incorrect_password'){
                        echo __("<p><strong>Error: </strong>Password is not correct</p>", "zotup");//Message if the password isn't associated with the email
                        }
                        }
                    ?>
                </div>

            <?php
                if ( ! is_user_logged_in() ) { // Display WordPress login form:
                    $args = array(
                        'redirect' => admin_url(), 
                        'form_id' => 'loginform-custom',
                        'label_remember' => __( 'Remember Me' ),
                    );
                    wp_login_form( $args ); ?>
                    <p class="login-link"><a href="/register"><strong>Register now</strong></a> or <a href="/lost-password"><strong>recover lost password.</strong></a></p>
                <?php } else { // If logged in:
                    echo "<p><strong>";
                    wp_loginout( home_url('login') ); // Display "Log Out" link.
                    echo "</strong> | <strong>";
                    wp_register('', ''); // Display "Site Admin" link.
                    echo "</strong></p>";
                }
            ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
