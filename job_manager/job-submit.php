<?php
/**
 * Content for job submission (`[submit_job_form]`) shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-submit.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @version     1.34.3
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
wp_enqueue_script('resume_dashboard_script');
wp_enqueue_style('resume_dashboard_style');

// Redirect candidates to specific pages
checkRoleAndRedirect(['administrator', 'employer'], 81);

global $job_manager;
$current_url =  home_url( $_SERVER['REQUEST_URI'] );
?>

<section class="hero hero--pb-small hero-default-image">
	<div class="hero__guide hero__guide--scroll">
		<div class="container hero__guide--space-between">
			<span class="hero__guide-list step-2 active" >
				<span class="hero__guide-checkmark"></span>Company details
			</span>
			<span class="hero__guide-list step-3">
				<span class="hero__guide-checkmark"></span>job details
			</span>
			<span class="hero__guide-list step-4">
				<span class="hero__guide-checkmark"></span>preview
			</span>
			<span class="hero__guide-list step-5">
				<span class="hero__guide-checkmark"></span>packages & boosters
			</span>
			<span class="hero__guide-list step-6">
				<span class="hero__guide-checkmark"></span>payment
			</span>
		</div>
	</div>
	<div class="container">
		<div class="hero__content steps">
			<div class="step step-1">
				<span class="hero__subtitle"><?php _e('Step 1', 'barbell-jobs'); ?></span>
				<h1><?php _e('Company details', 'barbell-jobs'); ?></h1>
			</div>
			<div class="step step-2">
				<span class="hero__subtitle"><?php _e('Step 2', 'barbell-jobs'); ?></span>
				<h1><?php _e('Job details', 'barbell-jobs'); ?></h1>
			</div>
		</div>
	</div>
</section>


<div class="container post-job">
	<div class="post-job__main">
		<form action="<?php echo esc_url( $action ); ?>" method="post" id="submit-job-form" class="job-manager-form" enctype="multipart/form-data">
			<?php if (job_manager_user_can_post_job() || job_manager_user_can_edit_job($job_id)): ?>
				<?php do_action('submit_job_form_start'); ?>
				
				<div class="multistep-form">
					<!-- Step one -->
					<div class="step step-1">
						<div class="post-job__account">

							<h2 class="post-job__title"><?php _e('Account details', 'barbell-jobs'); ?></h2>
							<?php
								if (isset($resume_edit) && $resume_edit) {
									printf('<p><strong>' . esc_html__( "You are editing an existing job, %s", 'wp-job-manager') . '</strong></p>', '<a href="?job_manager_form=submit-job&new=1&key=' . esc_attr( $resume_edit ) . '">' . esc_html__('create A New Job', 'wp-job-manager') . '</a>');
								}
							?>
	
							<?php if (apply_filters('submit_job_form_show_signin', true)): ?>
								<?php get_job_manager_template('account-signin.php'); ?>
							<?php endif; ?>

						</div>
						<!-- Company Information Fields -->
						<div class="post-job__form">
							<?php if ( $company_fields ) : ?>
								<h2 class="post-job__title"><?php _e('Company info', 'barbell-jobs'); ?></h2>
								<?php do_action('submit_job_form_company_fields_start'); ?>
								<?php foreach ( $company_fields as $key => $field ) : ?>
									<fieldset class="fieldset-<?php echo esc_attr( $key ); ?> fieldset-type-<?php echo esc_attr( $field['type'] ); ?>">
										<label for="<?php echo esc_attr( $key ); ?>"><?php echo wp_kses_post( $field['label'] ) . wp_kses_post( apply_filters('submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __('(optional)', 'wp-job-manager') . '</small>', $field ) ); ?></label>
										<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
											<?php get_job_manager_template('form-fields/' . $field['type'] . '-field.php', ['key' => $key, 'field' => $field] ); ?>
										</div>
									</fieldset>
								<?php endforeach; ?>
								<?php do_action('submit_job_form_company_fields_end'); ?>
							<?php endif; ?>
							<input type="button" class="btn btn--primary js-next btn--mt-medium" data-selector=".step-2" data-selector2=".step-3" value="<?php _e('Next step', 'barbell-jobs'); ?>" />
						</div>
					</div>

					<!-- Step two -->
					<div class="step step-2">
						<div class="post-job__form">
							<!-- Basic Information Fields -->
							<h2 class="post-job__title"><?php _e('Basic info', 'barbell-jobs'); ?></h2>
							<?php do_action('submit_job_form_job_fields_start'); ?>
							<?php foreach ( $job_fields as $key => $field ) : ?>
								<fieldset class="fieldset-<?php echo esc_attr( $key ); ?> fieldset-type-<?php echo esc_attr( $field['type'] ); ?>">
									<label for="<?php echo esc_attr( $key ); ?>"><?php echo wp_kses_post( $field['label'] ) . wp_kses_post( apply_filters('submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __('(optional)', 'wp-job-manager') . '</small>', $field ) ); ?></label>
									<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
										<?php get_job_manager_template('form-fields/' . $field['type'] . '-field.php', [ 'key' => $key, 'field' => $field ] ); ?>
									</div>
								</fieldset>
							<?php endforeach; ?>
							<?php do_action('submit_job_form_job_fields_end'); ?>
							<p>
								<input type="hidden" name="job_manager_form" value="<?php echo esc_attr( $form ); ?>" />
								<input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
								<input type="hidden" name="step" value="<?php echo esc_attr( $step ); ?>" />
								<div class="post-job__button-group">
									<input type="button" class="btn btn--ghost js-prev" data-selector=".step-1" data-selector2=".step-3" value="<?php _e('Back', 'barbell-jobs'); ?>" />

									<?php if (strpos($current_url, "action=edit") !== false): ?>
										<input type="submit" name="submit_job" class="btn btn--primary js-next" value="<?php _e('Save changes', 'barbell-jobs'); // echo esc_attr( $submit_button_text ); ?>" />
									<?php else: ?>
										<input type="submit" name="submit_job" class="btn btn--primary js-next" value="<?php _e('Next step', 'barbell-jobs'); // echo esc_attr( $submit_button_text ); ?>" />
									<?php endif; ?>
									
								</div>

								<?php
								if (isset($can_continue_later) && $can_continue_later) {
									// echo '<input type="submit" name="save_draft" class="button secondary save_draft" value="' . esc_attr__('Save Draft', 'wp-job-manager') . '" formnovalidate />';
								}
								?>
								<span class="spinner" style="background-image: url(<?php echo esc_url( includes_url('images/spinner.gif') ); ?>);"></span>
							</p>
						</div>
					</div>
				</div>
				<?php do_action('submit_job_form_end'); ?>
			<?php else : ?>
				<?php do_action('submit_job_form_disabled'); ?>
			<?php endif; ?>
		</form>
	</div>
	<div class="post-job__sidebar">

		<?php if ( have_rows( 'testimonial_on_post_a_job_page', 'option' ) ) : ?>
			<?php while ( have_rows( 'testimonial_on_post_a_job_page', 'option' ) ) : the_row(); ?>

				<div class="slider__item quote-wrap quote-wrap--small">
					<div class="quote quote--small">
						<blockquote>Barbell Jobs helped me find multiple potential candidates within weeks of my job post going live. The entire process is very efficient and very easy to follow.</blockquote>
						<div class="quote-author">

								<div class="quote-author__img">
									<img src="<?php echo get_template_directory_uri(); ?>/img/testimonial.jpg" alt="barbelljobs" />
								</div>
			
							<div class="quote-author__content">
								<h6>Thomas L. Carter</h6>
								<span>Member since June 2020</span>
							</div>

						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</div>
