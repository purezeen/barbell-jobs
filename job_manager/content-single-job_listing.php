<?php

/**
 * Single job listing.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-single-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @since       1.0.0
 * @version     1.28.0
 */

use Gettext\Languages\Exporter\Php;

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
global $post;
$job_id = $post->ID;
$post->post_status;
?>

<div class="single-job container">
	<main class="single-job-main single-job-editor">
      <?php if (is_position_filled()) : ?>
         <div class="position-filled message message-info"><?php _e('This position has been filled', 'wp-job-manager'); ?></div>
         <br>
      <?php elseif (!candidates_can_apply() && 'preview' !== $post->post_status) : ?>
         <div class="listing-expired message message-info"><?php _e('Applications have closed', 'wp-job-manager'); ?></div>
      <?php endif; ?>

		<?php wpjm_the_job_description(); ?>
	</main>
	<aside class="single-job-sidebar">
		<div class="single-job-sidebar__info">
			<div class="single-job-sidebar__profile-img">
				<?php the_company_logo('full'); ?>
			</div>
			<?php the_company_name('<h3>', '</h3>'); ?>
			<?php if ($website = get_the_company_website()) : ?>
				<!-- <a class="btn-visit-site" href="<?php echo esc_url($website); ?>" rel="nofollow"><?php esc_html_e('Visit website', 'wp-job-manager'); ?></a> -->
			<?php endif; ?>
         <div class="social-icons social-icons--horizontal">
            <?php $company_linkedin = get_post_meta( $post->ID, '_company_linkedin', true );?>
            <?php if($company_linkedin): ?>
               <a href="<?php echo $company_linkedin; ?>" class="social-icon social-icon--medium social-icon--red" target="_blank">
                  <div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon-red.svg" alt="linkedin"></div>
               </a>
            <?php endif; ?>
            <?php if(get_the_company_twitter()): ?>
               <a href="<?php echo wp_kses_post( get_the_company_twitter() ); ?>" class="social-icon social-icon--medium social-icon--red" target="_blank">
                  <div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon-red.svg" alt="twitter">
                  </div>
               </a>
            <?php endif; ?>
            <?php $company_website = get_post_meta( $post->ID, '_company_website', true );?>
            <?php if($company_website): ?>
               <a href="<?php echo $company_website; ?>" class="social-icon  social-icon--medium social-icon--red" target="_blank">
                  <div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/site-icon-red.svg" alt="website">
                  </div>
               </a>
            <?php endif; ?>
            <?php $company_instagram = get_post_meta( $post->ID, '_company_instagram', true );?>
            <?php if($company_instagram): ?>
               <a href="<?php echo $company_instagram; ?>" class="social-icon  social-icon--medium social-icon--red" target="_blank">
                  <div class="social-icon__img"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-red.png" alt="instagram">
                  </div>
               </a>
            <?php endif; ?>
         </div>

			<hr class="dashed">
			<ul class="unstyle-list single-job-sidebar__info-content tag-icons">
            <li class="applied-tag"> <?php 
               if ( user_has_applied_for_job( get_current_user_id(), $post->ID ) ) {
               echo "Applied";
               } ?>
            </li>
            
            <?php $location = get_post_meta( $post->ID, '_job_location', true ); ?>
            <?php $job_position_country = get_post_meta( $post->ID, '_job_position_country', true ); ?>
            <?php $job_position_state = get_post_meta( $post->ID, '_job_position_state', true ); ?>
            <?php $job_position_city = get_post_meta( $post->ID, '_job_position_city', true ); ?> 

            <?php if($location): ?>
               <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon-grey.png" alt="location icon" width="16px" height="16px"></span><?php the_job_location(); ?>
               </li>
            <?php elseif( $job_position_country || $job_position_state || $job_position_city ): ?>
               <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon-grey.png" alt="location icon"></span>
						<?php if($job_position_country ): ?>
							<?php echo $job_position_country . ", "; ?>
						<?php endif; ?>

						<?php if($job_position_state && $job_position_country == "USA" ): ?>
							<?php echo $job_position_state . ", "; ?>
						<?php endif; ?>

						<?php if( $job_position_city ): ?>
							<?php echo $job_position_city; ?>
						<?php endif; ?>
               </li>
            <?php endif; ?>
		
            <?php 
            $job_type = wpjm_get_the_job_types( $post );
            if ( $job_type ) { ?>
               <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/job-type-icon-grey.png" alt="location icon"></span><?php wpjm_the_job_types(); ?></li>
            <?php } ?>
            
            <?php $years_of_experience = get_post_meta( $post->ID, '_job_yearsofexperience', true ); ?>
            <?php if($years_of_experience): ?>
               <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/job-type-icon-grey.png" alt="location icon"></span>Years of experience: <?php echo $years_of_experience; ?>
            <?php endif; ?>
           
            <?php if($post->_job_salary_from || $post->_job_salary_to) { ?>
				   <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/salary-icon-grey.png" alt="location icon"></span><?php echo '$' .$post->_job_salary_from . " - " .$post->_job_salary_to; ?>  
                  <?php $job_salary_timeframe = get_post_meta( $post->ID, '_job_salary_timeframe', true ); ?>
                  <?php 
                  switch ($job_salary_timeframe) {
                     case 'per_class':
                        echo "per class";
                        break;
                     case 'fixed_monthly':
                        echo "fixed/monthly";
                        break;
                     default:
                        echo $job_salary_timeframe;
                     }
                  ?>
               </li>
            <?php } ?> 
            
               <?php 
               $post = get_post( $post );
               $job_expires = $post->_job_expires;
               $job_expires_frontend = $post->_frontend_expires;
               if($post->post_status == "expired"): ?>
               <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/clock-icon-grey.png" alt="location icon"></span>Applications have closed
               <?php elseif( $job_expires_frontend ): ?>
                  <li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/clock-icon-grey.png" alt="location icon"></span>Applications closing <?php echo $job_expires_frontend; ?>
                  <?php // $display_diff = sprintf( esc_html__( 'Applications closing in %s', 'wp-job-manager' ), human_time_diff( strtotime($job_expires_frontend), time() ) );
                  // echo $display_diff;
               endif;
               ?>
            </li>
            
				<li><span class="tag-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/clock-icon-grey.png" alt="location icon"></span><?php the_job_publish_date(); ?></li>
         </ul>
         <!-- Hide apply now button for employeres  -->
         <?php if (is_position_filled() == false): ?>
            
            <!-- Application type - with form or with url  -->
            <?php $job_application = get_post_meta( $post->ID, '_application', true ); ?>
            <?php $job_application_type = get_post_meta( $post->ID, '_application_type', true ); ?>
            <?php $job_application_url = get_post_meta( $post->ID, '_application_url', true ); ?>
            <?php $user_applied = user_has_applied_for_job( get_current_user_id(), $post->ID ); ?>
            <?php if($user_applied): ?>
          
            <?php elseif(empty($job_application_url)): ?>
               <a href="#" class="btn btn--wide btn--primary open-modal">Apply now</a>
            <?php elseif(!empty($job_application_url)): ?>
               <a href="<?php echo $job_application_url; ?>" target="_blank" class="btn btn--wide btn--primary">Apply now</a>
            <?php endif; ?>
            
         <?php elseif(is_position_filled() == true): ?>
            
         <?php endif; ?>
         <!-- Share jobs  -->
         <div class="btn-share">Share job <img src="<?php echo get_template_directory_uri(); ?>/img/share-icon.svg" alt="share job">
            <div class="btn-share__content">
               <?php echo do_shortcode('[addtoany]'); ?>
            </div>
         </div>
		</div>
	</aside>
</div>

<?php if(hasRole($role="employer") == "true"): ?>
   <div class="modal-wrap modal-wrap--center">
      <div class="modal">
         <div class="modal__header modal__close">
            <h3>APPLY NOW</h3>
            <span class="modal__close-btn">
               <span>+</span>
            </span>
         </div>
         <div class="modal__content wp-job-manager-application-form">
           <p>Logged in as candidate to apply for the job.</p>
         </div>
      </div>
   </div>
   
<?php else: ?>
   <!-- Job application form  -->
   <div class="modal-wrap modal-wrap--center">
      <div class="modal">
         <div class="modal__header modal__close">
            <h3>APPLY NOW</h3>
            <span class="modal__close-btn">
               <span>+</span>
            </span>
         </div>
         <div class="modal__content wp-job-manager-application-form">
            <?php echo do_shortcode('[job_apply id="' . $job_id . '"]'); ?>
         </div>
      </div>
   </div>

<?php endif; ?>

<!-- Similar jobs  -->

<?php $get_terms_type = get_the_terms( $post->ID, 'job_listing_type' ); ?>
<?php $get_terms_categories = get_the_terms( $post->ID, 'job_listing_category' ); ?>
<?php $get_term_category_array = array(); 
?>

<?php if( !empty($get_terms_categories) ) { ?>
   <?php foreach ($get_terms_categories as $get_term_category) {
    $get_term_category_array[] = $get_term_category -> term_id;
    
   }
}?>

<?php $get_terms_type_id = $get_terms_type[0] -> term_id; ?>
<?php $get_terms_category_id = $get_term_category_array[0]; ?>
<?php $do_not_duplicate = $post->ID; ?>

<?php $args = array (
   'post_type' => 'job_listing',
   'post_status' => 'publish', 
   'post__not_in' => array($do_not_duplicate),
   'orderby' => 'rand',
   'tax_query' => array(
      'relation' => 'OR',
      array (
          'taxonomy' => 'job_listing_type',
          'field' => 'id',
          'terms' => $get_terms_type_id,
      ),
      array (
         'taxonomy' => 'job_listing_category',
         'field' => 'id',
         'terms' => $get_term_category_array,
     )
  ),
   'posts_per_page' => 4
);?>

<?php $related_post = new WP_Query($args); ?>

<?php if( $related_post->have_posts()): ?>

   <section class="similar-jobs-container section section-background-light">
      <div class="container">
         <h2 class="section-title">Similar Jobs</h2>
         <div class="similar-jobs">

            <?php while( $related_post->have_posts()) : $related_post->the_post(); ?>

               <div class="similar-jobs__col">
                  <a href="<?php the_job_permalink(); ?>" class="job-info border-dashed-top">
                     <div class="job-info__img job-info__img--small">
                        <?php the_company_logo('medium'); ?>
                     </div>
                     <div class="job-info__content job-info__content--small">
                        <?php the_company_name( '<p>', '</p> ' ); ?>
                        <h4><?php the_title(); ?></h4>
                        <p><span><?php the_job_location( false ); ?></span>
                           <?php do_action( 'job_listing_meta_start' ); ?>
                           <?php if ( get_option( 'job_manager_enable_types' ) ) { ?>
                              <?php $types = wpjm_get_the_job_types(); ?>
                              <?php if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
                                 <span class="job-type <?php echo esc_attr( sanitize_title( $type->slug ) ); ?>"><?php echo esc_html( $type->name ); ?></span>
                              <?php endforeach; endif; ?>
                           <?php } ?></p>
                     </div>
                  </a>
               </div>

            <?php endwhile; ?>
         </div>
      </div>
   </section>
       
   <?php wp_reset_postdata(); ?>
<?php endif; ?>

