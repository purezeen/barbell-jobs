<?php
/**
 * Job listing in the loop.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @since       1.0.0
 * @version     1.34.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
?>

<?php 
	//var_dump($post);
	
?>

<label <?php job_listing_class('job-listing'); ?> data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>">
		<input type="checkbox" class="job-listing-click">
		<div class="job-listing__header">
			<div class="job-listing__img">
				<?php the_company_logo('company_logo'); ?>
			</div>
			<div class="job-listing__content">
				<ul class="job-listing__content-left unstyle-list ">
					<li class="disabled-click"><?php the_company_name( '<p>', '</p> ' ); ?></li>
					<li><h3><?php wpjm_the_job_title(); ?></h3></li>
					<li class="disabled-click"><p><span>
					<?php $location = get_post_meta( $post->ID, '_job_location', true ); ?>
					<?php $job_position_country = get_post_meta( $post->ID, '_job_position_country', true ); ?>
					<?php $job_position_state = get_post_meta( $post->ID, '_job_position_state', true ); ?>
					<?php $job_position_city = get_post_meta( $post->ID, '_job_position_city', true ); ?> 

					<?php if($location): ?>
						<?php the_job_location(); ?>
						
					<?php elseif( $job_position_country || $job_position_state || $job_position_city ): ?>
					
							<?php if($job_position_country ): ?>
								<?php echo $job_position_country . ", "; ?>
							<?php endif; ?>

							<?php if($job_position_state && $job_position_country == "USA" ): ?>
								<?php echo $job_position_state . ", "; ?>
							<?php endif; ?>

							<?php if( $job_position_city ): ?>
								<?php echo $job_position_city; ?>
							<?php endif; ?>
					<?php endif; ?>
			
					</span>
						<?php do_action( 'job_listing_meta_start' ); ?>
						<?php if ( get_option( 'job_manager_enable_types' ) ) { ?>
							<?php $types = wpjm_get_the_job_types(); ?>
							<?php if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
								<span class="job-type <?php echo esc_attr( sanitize_title( $type->slug ) ); ?>"><?php echo esc_html( $type->name ); ?></span>
							<?php endforeach; endif; ?>
						<?php } ?></p>
					</li>
				</ul>
				<div class="job-listing__content-right">
					<div class="job-listing__content-right-wrap">
						<span class="date"><?php the_job_publish_date(); ?></span>
						<!-- Label red  -->
							<?php if(is_position_featured() == 1)  { ?><span class="label label--featured2">Featured</span> <?php } ?>
							<?php $staffpick = get_post_meta( $post->ID, '_job_staffpick', true );
							if ( $staffpick ): ?>
								<span class="label label--featured">Staff pick</span>
							<?php endif; ?>
							<?php $datetime1 = date_create( $post->post_date );
									$datetime2 = date_create(); // current date
									$interval = date_diff( $datetime1, $datetime2 );
									$days_old =  $interval->format( '%a' );
							if($days_old < 5) { ?>
								<span class="label label--pick">New job</span>
							<?php } ?>
						</div>
						<div class="arrow down arrow--light"></div>
				</div>
			</div>
		</div>
		<div class="job-listing__short-description">
			
			<div class="job-listing__short-description--content">

				<p><?php echo $my_excerpt = get_the_excerpt(); ?></p>				
			</div>
		
			<a href="<?php the_job_permalink(); ?>" class="btn btn--primary btn--medium">Find out more</a>
		</div>
</label>

