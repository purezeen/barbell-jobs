<?php
/**
 * Job listing preview when submitting job listings.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-preview.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @version     1.32.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php global $post; ?>

<section class="hero hero--single-layout hero-default-image">
   <div class="container">
		<div class="banner banner--mb">
			<div class="banner__content" >
				<div class="banner__icon"><img src="<?php echo get_template_directory_uri(); ?>/img/preview-icon.svg" alt="preview"></div>
				<div class="banner__content-inner">
					<p>This is only a <strong>PREVIEW</strong> of your job. If everything looks OK, click on the button and proceed to the next step. If you'd like to make any edits, <input type="submit" form="job_preview" name="edit_job" class="btn--link" value="<?php _e('click here', 'wp-job-manager'); ?>" /> and go back.</p>
				</div>
			</div>
			<div class="banner__btn banner__btn--group">
				<input type="submit" form="job_preview" name="continue" id="job_preview_submit_button" class="btn btn--small btn--light btn--remove-border" value="<?php  _e('Close preview and go next', 'wp-job-manager'); ?>" />
			</div>
		</div>
	</div>
	<div class="container">
		<div class="hero__content">
			<h1><?php wpjm_the_job_title(); ?></h1>
				<div class="hero__tags hero__tags--half">
					<?php $job_perks = get_post_meta( $post->ID, '_job_perks', true ); ?>
					<?php if($job_perks): ?>
						<?php
							// Company tag lines
							$job_perks= explode(", ", $job_perks);
							foreach ($job_perks as $value) {
									echo '<span class="tag">';
									echo $value;
									echo '</span>';
							}
						?>
					<?php endif; ?>
					<?php
					// $terms = get_the_terms( $post->ID, 'job_listing_category' );
					// if($terms) {
					// 	foreach($terms as $term) {
					// 			echo '<span class="tag">' . $term->name . '</span>';
					// 	}
					// }
					?>
					<?php
					// $job_types = get_the_terms( $post->ID, 'job_listing_type' );
					// if($job_types) {
					// 	foreach($job_types as $job_type) {
					// 			echo '<span class="tag">' . $job_type->name . '</span>';
					// 	}
					// } 
					?>
				</div>
		</div>
	</div>
</section>

<form method="post" id="job_preview" action="<?php echo esc_url( $form->get_action() ); ?>">
	<?php
	/**
	 * Fires at the top of the preview job form.
	 *
	 * @since 1.32.2
	 */
	do_action( 'preview_job_form_start' );
	?>
	<div class="job_listing_preview single_job_listing">
		<?php get_job_manager_template_part('content-single', 'job_listing'); ?>
		<input type="hidden" name="job_id" value="<?php echo esc_attr($form->get_job_id()); ?>" />
		<input type="hidden" name="step" value="<?php echo esc_attr($form->get_step()); ?>" />
		<input type="hidden" name="job_manager_form" value="<?php echo esc_attr($form->get_form_name()); ?>" />
	</div>
	<?php
	/**
	 * Fires at the bottom of the preview job form.
	 *
	 * @since 1.32.2
	 */
	do_action( 'preview_job_form_end' );
	?>
</form>
