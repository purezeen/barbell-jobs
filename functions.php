<?php
/**
 * barbell-jobs functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package barbell-jobs
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'barbell_jobs_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function barbell_jobs_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on barbell-jobs, use a find and replace
		 * to change 'barbell-jobs' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'barbell-jobs', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'barbell-jobs' ),
				'footer_col1' => esc_html__( 'Footer column1', 'barbell-jobs' ),
				'footer_col2' => esc_html__( 'Footer column2', 'barbell-jobs' ),
				'footer_col3' => esc_html__( 'Footer column3', 'barbell-jobs' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'barbell_jobs_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'barbell_jobs_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function barbell_jobs_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'barbell_jobs_content_width', 640 );
}
add_action( 'after_setup_theme', 'barbell_jobs_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function barbell_jobs_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'barbell-jobs' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'barbell-jobs' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'barbell_jobs_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function barbell_jobs_scripts() {
	wp_enqueue_style( 'barbell-jobs-style', get_stylesheet_uri(), array(), '1.56789' );
	//wp_style_add_data( 'barbell-jobs-style', 'rtl', 'replace' );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/css/custom-style.css', array(), _S_VERSION );

	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css', array(), _S_VERSION );

	wp_enqueue_script( 'barbell-jobs-navigation', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.56989', true );

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), _S_VERSION, true );

	wp_enqueue_script( 'select2', get_template_directory_uri() . '/js/select2.min.js', array('jquery'), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'barbell_jobs_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * WP Job manager Custom Functions.
 */
require get_template_directory() . '/inc/barbell-job-manager.php';

/**
 * Login and Register
 */
require get_template_directory() . '/inc/custom-login-register.php';

/**
 * Theme functionality
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Resume functionality
 */
require_once get_template_directory().'/inc/woocommerce.php';

/**
 * Resume functionality
 */
require_once get_template_directory().'/inc/resume-function.php';

/**
 * Resume functionality
 */
require_once get_template_directory().'/inc/acf.php';

function mytheme_custom_excerpt_length( $length ) {
	return 37;
}
add_filter( 'excerpt_length', 'mytheme_custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_theme_support( 'block-templates' );

add_image_size('avatar', 144, 144, ['center', 'center']);
add_image_size('size320x260', 320, 260, true );
add_image_size('company_logo', 80);
add_image_size('blog-hero', 1320);
add_image_size('full', 1920);

// Prepend footer title with acf 
add_filter('wp_nav_menu_items', 'my_wp_nav_menu_items', 10, 2);

function my_wp_nav_menu_items( $items, $args ) {
	
	// get menu
	$menu = wp_get_nav_menu_object($args->menu);
	
	// modify primary only
	if( $args->theme_location == 'footer_col1' || $args->theme_location == 'footer_col2' || $args->theme_location == 'footer_col3') {
		// vars
		$footer_column_title = get_field('footer_column_title', $menu);
		// prepend logo
		$html_footer_title = '<li><span class="footer-col__title">' . $footer_column_title . '</span></li>';
		
		// append html
		$items = $html_footer_title . $items;
	}
	
	// return
	return $items;
}

// Acf options 
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

add_action( 'template_redirect', 'checkout_redirect_non_logged_to_login_access');
function checkout_redirect_non_logged_to_login_access() {
	
	$user = wp_get_current_user();
	$user = $user->roles;
	$user_role = in_array( 'candidate', $user );

    // Here the conditions (woocommerce checkout page and unlogged user)
    if( (is_checkout() && !is_user_logged_in()) || (is_checkout() && $user_role == 'true')){

        // Redirecting to your custom login area
        wp_redirect( home_url( '/register/' ) );

        // always use exit after wp_redirect() function.
        exit;
    }
}

// Change job listing slug 
// Create slug: job_title-company_name/ 
add_action( 'save_post', 'barbelljobs_save_post_callback' );

function barbelljobs_save_post_callback( $post_id ) {

    // verify post is not a revision
    if ( ! wp_is_post_revision( $post_id ) ) {

		// unhook this function to prevent infinite looping
		remove_action( 'save_post', 'barbelljobs_save_post_callback' );
		
		$args = array(
			'post_type' => 'job_listing',
			'post__in' => [$post_id]
			
		);
		$job_post = get_posts($args);
		
		foreach ($job_post as $job) :
			$job_title = $job->post_title;
	  	endforeach;
		
		if($job_post) {
			$company_name = get_field('_company_name', $post_id);
			wp_update_post([
				"post_name" => $job_title . '-' . $company_name,
				"ID" => $post_id
			]);
		}
		
		
		// re-hook this function
		add_action( 'save_post', 'barbelljobs_save_post_callback' );
    }
}
