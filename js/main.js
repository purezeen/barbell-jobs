(function ($) {

   $(window).on('load', function () {

      setTimeout(function () {
         $('.job-manager-message, .job-manager-info, .job-manager-error').fadeOut();
      }, 5000);

      $('#company_logo').after('<span class="btn btn--border btn--medium">Add company logo</span>');

      $('.fieldset-company_logo .description').text('The photo must be a logo of your company. Supported formats are PNG, JPG up to 2MB with at least 256x256px.');

      // Modal 
      $('.open-modal').on('click', function (e) {
         e.preventDefault();
         $('.modal-wrap').fadeIn();
         // set the timeout
         $('body').addClass('open-modal');
      });

      $('.close-modal, .modal__close-btn').on('click', function (e) {
         e.preventDefault();
         $('.modal-wrap').fadeOut(300);
         setTimeout(function () {
            $('body').removeClass('open-modal');
            // 10 seconds
         }, 350);
      });

      $('.modal-wrap').on('click', function (event) {

         if (!$(event.target).closest('.modal').length) {
            setTimeout(function () {
               $('body').removeClass('open-modal');
               // 10 seconds
            }, 350);
            $('.modal-wrap').fadeOut(300);
            $('.loader').hide();
         }
      });

      // Share button 
      $('.btn-share').on('click', function () {
         $('.btn-share__content').slideToggle();
      })

      $('body').on('click', function() {
         if (!$(event.target).closest('.btn-share__content').length && !$(event.target).closest('.btn-share').length) {
            $(".btn-share__content").slideUp();
         }
      });

      // Add checkmark - login form 
      var login_form = $('.login-form .login-remember label');
      login_form.addClass('checkmark-container');
      login_form.append('<span class="checkmark"></span>');

      // Add radio button style to post a job form 
      var post_job_form = $('.post-job__form .fieldset-type-radio .field input');

      post_job_form.each(function () {
         $(this.nextSibling).wrap('<span class="text"></span>');
      });

      post_job_form.each(function () {
         $(this).wrap('<div class="radio-container"/>')
      });

      var post_job_radio = $('.fieldset-type-radio .radio-container');
      post_job_radio.each(function () {
         // $(this).next('.text').andSelf().wrapAll('<label class="radio-container__label">');
      });

      $('.fieldset-type-radio .radio-container').append('<span class="radio-checkmark"></span>');

      // Booster types - create checkbox as radio button 
      $('.product-item__booster input:checkbox').on('change', function() {

         // Remove the selection from all product
         $('.product-item__booster').removeClass('input-selected');
         $('.product-item__booster input:checkbox').prop('checked', false);

         // Select this one
         $(this).prop('checked', true);
         $(this).parent().parent().parent().addClass('input-selected');
      });

      // Product item - radio button 
      $('.product-item input:radio').on('change', function () {
         $('.product-item').removeClass('input-selected-radio');
         if ($(this).is(':checked')) {
            $(this).parent().parent().parent().addClass('input-selected-radio');
         } else {
            $(this).parent().parent().parent().removeClass('input-selected-radio');
         }
      });

      // Navigation
      $('.header').on('click', '.header__burger', function () {
         $(this).closest('.header').addClass('header--active');
         $(this).closest('.header').find('.header__nav').slideDown(100);
         $('body').css('overflow', 'hidden');
      });

      // Navigation close button 
      $('.header').on('click', '.header__close', function () {
         $(this).closest('.header').find('.header__nav').slideUp(100);
         var element = $(this).closest('.header');
         element.addClass('current');
         setTimeout(function () {
            element.removeClass('header--active');
         }, 100);

         $('body').css('overflow', 'initial');
      });

      // Hide Header on on scroll down
      'use strict';
      var c, currentScrollTop = 0,
         navbar = $('.header');

      $(window).on('scroll', function() {
         var a = $(window).scrollTop();
         var b = navbar.height();

         currentScrollTop = a;
         if (c < currentScrollTop && a > b + b) {
            navbar.addClass("header--scroll-up");
            navbar.removeClass("header--background-dark");

         } else if (c > currentScrollTop && !(a <= b)) {
            navbar.removeClass("header--scroll-up");
            navbar.addClass("header--background-dark");
         }
         c = currentScrollTop;


         if (a < 100) {
            navbar.removeClass("header--background-dark");
         }
      });

      $(document).on('click', function() {
         $(".btn-profile__dropdown").slideUp();
      });

      $('.btn-profile__dropdown').on('click', function (e) {
         e.stopPropagation();
      });

      // Btn profile dropdown meni 
      $('.btn-profile').on('click', function (e) {
         e.stopPropagation();
         $(this).find('.btn-profile__dropdown').slideToggle();
      });

      // Comment checkmark 
      var comment_checkmar = $('.comment-form-cookies-consent input');
      comment_checkmar.wrap("<div class='checkmark-container'></div>")
      comment_checkmar.append("<span class='checkmark'></span>");

      $('.job-manager-term-checklist label').append('<span class="checkmark"></span>');
      $('.job-manager-term-checklist label').addClass('checkmark-container');
      $('.required-field').prev('label').append("<span class='required-star'> *</span>");
      $('.fieldset-company_logo .required-field').append("<span class='required-star'> *</span>");

      $('select').select2({
         // minimumResultsForSearch: Infinity
      });

      $('.filters select').select2({
         allowClear: true
      });

      // Slick
      $('.slick-slider').slick({
         arrows: false,
         dots: false,
         infinite: true,
         speed: 300,
         slidesToShow: 1
      });

      $('.slider-controls__btn--prev').on('click', function () {
         $(this).parent().parent().find('.slick-slider').slick('slickPrev');
      });

      $('.slider-controls__btn--next').on('click', function (e) {
         e.preventDefault();
         $(this).parent().parent().find('.slick-slider').slick('slickNext');
      });


      $(document).on('click', function (e) {
         if ($(e.target).closest(".more-dots").length === 0) {
            $('.more-dots__popup').hide();
         }
      });

      $(document).on('click', '.more-dots', function (e) {
         var more_dots_popup = $(this).find('.more-dots__popup');
         $('.more-dots__popup').not(more_dots_popup).hide();
         $(this).find('.more-dots__popup').slideToggle();
      })

      $(".multiple-select-skill").length == 0;

      $(document).on('change', '.multiple-select-skill', function (e) {
         console.log($(this).length);
         if ($(this).length == 0) {

         }
      })

      // Hero quide 
      var hero_quide = $('.hero__guide');

      if (hero_quide.length != 0) {
         var stickyTop = hero_quide.offset().top;

         $(window).on('scroll', function () {
            if ($(window).scrollTop() >= stickyTop) {
               $('.hero__guide').addClass('hero__guide--fixed');
            } else {
               $('.hero__guide').removeClass('hero__guide--fixed');
            }
         });
      }

      // Company logo 
      // var companyLogo = document.getElementById("company_logo");
      // if (companyLogo) {
      // companyLogo.addEventListener("change", PreviewImage);
      // }

      function PreviewImage() {
         // console.log(getElementsByClassName("job-manager-uploaded-files"))
         // var oFReader = new FileReader();
         // oFReader.readAsDataURL(document.getElementsByClassName("job-manager-uploaded-files")[0].files[0]);

         // oFReader.onload = function (oFREvent) {
         //     document.getElementsByClassName("job-manager-uploaded-files")[0].src = oFREvent.target.result;
         // };
      };

      // Active filters on job search page
      var filterContainer = $(document).find('#filter-custom');
      filterContainer.on('change', '.filters input, .filters select', function () {

         var activeFilters = filterContainer.find('.filter-active'),
            filtersList = activeFilters.find('.unstyle-list');

         // Remove all active filters
         filtersList.html('');

         // Find all checked input, and add them to active filter
         var checkedFilters = $(document).find('#filter-custom .filters input:checked');
         var checkedSelectFilters = $(document).find('#filter-custom .filters .state-wrap option:selected');

         activeFilters.show('slow');

         if (checkedSelectFilters.length > 0) {
            checkedSelectFilters.each(function () {
               console.log($(this));
               var filterId = $(this).attr('data-select2-id'),
                  filterName = $(this).val();
               filtersList.append('<li id="select-filter-remove" data-selector="' + filterId + '"><div class="btn-close btn-close--red btn-close--medium filter-active-item"><span>+</span></div>' + filterName + '</li>');
            });
         }

         if (checkedFilters.length > 0) {
            checkedFilters.each(function () {
               var filterId = $(this).attr('id'),
                  filterName = $(this).parent()[0].textContent;
               filtersList.append('<li class="js-remove-filter"  data-selector="' + filterId + '"><div class="btn-close btn-close--red btn-close--medium filter-active-item"><span>+</span></div>' + filterName + '</li>');
            });
         }
      });

      // On remove filter click
      filterContainer.on('click', '.js-remove-filter', function () {
         selector = $(this).data('selector');
         // Trigger a click
         $(document).find('#' + selector).click();
      });

      $exampleMulti = $('.state-wrap select');
      filterContainer.on('click', '#select-filter-remove', function () {
         $('.select2-selection__clear').trigger('mousedown');
      });

      // Add placeholder to login form 
      $('.login-form #user_login').attr('placeholder', 'Username');
      $('.login-form #user_pass').attr('placeholder', 'Password');

      $('#user_login').attr('placeholder', 'Email');

      // Job listings 
      var job_listing_interval = setInterval(function () {
         if ($('.job-listing').is(':visible')) {

            // Append title and cta after featured jobs
            function cta_banner() {
               $('.job_position_featured').last().next('.job_listing').before('<article class="cta cta--inner section-mb section-mt"><div class="cta__content"><p>Skip the line and connect directly with hiring managers and companies looking for you.</p><a href="/landing-page/" class="btn btn--light">Create a profile</a></div><div class="cta__logo"><img src="' + templateUrl + '/img/barbell-jobs-white.png" alt="barbell jobs logo"></div></article><h2 class="section-title-medium">Other jobs</h2>');
            }

            cta_banner();
            // On click load more add title and cta 

            $('.load_more_jobs').on('click', function () {
               setTimeout(function () {
                  cta_banner();
               }, 500);
            })

            $('#wp-job-manager-custom .btn').on('click', function () {
               setTimeout(function () {
                  cta_banner();
               }, 2000);
            });

            clearInterval(job_listing_interval);
         }
      }, 100);

      // Filters 
      var job_listing_interval_filter = setTimeout(function () {
         $('.filter-btn').on('click', function () {
            $('#filter-custom').slideDown(300);
            $('body').toggleClass('overflow-hidden');
            $('.wpjmsf-checklist-field-wrapper label').append('<span class="checkmark"></span>');
         });

         $('.filter-close').on('click', function () {
            $('#filter-custom').slideUp(300);
            $('body').toggleClass('overflow-hidden');
         });

         if ($('.filters').is(':visible')) {

            setTimeout(function () {
               $('.wpjmsf-checklist-field-wrapper label').append('<span class="checkmark"></span>');
               clearTimeout(job_listing_interval_filter);
            }, 500);
         }

      }, 1000);

      // Filters 
      // When click on USA checkbox, enamb
      var inputUSA = $(document).find('#filter-custom');
      if (inputUSA) {
         inputUSA.on('change', 'input[type=checkbox]#job_position_country_USA', function () {
            var input_checked = $('input[type=checkbox]#job_position_country_USA:checked');
            var inputSelect = $(".state-wrap select");
            var inputState = $('.state-wrap');
            if (input_checked.length == 1) {
               inputSelect.attr("disabled", false);
               inputState.removeClass('disabled-wrap');
            } else {
               inputSelect.attr("disabled", true);
               $('.state-wrap .select2-selection__clear').trigger('mousedown');
               inputState.addClass('disabled-wrap');
            }
         });
      }

   });

})(jQuery);