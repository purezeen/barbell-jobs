(function ($) {
    $(document).ready(function () {
        // console.log('Resume init.');

        /** Get request via AJAX */
        $(document).on('click', '.js-ajax', function(evt) {
            evt.preventDefault();
            var url = $(this).data('url');

            // console.log(url);

            if (confirm("Are you sure?") == true) {
                // Get call
                $.get(url).done(function(response) {
                    var data = response.data;
                    // console.log(data);
                    if (typeof data != 'undefined' && data) {
                        data.forEach(value => {
                            // console.log({
                            //     value_selector: value.selector,
                            //     value_content: value.content
                            // });
                            $(document).find(value.selector).html(value.content);
                        });
                        // console.log(response);
                    }
                });
            }
        });

        /**
         * Show the modal window
         * @param {DOMObject} modal 
         */
        function resumeShowModal(modal = false) {
            modal = modal ? modal : $(document).find('.modal-wrap');
            modal.fadeIn();
            $('body').addClass('open-modal');
        }

        /**
         * Hide the modal window
         * Remove its content
         * Remove all event listeners
         * @param {DOMObject} modal 
         */
        function resumeHideModal(modal = false) {
            modal = modal ? modal : $(document).find('.modal-wrap');
            modal.fadeOut();
            $('body').removeClass('open-modal');
          
            // Remove the content
            modal.find('.js-header, .js-content, .js-footer').html('');

            // Remove event listeners
            modal.off();
        }

        // Open modal
        $(document).on('click', '.js-open-modal', function() {
            var modal = $(document).find('.modal-wrap');
            var loader = $(document).find('.loader').show();
            var url = $(this).data('url');
            $.post(url, {})
                .done(function(response) {
                    resumeShowModal(modal);
                    var data = response.data;
                    
                    modal.find('.js-header').html(data.header);
                    modal.find('.js-content').html(data.content);
                    modal.find('.js-footer').html(data.footer);

                    // Image display/upload functionality
                    modal.on('change', 'input[type="file"]', function(evt) {
                        var files = event.target.files,  
                            [file] = files,
                            type = $(this).data('type');

                            // /** @debug */
                            // console.log({
                            //     type: type,
                            //     file: file,
                            //     files: files
                            // });

                        if (file) {
                            var allowedImages = [
                                'image/jpeg',
                                'image/jpg',
                                'image/png'
                            ];
                            var allowedFiles = [
                                'application/pdf',
                                'application/msword',
                                'application/vnd.oasis.opendocument.text'
                            ]
                            if (type == 'image' && (!allowedImages.includes(file.type) || file.size < 25000 || file.size > 2000000)) {
                                alert('Photo must be headshot of your face. Supported formats are PNG, JPG up to 2MB with at least 256x256px.');
                            } else if (type == 'file' && (!allowedFiles.includes(file.type) || file.size > 4000000)) {
                                alert('Supported formats are PDF, DOC, DOCX, ODT, smaller than 4Mb.');
                            } else {
                                // Upload this image via AJAX, and fill the field with the image URL
                                var rid = $(this).data('id'),
                                    imageSelector = $(this).data('image-selector'),
                                    formSelector = $(this).data('form-selector'),
                                    name = $(this).attr('name'),
                                    fd = new FormData();

                                $.each(files, function(key, value) {
                                    fd.append(key, value);
                                });
                                fd.append('rid', rid);
                                fd.append('type', type);
                        
                                $.ajax({
                                    url: '/wp-admin/admin-ajax.php?action=file_upload&fname='+name,
                                    type: "POST",
                                    data: fd, 
                                    dataType: 'json',
                                    contentType: false,
                                    cache: false,
                                    processData:false,
                                    success: function(response) {
                                        // /** @debug */
                                        // console.log(response);
                                        
                                        if (response.error) {
                                            alert(response.error);
                                        } else {
                                            // console.log(modal.find(formSelector));
                                            // console.log(modal.find(imageSelector));
                                            if (imageSelector) {
                                                modal.find(imageSelector).attr('src', response.src);
                                            }
                                            if (formSelector) {
                                                modal.find(formSelector).val(type == 'file' ? response.aid : response.src);
                                            }
                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        console.error('Error: ' + textStatus);
                                    }
                                });
                            }
                        }

                    });

                    modal.find('#resumeForm').on('submit', function(evt) {
                        evt.preventDefault();

                        // 1. Get form data,
                        // 2. Get submit URL,
                        // 3. Send an AJAX call, and
                        // 4. Update the page content with the response data

                        var form = $(this),
                            url = form.attr('action'),
                            formData = form.serialize();
                            // console.log(url);

                        // Check the candidate photo
                        if (form.hasClass('candidate-profile') && !form.find('.candidate-photo').val().length) {
                            alert('Please upload a photo of yourself.');
                        } else {
                            // Post request
                            $.post(url, {
                                form: formData
                            })
                                .done(function(response) {
                                    if (response.error) {
                                        alert(response.error);
                                    } else {
                                        var data = response.data;
                                        // console.log(data);
                                        if (typeof data != 'undefined' && data) {
                                            data.forEach(value => {
                                                // console.log({
                                                //     value_selector: value.selector,
                                                //     value_content: value.content
                                                // });
                                                $(document).find(value.selector).html(value.content);
                                            });
                                            // console.log(response);
                                        }
                                        resumeHideModal(modal);
                                        loader.hide();
                                    }
                                });
                        }
                    });

                    // Close modal
                    modal.find('.js-close').on('click', function() {
                        resumeHideModal();
                        loader.hide();
                    });
                });
        });

        // Multistep form
        var jobForm = $(document).find('#submit-job-form'),
            multistepForm = jobForm.find('.multistep-form');
            // console.log([jobForm, multistepForm]);

        // On change remove error class from field 
        $(document).on("change paste keyup", '.error', function() {
            $(this).removeClass('error'); 
        });

        // Next step
        multistepForm.on('click', '.js-next', function() {
            var selector2 = $(this).data('selector2');
            var selector = $(this).data('selector'),
                valide = true;
                var file_type = $(".fieldset-company_logo .job-manager-uploaded-file");
                if ( file_type.length == 0){
                    valide = false;
                    $(".step .fieldset-company_logo.fieldset-type-file").addClass('error');
                } else {
                    $(".step .fieldset-company_logo.fieldset-type-file").removeClass('error');
                }

                $(this).closest('.step').find('input, textarea').each(function(key, required) {
                    if (!required.checkValidity()) {
                        valide = false;
                        required.classList.add('error');
                    } else {
                        required.classList.remove('error');
                    }
                });

                $(this).closest('.step').find('.fieldset-type-term-select select').each(function(key, required) {
                    if (!required.checkValidity()) {
                        valide = false;
                        $(this).parent().parent().addClass('error');
                    } else {
                        $(this).parent().parent().removeClass('error');
                    }
                });

                $(this).closest('.step').find('.fieldset-type-term-multiselect select').each(function(key, required) {
                    var multiField = $(this).val();
                    if (multiField == "") {
                        valide = false;
                        $(this).parent().parent().addClass('error');
                    } else {
                        $(this).parent().parent().removeClass('error');
                    }
                });

                $(this).closest('.step').find('#job_description_ifr').each(function(key, required) {
                    var iframeBody = $(this).contents().find("body").text();
                    if (iframeBody == '') {
                        valide = false;
                        $(this).parent().parent().parent().parent().parent().parent().parent().addClass('error');
                    } else {
                        $(this).parent().parent().parent().parent().parent().parent().parent().removeClass('error');
                    }
                });

                $(this).closest('.step').find('.wp-editor-wrap textarea').each(function(key, required) {
                  
                    if (!required.checkValidity()) {
                        valide = false;
                        $(this).parent().parent().addClass('error');
                    } else {
                        $(this).parent().parent().removeClass('error');
                    }
                });
                
                if (!valide) {
                    alert('Please fill all required elements.');
                } else {
                    if (selector) {
                        $(document).find('.step').hide();
                        $(document).find(selector).show();
                        $(document).find('.hero__guide '+selector2).addClass('active');
                    }

                    $('html, body').animate({
                        scrollTop: 0
                    }, 'slow');
                }
        });

        // Prev step
        multistepForm.on('click', '.js-prev', function() {
            var selector = $(this).data('selector');
            var selector2 = $(this).data('selector2');
            if (selector) {
                $(document).find('.step').hide();
                $(document).find(selector).show();
                $(document).find('.hero__guide '+selector2).removeClass('active');
            }

            $('html, body').animate({
                scrollTop: 0
            }, 'slow');
        });

        // Packages functionality
        $(document).on('change', '.product-item', function() {
            calculateThePrice();
        });

        /**
         * Calculate the package prices
         */
        function calculateThePrice() {
            $(document).find('.package-summary .content').html('');
            var all = 0;
            $(document).find('.product-item input:checked').each(function(key, item) {
                // console.log([$(item).data('name'), $(item).data('price')]);
                $(document).find('.package-summary .content').append('<div class="content-row"><div class="content-name"><span>'+$(item).data('name')+ '</span></div><span class="content-price">$' + $(item).data('price')+'</span></div>');
                all = all + $(item).data('price');
            });
            $(document).find('.package-summary .content').append('<p class="total-price"><span>Total price</span><b>$'+all+'</b></p>');
        }
        calculateThePrice();

        // Add products to cart
        $(document).on('click', '.js-checkout', function() {
            $(document).find('.product-item input:checked').each(function(key, item) {
                var _item = $(item),
                    pid = _item.val();
                // console.log(pid);
                addToCart(pid);
            });

            // console.log($(this).data('url'));
        });
    });

    // Add product to cart function
    function addToCart(pid) {
        $.get('/?add-to-cart=' + pid, function(response) {
            // console.log(response);
        });
    }

}) (jQuery);
