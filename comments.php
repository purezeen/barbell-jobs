<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package barbell-jobs
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="section comments">
	<div class="container">
		<div class="comments__inner">
			<?php
			$aria_req = "required";
			$fields =  array(
				'author' => '<div class="form-col2"><p class="general-form js-float-label-wrapper">'.'<label for="author">Name</label><input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
				'email'  => '<p class="general-form js-float-label-wrapper">'.'<label for="email">E-mail</label><input id="email" class="form-control" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',
				'cookies' => ""
			); 

			$comments_args = array(
				'fields' =>  $fields,
				'class_container' => "form-small",
				'comment_notes_before' => "",
				'title_reply'=>'<h2 class="section-title">'.'Leave a comment'.'<h2>',
				'comment_field' => '<p class="general-form js-float-label-wrapper" ><label for="comment">Your comment</label><textarea id="comment" class="form-control" name="comment" rows="4" cols="100" aria-required="true"></textarea></p>',
				'comment_notes_after' => '',
				'label_submit' => __('Send comment'),
			);

			comment_form($comments_args);

			// You can start editing here -- including this comment!
			if ( have_comments() ) :
				?>
			<hr class="dashed">

			<div class="comment-header">
				<h2 class="comment-title">Comments</h2>
				<span class="comment-count">
					<?php
						$barbell_jobs_comment_count = get_comments_number();
						if ( '1' === $barbell_jobs_comment_count ) {
							printf(
								/* translators: 1: title. */
								esc_html__( 'One comment', 'barbell-jobs' ),
								'<span>' . wp_kses_post( get_the_title() ) . '</span>'
							);
						} else {
							printf( 
								/* translators: 1: comment count number, 2: title. */
								esc_html( _nx( '%1$s comments', '%1$s comments', $barbell_jobs_comment_count, 'comments title', 'barbell-jobs' ) ),
								number_format_i18n( $barbell_jobs_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'<span>' . wp_kses_post( get_the_title() ) . '</span>'
							);
						}
						?>
				</span>
			</div>
			
			<?php the_comments_navigation(); ?>

			<ol class="comment-list unstyle-list">
				<?php
					wp_list_comments(
						array(
							'style'      => 'ol',
							'short_ping' => true,
						)
					);
					?>
			</ol><!-- .comment-list -->

			<?php
				the_comments_navigation();

				// If comments are closed and there are comments, let's leave a little note, shall we?
				if ( ! comments_open() ) :
					?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'barbell-jobs' ); ?></p>
			<?php
				endif;
			endif;
			?>
		</div>
	</div>
</div><!-- #comments -->