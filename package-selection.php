<?php
/**
 * Template for choosing a package during the Job Listing submission.
 *
 * This template can be overridden by copying it to yourtheme/wc-paid-listings/package-selection.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager-resumes
 * @category    Template
 * @since       1.0.0
 * @version     2.9.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

wp_enqueue_script('resume_dashboard_script');
wp_enqueue_style('resume_dashboard_style');

// Redirect candidates to specific pages
checkRoleAndRedirect(['administrator', 'employer'], 81);

global $job_manager;
?>

<section class="hero hero--pt-small hero--pb-small hero-default-image">
	<div class="hero__guide hero__guide--scroll">
		<div class="container hero__guide--space-between">
			<span class="hero__guide-list active">
				<span class="hero__guide-checkmark"></span>Company details
			</span>
			<span class="hero__guide-list active">
				<span class="hero__guide-checkmark"></span>job details
			</span>
			<span class="hero__guide-list active">
				<span class="hero__guide-checkmark"></span>preview
			</span>
			<span class="hero__guide-list active">
				<span class="hero__guide-checkmark"></span>packages & boosters
			</span>
			<span class="hero__guide-list">
				<span class="hero__guide-checkmark"></span>payment
			</span>
		</div>
	</div>
	<div class="container">
		<div class="hero__content">
			<span class="hero__subtitle"><?php _e('Step 4', 'barbell-jobs'); ?></span>
			<h1><?php _e('Packages & boosters', 'barbell-jobs'); ?></h1>
		</div>
	</div>
</section>
<div class="container post-job-packages">
	<div class="post-job-packages__main">
		<?php
        if ($packages || $user_packages):
            // VIP product id
            $vipProductId = 119;
            $user = wp_get_current_user();
			$vip = wcs_user_has_subscription($user->ID, $vipProductId, "active");
		
            // Check if the post is published or not
            $thisJob = get_post($_POST['job_id']);
            $published = ($thisJob->post_status == 'publish') ? true : false;
            ?>
			<?php if (!$published && !$vip && $packages): ?>
				<h2 class="post-job-packages__title"><?php _e('Pick your plan', 'barbell-jobs'); ?></h2>
				<?php foreach ($packages as $key => $package): ?>
					<?php $product = wc_get_product($package); ?>
					<?php if (!$product->is_type(array('job_package', 'job_package_subscription')) || !$product->is_purchasable()) { continue; } ?>
					<?php
						$id = $product->get_id();
						$name = $product->get_name();
						$price = wc_price($product->get_price(), ['decimals' => 0]);
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()));
					?>
					<label class="product-item <?php echo !$key ? 'input-selected-radio' : ''; ?>" for="product-<?php echo $id; ?>">
						<div class="product-name">
							<div class="product-image">
								<img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" >
							</div>
							<h3><?php echo $name; ?></h3>
							<p><?php echo $package->post_excerpt; ?></p>
						</div>
						<div class="product-price">
							<?php echo $price; ?>
							<label class="radio-container">
							<input type="radio" name="product[]" value="<?php echo $id; ?>" id="product-<?php echo $id; ?>" required <?php echo !$key ? 'checked' : ''; ?> data-name="<?php echo $name; ?>" data-price="<?php echo $product->get_price(); ?>" />
							<span class="radio-checkmark"></span>
							</label>
						</div>
					</label>		
				<?php endforeach; ?>
				<span class="post-job-packages__mb"></span>
			<?php endif; ?>

			<?php if ($vip): ?>
				<?php
				// Get vip products
				$vipProducts = wc_get_products([
					'category' => ['vip'],
				]);
				if (is_array($vipProducts)): ?>
					<h2 class="post-job-packages__title"><?php _e('VIP', 'barbell-jobs'); ?></h2>
					<?php foreach ($vipProducts as $key => $vipProduct): ?>
						<?php
							$id = $vipProduct->get_id();
							$name = $vipProduct->get_name();
							$price = wc_price($vipProduct->get_price(), ['decimals' => 0]);
							$image = wp_get_attachment_image_src(get_post_thumbnail_id($vipProduct->get_id()));
						?>
						<label class="product-item vip" for="product-<?php echo $id; ?>" style="border-color: #F45434 !important; background-color: rgba(243, 64, 27, 0.05);">
						
						<img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" class="product-image product-image__booster">
						<div class="product-name product-name__booster"><?php echo $name; ?></div>
						<div class="product-price">
							<?php echo $price; ?>
							<div class="checkmark-container">
								<input type="radio" name="product[]" value="<?php echo $id; ?>" id="product-<?php echo $id; ?>" data-name="<?php echo $name; ?>" data-price="<?php echo $vipProduct->get_price(); ?>" checked/>
								<span class="radio-checkmark"></span>
							</div>
						</div>
						</label>
						<br />
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endif; ?>
			<?php echo getBoosterPackages(); ?>
			<input type="hidden" name="job_checkout" value="1">
			<input type="hidden" name="step" value="1" />
			<input type="submit" class="button btn btn--primary btn--desktop" value="<?php _e('Go to payment', 'barbell-jobs'); ?>">
		<?php else : ?>
			<p><?php _e('No packages found', 'wp-job-manager-wc-paid-listings'); ?></p>
		<?php endif; ?>
	</div>
	<div class="post-job-packages__sidebar">
		<div class="package-summary">
			<h3 class="post-job-packages__title"><?php _e('Package summary', 'barbell-jobs'); ?></h3>
			<div class="content package-summary__content">
				<p><?php _e('Your selection is empty', 'barbell-jobs'); ?></p>
			</div>
		</div>
		<input type="submit" class="button btn btn--primary btn--mobile"  value="<?php _e('Go to payment', 'barbell-jobs'); ?>">
	</div>
</div>
